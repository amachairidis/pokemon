local xLoc = 77
local yLoc = 1.5
local zLoc = -40
local color = "yellow"
local input
local chipY = 2.61
local kantoLocations = {{26.09,-16.62},{32.89,-16.57},{26.13,-22.10},{32.88,-22.07}}
local jhotoLocations = {{31.74,-16.63},{38.40,-16.61},{31.74,-21.89},{38.43,-21.96}}
local hoennLocations = {{-5.99,-14.04},{-6.01,-19.17},{0.57,-14.03},{0.63,-19.13}}
local sinnohLocations = {{-32.73,14.82},{-32.8,9.68},{-26.23,14.89},{-26.57,9.72}}
local unovaLocations = {{-36.86,22.11},{-36.86,17.26},{-30.65,22.12},{-30.6,17.24},{-5.2,6.16},{1.8,10.1},{6.1,5.95}}
local kalosLocations = {{29.11,-15.45},{29.08,-20.38},{35.36,-15.45},{35.37,-20.39}}
local alolaLocations = {{-14.68,-14.47},{-14.66,-19.43},{-8.41,-14.45},{-8.36,-19.41}}
local locations = {kantoLocations, jhotoLocations, hoennLocations, sinnohLocations, unovaLocations, kalosLocations, alolaLocations}

function onLoad()

end

function move()
  self.setPosition({xLoc, yLoc, zLoc})
end

function activate(input_params)
    input = input_params
  self.createButton({ --Apply settings button
      label="Deal", click_function="deal_chips",
      function_owner=self, tooltip="Click to deal " .. color .. " pokemon chips to the unoccupied " .. color .." spaces.",
      position={0,0,3}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
  self.createButton({ --Apply settings button
      label="Shuffle", click_function="shuffle_chips",
      function_owner=self, tooltip="Click to shuffle contents of the " .. color .. " pokeball.",
      position={0,0,5}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
  self.createButton({ --Apply settings button
      label="Collect", click_function="collectchips",
      function_owner=self, tooltip="Click to return the " .. color .. " chips to the " .. color .. " pokeball.",
      position={0,0,7}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
end

function deal_chips()
  local currentLocations = locations[input.region]
  local put_list = {}
  for i=1, 4 do
    local param = {}
    param.origin = {currentLocations[i][1], chipY, currentLocations[i][2]}
    param.direction = {0,-1,0}
    param.type = 1
    param.max_distance = 1
    param.debug = false
    hits = Physics.cast(param)
    if #hits ~= 0 then
      put_list[i] = Global.call("put_chips_to_container", {container=self, chips=hits})
      --self.putObject(getObjectFromGUID(hits[1].hit_object.guid))
    end
  end
  wait_for_put(put_list, 1, 0)
end

function wait_for_put(put_list, pos_nbr, chip_nbr)
    chip_nbr = chip_nbr + 1
    if pos_nbr > #put_list then
        shuffle_chips()
        deal()
    elseif chip_nbr > #put_list[pos_nbr] then
        wait_for_put(put_list, pos_nbr+1, 0)
    else
        Wait.condition(
            function() wait_for_put(put_list, pos_nbr, chip_nbr) end,
            function() return getObjectFromGUID(put_list[pos_nbr][chip_nbr]) == nil end
        )
    end
end

function shuffle_chips()
   self.shuffle()
 end

 function collectchips()
   local currentLocations = locations[input.region]
   for i=1, #currentLocations do
     local param = {}
     param.origin = {currentLocations[i][1], chipY, currentLocations[i][2]}
     param.direction = {0,-1,0}
     param.type = 1
     param.max_distance = 1
     param.debug = false
     hits = Physics.cast(param)
     if #hits ~= 0 then
        if hits[2] ~= nil then
            self.putObject(getObjectFromGUID(hits[2].hit_object.guid))
        else
            self.putObject(getObjectFromGUID(hits[1].hit_object.guid))
        end
    end
   end
 end


 function deal()
   local currentLocations = locations[input.region]
   for i=1, #currentLocations do
     if self.getQuantity() > 0 then
       if i <5 then
         local param = {}
         param.position = {currentLocations[i][1], chipY, currentLocations[i][2]}
         param.rotation = {180,0,0}
         self.takeObject(param)
       else
         local param = {}
         param.origin = {currentLocations[i][1], chipY, currentLocations[i][2]}
         param.direction = {0,-1,0}
         param.type = 1
         param.max_distance = 1
         param.debug = false
         hits = Physics.cast(param)
         if #hits == 0 then
           local param = {}
           param.position = {currentLocations[i][1], chipY, currentLocations[i][2]}
           param.rotation = {180,0,0}
           self.takeObject(param)
         end
       end
     end
   end
 end