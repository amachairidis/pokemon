version = 1000
initialLocation = {1.5, 0, -1}
battlemodels_GUID = 'bebe24'

function onChat(message, player)
    --[[read_write_guidlist(message)
    read_write_modelfill(message)
    check_correctness(message)
    toggle_spawn_scale(message)
    print_save(message)]]
end

scale_on_spawn = false
SPAWN_SCALE = 100
--[[ The onLoad event is called after the game save finishes loading. --]]
function onLoad(save_string)
  local table = {}
  table.origin = initialLocation
  table.direction = {0,1,0}
  table.type = 1
  table.max_distance = 5
  local hits = Physics.cast(table)
  if(getObjectFromGUID("c041f4") == nil) then
    local region = hits[1].hit_object.getStateId()
    print("board state = " .. region )
    local table = {}
    table.region = region
    getObjectFromGUID("4dba05").call("activate", table)

    if (getObjectFromGUID("118e0e") == nil) then
      getObjectFromGUID("52a142").call("activate", table)
      getObjectFromGUID("97fba7").call("activate", table)
      getObjectFromGUID("1c139b").call("activate", table)
      getObjectFromGUID("950dc9").call("activate", table)
      getObjectFromGUID("4e3b10").call("activate", table)
    else
      getObjectFromGUID("118e0e").call("activate", table)
    end

  end
  battlemodels = getObjectFromGUID(battlemodels_GUID)
  battlemodels.interactable = false
  battlemodels.setLock(true)
  local save_table
  if save_string and save_string ~= "" then
      save_table = JSON.decode(save_string)
  end
  --print(save_string)
  if save_table then
      models_enabled = save_table.enabled
      global_scale = save_table.model_scale
      spawn_delay = save_table.spawn_delay
  else
      models_enabled = true
      global_scale = 2
      spawn_delay = 0
  end
  for key,pokemon_base in pairs(all_pokemon) do
      local saved_state
      if pokemon_base.states then
          for skey,pokemon_state in pairs(pokemon_base.states) do
              if save_table and save_table.base[key] and save_table.base[key].states[skey] then
                  pokemon_state.created_before = save_table.base[key].states[skey].created_before
              else
                  pokemon_state.created_before = false
              end
              if pokemon_state.custom_scale == nil then
                  pokemon_state.custom_scale = 1
              end
          end
      else
          if save_table and save_table.base[key] then
              pokemon_base.created_before = save_table.base[key].created_before
          else
              pokemon_base.created_before = false
          end
          if pokemon_base.custom_scale == nil then
              pokemon_base.custom_scale = 1
          end
      end
      pokemon_base.in_creation = false
      if pokemon_base.idle_effect == nil then
          pokemon_base.idle_effect = "Idle"
      end
      if pokemon_base.run_effect == nil then
          pokemon_base.run_effect = "Run"
      end
      if pokemon_base.spawn_effect == nil then
          pokemon_base.spawn_effect = "Special Attack"
      end
      if pokemon_base.faint_effect == nil then
          pokemon_base.faint_effect = "Faint"
      end
      if pokemon_base.despawn_time == nil then
          pokemon_base.despawn_time = 1.5
      end
      if pokemon_base.offset == nil then
          pokemon_base.offset = {x=0, y=0, z=0}
      end
      if pokemon_base.persistent_state == nil then
          pokemon_base.persistent_state = true
      end
      for _,guid in pairs(pokemon_base.chip_GUIDs) do
          active_chips[guid] = {
              chip_GUID = guid,
              base = pokemon_base,
              in_creation = false,
              picked_up = false
          }
          local chip = getObjectFromGUID(guid)
          if chip then
              active_chips[guid].chip = chip
              chip.registerCollisions()
          end
          --print(pokemon_base.name, " ", guid)
          if save_table and save_table.active[guid] then
              local pokemon = active_chips[guid]
              local active_save = save_table.active[guid]

              if active_save.state_key then
                  pokemon.state_key = active_save.state_key
                  pokemon.state = pokemon.base.states[active_save.state_key]
              end
              local model = getObjectFromGUID(active_save.model_GUID)
              if model then
                  pokemon.model = model
                  active_models[active_save.model_GUID] = pokemon
                  if not pokemon.state then
                      pokemon.state = pokemon.base
                  end
                  model.interactable = false
                  model.dynamic_friction = 1
                  model.mass = 0
                  model.setLock(true)
                  try_activate_effect(pokemon.model, pokemon.state.idle_effect or pokemon.base.idle_effect)
              end
          end
      end
  end
end

function init_state(pokemon_state, saved_state)
    if saved_state then
        pokemon_state.created_before = saved_state.created_before
    else
        pokemon_state.created_before = false
    end
end

function onSave()
    return generate_save_string()
end

function any_state_created_before(states)
    for _,pokemon_state in pairs(states) do
        if pokemon_state.created_before then
            return true
        end
    end
    return false
end

function generate_save_string()
    local base_table = {}
    for key,pokemon_base in pairs(all_pokemon) do
        if (pokemon_base.states and any_state_created_before(pokemon_base.states)) or pokemon_base.created_before then
            local base_data = {}
            if pokemon_base.states then
                base_data.states={}
                for skey,pokemon_state in pairs(pokemon_base.states) do
                    if pokemon_state.created_before then
                        base_data.states[skey] = {
                            created_before = pokemon_state.created_before
                        }
                    end
                end
            else
                base_data.created_before = pokemon_base.created_before
            end
            base_table[key] = base_data
        end
    end
    local active_table = {}
    for key,pokemon in pairs(active_chips) do
        if pokemon.model then
            active_table[key] = {
                model_GUID = pokemon.model.getGUID()
            }
            if pokemon.state_key then
                active_table[key].state_key = pokemon.state_key
            end
        elseif pokemon.state_key then
            active_table[key] = {
                state_key = pokemon.state_key
            }
        end
    end
    local save_table = {version=version, base=base_table, active=active_table, enabled = models_enabled, model_scale=global_scale, spawn_delay=spawn_delay}
    return JSON.encode(save_table)
end

-- further events
function onObjectLeaveContainer(container, leave_object)
    --print("Leave container " .. leave_object.getGUID())
    local pokemon = get_pokemon_by_GUID(leave_object.getGUID())
    if pokemon then
        pokemon.chip = leave_object
        pokemon.chip.registerCollisions()
        Wait.time(function() wait_for_chip(pokemon) end, 0.5 --[[ Prevent spawning while object is dealt]])
    end
end

function onObjectEnterContainer(container, enter_object)
    --print("Enter container " .. enter_object.getGUID())
    -- Debug only
    if (guid_nbr) then
        guid_nbr = guid_nbr + 1
        guid_list[guid_nbr] = enter_object.getGUID()
        if guid_list_with_names then
            names_list[guid_nbr] = enter_object.getName()
        end
    end

    -- Actual code
    local pokemon = get_pokemon_by_GUID(enter_object.getGUID())
    if pokemon then
        enter_object.unregisterCollisions()
        pokemon.chip = nil
        wait_for_chip(pokemon)
    end
end

function onObjectCollisionEnter(hit_object, collision_info)
    --print("Collision " .. hit_object.getGUID())
    local pokemon = get_pokemon_by_GUID(hit_object.getGUID())
    if pokemon then
        wait_for_chip(pokemon)
    end
end

function onObjectHover(player_color, hover_object)
    --if hover_object then print("Hover " .. hover_object.getGUID()) end
    if hover_object then
        local pokemon = get_pokemon_by_GUID(hover_object.getGUID())
        if pokemon and next(Player[player_color].getHoldingObjects()) and Player[player_color].getHoldingObjects()[1].getGUID() ~= pokemon.chip_GUID then
            if pokemon.model and has_something_on_top(pokemon) then
                despawn_now(pokemon)
                wait_for_empty_stack(pokemon)
            end
        end
    end
end

function onObjectPickUp(player_color, picked_up_object)
    --print("Picked Up " .. picked_up_object.getGUID())
    pickUp(get_pokemon_by_GUID(picked_up_object.getGUID()))
end

function onObjectDrop(player_color, dropped_object)
    --print("Dropped " .. dropped_object.getGUID())
    drop(get_pokemon_by_GUID(dropped_object.getGUID()))
end

function onObjectDestroy(dying_object)
    local guid = dying_object.getGUID()
    local pokemon_by_chip = get_pokemon_by_GUID(guid)
    if pokemon_by_chip then
        pokemon_by_chip.chip = nil
        despawn_now(pokemon_by_chip)
    else
        local pokemon_by_model = get_pokemon_by_GUID(guid, true)
        if pokemon_by_model then
            -- model is destroyed even though chip isn't (may happen during table flip)
            pokemon_by_model.model = nil
            active_models[guid] = nil
        end
    end
end
-- Function to retrieve pokemon

function get_pokemon_by_GUID(guid, by_model_guid)
    if by_model_guid then
        return active_models[guid]
    else
        return active_chips[guid]
    end
end

function is_pokemon_chip(tbl)
    return get_pokemon_by_GUID(tbl.obj_GUID) ~= nil
end

--model spawn and despawn related functions
function isFaceUp(obj)
	return (obj.getTransformUp()[2] > 0)
end

function has_something_on_top(pokemon)
    assert(pokemon.chip)
    local hits = Physics.cast({
        origin = model_position(pokemon, true),
        direction = {x=0, y=1, z=0},
        type = 3,
        size = pokemon.chip.getBounds().size
    })
    for i=1,#hits do
        if hits[i].hit_object ~= pokemon.model and hits[i].hit_object ~= pokemon.chip then
            return true
        end
    end
    return false
end

function get_pokemon_below(pokemon)
    assert(pokemon and pokemon.chip)
    local hits = Physics.cast({
        origin = model_position(pokemon, true),
        direction = {x=0, y=-1, z=0},
        type = 1
    })
    local number_pokemon_below = 0
    local pokemon_below = {}
    for i=1,#hits do
        if hits[i].hit_object ~= pokemon.chip then
            local hit_pokemon = get_pokemon_by_GUID(hits[i].hit_object.getGUID())
            if hit_pokemon then
                number_pokemon_below = number_pokemon_below + 1
                pokemon_below[number_pokemon_below] = hit_pokemon
            end
        end
    end
    return pokemon_below
end

function despawn_below(pokemon)
    if not pokemon or not pokemon.chip then return end
    local pokemon_below = get_pokemon_below(pokemon)
    for i=1,#pokemon_below do
        despawn_now(pokemon_below[i])
    end
end

function wait_for_chip(pokemon)
    Wait.condition(
        function() check_for_spawn_or_despawn(pokemon) end,
        function() return not pokemon.chip or pokemon.chip.resting or (pokemon.model==nil) == isFaceUp(pokemon.chip) end
    )
end

function check_for_spawn_or_despawn(pokemon)
    if not models_enabled then return end
    if not pokemon.base.model_GUID and not pokemon.base.states then return end -- some pokemon do not have a model yet
    if not pokemon.chip or (pokemon.model and not isFaceUp(pokemon.chip)) then
        if not pokemon.model or pokemon.in_creation then return end
        pokemon.in_creation = true
        if pokemon.model.getGUID() == pokemon.state.model_GUID then
            if pokemon.base.in_creation then
                pokemon.in_creation = false
                Wait.condition(function() check_for_spawn_or_despawn(pokemon) end, function() return not pokemon.base.in_creation end)
                return
            end
            pokemon.base.in_creation = true
        end
        local pokemon_model=pokemon.model
        pokemon.model=nil
        if pokemon.chip and not pokemon.picked_up and try_activate_effect(pokemon_model, pokemon.state.faint_effect or pokemon.base.faint_effect) then
            Wait.time(function() despawn_model(pokemon, pokemon_model) end, pokemon.base.despawn_time)
        else
            despawn_model(pokemon, pokemon_model)
        end
    elseif not pokemon.chip.resting then
        Wait.condition(function() check_for_spawn_or_despawn(pokemon) end, function() return not pokemon.chip or pokemon.chip.resting end)
    elseif not pokemon.model and isFaceUp(pokemon.chip) then
        if pokemon.in_creation or pokemon.base.in_creation then
            Wait.condition(
                function() Wait.time(function() check_for_spawn_or_despawn(pokemon) end, 0.7 --[[Time needed for despawn animation]]) end,
                function() return not pokemon.in_creation and not pokemon.base.in_creation end
            )
        elseif has_something_on_top(pokemon) then
            return
        else
            pokemon.in_creation = true
            pokemon.base.in_creation = true
            spawn_model(pokemon)
        end
    end
end

function despawn_now(pokemon)
    if not pokemon.model then return end
    if pokemon.in_creation then
        Wait.condition(function() despawn_now(pokemon) end, function() return not pokemon.in_creation end)
    else
        pokemon.in_creation = true
        if pokemon.model.getGUID() == pokemon.state.model_GUID then
            pokemon.base.in_creation = true
        end
        local pokemon_model = pokemon.model
        pokemon.model = nil
        despawn_model(pokemon, pokemon_model)
    end
end

function despawn_model(pokemon, model)
    active_models[model.getGUID()] = nil
    if model.getGUID() == pokemon.state.model_GUID then
        local despawn_scale = 1/SPAWN_SCALE
        if pokemon.scale_set then
            for _,scale in pairs(pokemon.scale_set) do
                despawn_scale = despawn_scale * scale
            end
            pokemon.scale_set = {}
        end
        scale_model(model, despawn_scale)
        battlemodels.putObject(model)
        if not pokemon.base.persistent_state then
            pokemon.state = nil
            pokemon.state_key = nil
        end
        pokemon.base.in_creation = false
    else
        destroyObject(model)
    end
    pokemon.in_creation = false
    assert(pokemon.model == nil)
end

function model_position(pokemon, without_offset)
    assert(pokemon.chip)
    local offset = {x=0, y=0, z=0}
    if not without_offset then
        offset = pokemon.base.offset
        offset = {x=offset.x*global_scale, y=offset.y*global_scale, z=offset.z*global_scale}
    end
    return pokemon.chip.positionToWorld (
        pokemon.chip.positionToLocal(
            pokemon.chip.getPosition()+{x=0, y=pokemon.chip.getBounds().size[2], z=0}
        ) + offset
    )
end

function model_rotation(pokemon)
    assert(pokemon.chip)
    return {x=0,y=pokemon.chip.getRotation()[2],z=0}
end

function bag_contains(guid, bag)
    for _,obj in ipairs(bag.getObjects()) do
        if guid == obj.guid then
            return true
        end
    end
    return false
end

function spawn_model(pokemon)
    assert(pokemon.chip)
    assert(pokemon.model == nil)
    if not pokemon.state then
        if pokemon.base.states then
            local r = math.random(1,#pokemon.base.states)
            pokemon.state = pokemon.base.states[r]
            pokemon.state_key = r
        else
            pokemon.state = pokemon.base
        end
    end
    base_model = getObjectFromGUID(pokemon.state.model_GUID)
    if base_model then
        local p = {
          position = model_position(pokemon)
        }
        pokemon.model = base_model.clone(p)
        pokemon.model.setPosition(model_position(pokemon))
        pokemon.model.setRotation(model_rotation(pokemon))
        Wait.condition(
            function()
                active_models[pokemon.model.getGUID()] = pokemon
                init_model(pokemon)
            end,
            function() return pokemon.model.getGUID() ~= pokemon.state.model_GUID end
        )
    elseif bag_contains(pokemon.state.model_GUID, battlemodels) then --check should not be needed if used correctly - but people tend to do crazy stuff
        active_models[pokemon.state.model_GUID] = pokemon
        local p = {
          position = model_position(pokemon),
          rotation = model_rotation(pokemon),
          guid = pokemon.state.model_GUID,
          smooth = false,
          callback_function = function(obj) spawn_callback(obj, futureName) end
        }
        battlemodels.takeObject(p)
        -- reset if pokemon hasn't spawned after 2 sec (e.g. if AssetBundle could not be loaded)
        Wait.condition(function() return end, function() return not pokemon.in_creation end, 2,
            function()
                assert(pokemon.model == nil)
                active_models[pokemon.state.model_GUID] = nil
                pokemon.in_creation = false
                pokemon.base.in_creation = false
            end
        )
    else -- for some reason model does not exist
        if pokemon.model_not_existent_message_printed == nil then
            print("Model " .. pokemon.state.model_GUID .. " for " .. pokemon.state.name .. " does not exist. Have you deleted it? If this occurs frequently, please contact the developer. :-)")
            pokemon.model_not_existent_message_printed = true
        end
        pokemon.in_creation = false
        pokemon.base.in_creation = false
    end
end

function spawn_callback(obj, name)
    if not obj then return end -- AssetBundle has not been found
    local pokemon = active_models[obj.getGUID()]
    if not pokemon then return end -- Timeout before spawn happened
    pokemon.model = obj
    assert(pokemon.model.getGUID() == pokemon.state.model_GUID)
    if not pokemon.state.created_before then
        pokemon.model.scale(global_scale)
        pokemon.state.created_before = true
        init_model(pokemon, true)
    else
        pokemon.model.scale(SPAWN_SCALE)
        init_model(pokemon)
    end
end

function init_model(pokemon, first_creation)
    pokemon.model.interactable = false
    pokemon.model.dynamic_friction = 1
    pokemon.model.mass = 0
    pokemon.model.setLock(true)
    pokemon.base.in_creation = false
    pokemon.in_creation = false
    Wait.condition(
        function()
            try_activate_effect(pokemon.model, pokemon.state.idle_effect or pokemon.base.idle_effect)
            Wait.time(
                function()
                    try_activate_effect(pokemon.model, pokemon.state.spawn_effect or pokemon.base.spawn_effect)
                    if first_creation --[[and pokemon.state.custom_scale ~= 1]] then
                        local scale_step = math.sqrt(math.sqrt(math.sqrt(pokemon.state.custom_scale)))
                        pokemon.scale_set = {scale_step, scale_step, scale_step, scale_step, scale_step, scale_step, scale_step, scale_step}
                        -- Wait 2 sec before scaling except if pokemon despawns in between
                        Wait.time(function() scale_model_next(pokemon) end, 2)
                    end
                end,
                spawn_delay
            )
        end,
        function() return not pokemon.model or (not pokemon.model.spawning and not pokemon.model.loading_custom) end
    )
end

function scale_model(pokemon_model, scale_factor)
    pokemon_model.scale(scale_factor)
    pokemon_model.mass = 0
end

function scale_model_next(pokemon)
    if not pokemon.model then return end
    for key,scale in pairs(pokemon.scale_set) do
        scale_model(pokemon.model, scale)
        pokemon.scale_set[key] = nil
        Wait.time(function() scale_model_next(pokemon) end, 0.05)
        return
    end
end

function try_activate_effect(model, effectName)
    if not model then return false end

    local triggerList = model.AssetBundle.getTriggerEffects()
    local loopingList = model.AssetBundle.getLoopingEffects()

    if (not triggerList or not loopingList) then
        return false
    end

    for _, effect in ipairs(triggerList) do
        if effect.name == effectName then
            model.AssetBundle.playTriggerEffect(effect.index)
            return true
        end
    end
    for _, effect in ipairs(loopingList) do
        if effect.name == effectName then
            model.AssetBundle.playLoopingEffect(effect.index)
            return true
        end
    end
    return false
end

-- pickup and drop related functions
function wait_for_empty_stack(pokemon)
    --print("Wait for empty stack")
    Wait.time(
        function()
            assert(pokemon)
            if not pokemon.chip then return end
            local hits = Physics.cast({
                origin = model_position(pokemon, true),
                direction = {x=0, y=1, z=0},
                type = 3,
                size = pokemon.chip.getBounds().size
            })
            local resting_on_top = false
            for i=1,#hits do
                if hits[i].hit_object ~= pokemon.model and hits[i].hit_object ~= pokemon.chip then
                    if hits[i].hit_object.resting then
                        resting_on_top = true
                    else
                        wait_for_empty_stack(pokemon)
                        return
                    end
                end
            end
            if not resting_on_top then
                check_for_spawn_or_despawn(pokemon)
            end
        end,
        0.5
    )
end

function pickUp(pokemon)
    if not pokemon or not pokemon.model then return end
    assert(pokemon.chip)
    pokemon.picked_up = true
    pokemon.model.setPosition(model_position(pokemon))
    pokemon.model.setLock(false)
    pokemon.model.jointTo(pokemon.chip, {
        ["type"]        = "Fixed",
        ["collision"]   = false,
        ["break_force"]  = math.infinity,
        ["break_torgue"] = math.infinity,
    })
    try_activate_effect(pokemon.model, pokemon.state.run_effect or pokemon.base.run_effect)
    wait_for_chip(pokemon)
    local pokemon_below = get_pokemon_below(pokemon)
    for i=1,#pokemon_below do
        wait_for_empty_stack(pokemon_below[i])
    end
end

function drop(pokemon)
    if not pokemon then return end
    if not pokemon.model then
        Wait.condition(function() despawn_below(pokemon) end, function() return not pokemon.chip or pokemon.chip.resting end)
        return
    end
    assert(pokemon.chip)
    pokemon.model.jointTo()
    pokemon.model.setPosition(model_position(pokemon))
    pokemon.model.setRotation(model_rotation(pokemon))
    pokemon.model.setLock(true)
    try_activate_effect(pokemon.model, pokemon.state.idle_effect or pokemon.base.idle_effect)
    pokemon.picked_up = false
    Wait.condition(function() despawn_below(pokemon) walk_to_chip(pokemon) end, function() return not pokemon.model or not pokemon.chip or pokemon.chip.resting end)
end

function walk_to_chip(pokemon)
    if pokemon.chip and pokemon.model and isFaceUp(pokemon.chip) then
        pokemon.model.setPositionSmooth(model_position(pokemon), false, false)
        pokemon.model.setRotationSmooth(model_rotation(pokemon), false, false)
    else
        check_for_spawn_or_despawn(pokemon)
    end
end

--
function scale_models(tbl)
    local new_scale
    local new_scale
    if tbl.scale > 1 then
        new_scale = math.ceil(global_scale*tbl.scale*100)/100
    else
        new_scale = math.floor(global_scale*tbl.scale*100)/100
    end
    if new_scale > 0 then
        for _,pokemon in pairs(active_chips) do
            if pokemon.model then
                scale_model(pokemon.model, new_scale/global_scale)
                pokemon.model.setPosition(model_position(pokemon))
            end
        end
        global_scale = new_scale
    end
end

function toggle_models_enabled()
    if models_enabled then
        models_enabled = false
        for _,pokemon in pairs(active_chips) do
            despawn_now(pokemon)
        end
    else
        models_enabled = true
        for _,pokemon in pairs(active_chips) do
            if pokemon.chip then
                check_for_spawn_or_despawn(pokemon)
            end
        end
    end
end

function get_models_enabled()
    return models_enabled
end

function increase_spawn_delay(tbl)
    local new_delay = math.floor(100*(spawn_delay+tbl.delay))/100
    if new_delay >= 0 then
        spawn_delay = new_delay
    end
end

function get_spawn_delay()
    return spawn_delay
end

-- For Yellow Pokemon. Needs every chip to be listed in the pokemon table
function put_chips_to_container(tbl)
    local put_list = {}
    local put_nbr = 0
    for i=1, #tbl.chips do
        local pokemon=get_pokemon_by_GUID(tbl.chips[i].hit_object.guid)
        if pokemon and pokemon.chip then
            put_nbr = put_nbr + 1
            put_list[put_nbr] = pokemon.chip_GUID
            tbl.container.putObject(pokemon.chip)
        end
    end
    return put_list
end

active_chips = {}
active_models = {}

-- Pokemon list
all_pokemon =
{
    {name="Bulbasaur", chip_GUIDs = {"b0ffb0"}, model_GUID = "e1b027", spawn_effect="Physical Attack"},
    {name="Ivysaur", chip_GUIDs = {"bfd8aa"}, model_GUID = "19c53c", offset={x=0, y=0.075, z=0}},
    {name="Mega Venusaur", chip_GUIDs = {"f82898","2d0ff4"}, model_GUID = "40331c", spawn_effect="Mega Evolve"},
    {name="Venusaur", chip_GUIDs = {"66aad1","af171b"}, model_GUID = "452ac8", spawn_effect="Physical Attack"},
    {name="Charmander", chip_GUIDs = {"e5ca28"}, model_GUID = "3226f2"},
    {name="Charmeleon", chip_GUIDs = {"23fb15"}, model_GUID = "2bf00e"},
    {name="Charizard", chip_GUIDs = {"0d0388","f04453"}, model_GUID = "27261b", spawn_effect="Status Attack"},
    {name="Mega Charizard X", chip_GUIDs = {"76c7ea","eda49b"}, model_GUID = "29611d", spawn_effect="Mega Evolve"},
    {name="Mega Charizard Y", chip_GUIDs = {"59d8a7","313e19"}, model_GUID = "3da16c", spawn_effect="Mega Evolve"},
    {name="Squirtle", chip_GUIDs = {"ebfb18"}, model_GUID = "a96b7f"},
    {name="Wartortle", chip_GUIDs = {"9d1f37"}, model_GUID = "99fd39"},
    {name="Blastoise", chip_GUIDs = {"a75561","1f0e60"}, model_GUID = "54ea11"},
    {name="Mega Blastoise", chip_GUIDs = {"37847e","b9bab5"}, model_GUID = "08b8f9", spawn_effect="Mega Evolve"},
    {name="Caterpie", chip_GUIDs = {"e4117c"}, model_GUID = "7c8b8a"},
    {name="Metapod", chip_GUIDs = {"16ef42","cb3474"}, model_GUID = "3f9c36"},
    {name="Butterfree", chip_GUIDs = {"ab31d4"}, model_GUID = "77c6f3"},
    {name="Weedle", chip_GUIDs = {"e3b88f"}, model_GUID = "535929"},
    {name="Kakuna", chip_GUIDs = {"9ae7b5","baed8d"}, model_GUID = "b46efe"},
    {name="Beedrill", chip_GUIDs = {"16a678"}, model_GUID = "68937e", spawn_effect="Status Attack"},
    {name="Mega Beedrill", chip_GUIDs = {"f777ca"}, model_GUID = "ef20d1", spawn_effect="Mega Evolve"},
    {name="Pidgey", chip_GUIDs = {"0ecd4a","a1ab4a"}, model_GUID = "e042f9"},
    {name="Pidgeotto", chip_GUIDs = {"0d71f0","975162"}, model_GUID = "47d87b", spawn_effect="Physical Attack"},
    {name="Mega Pidgeot", chip_GUIDs = {"0badc6","fbb4d9"}, model_GUID = "a6d82a", spawn_effect="Mega Evolve"},
    {name="Pidgeot", chip_GUIDs = {"f4dee9","5d6857"}, model_GUID = "831415"},
    {name="Rattata", chip_GUIDs = {"7f6f77"}, model_GUID = "dd6f20"},
    {name="Raticate", chip_GUIDs = {"e6509c"}, model_GUID = "2539f9"},
    {name="Spearow", chip_GUIDs = {"534cb3"}, model_GUID = "f508f3"},
    {name="Fearow", chip_GUIDs = {"3970a9"}, model_GUID = "c4b8b6"},
    {name="Ekans", chip_GUIDs = {"44cc3c"}, model_GUID = "ac74d9"},
    {name="Arbok", chip_GUIDs = {"30dccf","4d9852","9cf956"}, model_GUID = "0df3c1"},
    {name="Pikachu", chip_GUIDs = {"532f18","138311"}, model_GUID = "813555", spawn_effect="Status Attack"},
    {name="Raichu", chip_GUIDs = {"ac3284","65e9c9","3d7f9d"}, model_GUID = "ce46d3"},
    {name="Sandshrew", chip_GUIDs = {"a10bb0"}, model_GUID = "6ac711"},
    {name="Sandslash", chip_GUIDs = {"868cfc"}, model_GUID = "14e1b5"},
    {name="Nidoran F", chip_GUIDs = {"6bff38"}, model_GUID = "585515", spawn_effect="Physical Attack"},
    {name="Nidorina", chip_GUIDs = {"0c9122"}, model_GUID = "22488e"},
    {name="Nidoqueen", chip_GUIDs = {"b376ca","5d5875","c0fe92"}, model_GUID = "c91f41"},
    {name="Nidoran M", chip_GUIDs = {"7e029c"}, model_GUID = "3bb97e", spawn_effect="Physical Attack"},
    {name="Nidorino", chip_GUIDs = {"e26dbe"}, model_GUID = "ebed42"},
    {name="Nidoking", chip_GUIDs = {"f5a8c4","413a9b","b08688"}, model_GUID = "b038db"},
    {name="Clefairy", chip_GUIDs = {"2a2f73","463dee"}, model_GUID = "782a2e"},
    {name="Clefable", chip_GUIDs = {"0719d8"}, model_GUID = "b1a7ba"},
    {name="Vulpix", chip_GUIDs = {"09e465"}, model_GUID = "7e6d40"},
    {name="Ninetales", chip_GUIDs = {"b82e1a"}, model_GUID = "42c4da"},
    {name="Jigglypuff", chip_GUIDs = {"ac7e43"}, model_GUID = "92122e"},
    {name="Wigglytuff", chip_GUIDs = {"bd3290"}, model_GUID = "499574", spawn_effect="Physical Attack"},
    {name="Zubat", chip_GUIDs = {"8b0c85"}, model_GUID = "6ce120", spawn_effect="Physical Attack"},
    {name="Golbat", chip_GUIDs = {"506092","d0af9b"}, model_GUID = "9b980c"},
    {name="Oddish", chip_GUIDs = {"0dcd84"}, model_GUID = "79e706"},
    {name="Gloom", chip_GUIDs = {"67f18b"}, model_GUID = "aa2f29", spawn_effect="Physical Attack"},
    {name="Vileplume", chip_GUIDs = {"a38556","415482","9fe930"}, model_GUID = "588b3f"},
    {name="Paras", chip_GUIDs = {"4303df"}, model_GUID = "07242c", spawn_effect="Physical Attack"},
    {name="Parasect", chip_GUIDs = {"08b8e9"}, model_GUID = "5beef7"},
    {name="Venonat", chip_GUIDs = {"0ec0f6"}, model_GUID = "dd0526"},
    {name="Venomoth", chip_GUIDs = {"090fa8","faef47","8cca01","c29090"}, model_GUID = "530c83"},
    {name="Diglett", chip_GUIDs = {"915e3d"}, model_GUID = "808d36"},
    {name="Dugtrio", chip_GUIDs = {"4fa758","47c773"}, model_GUID = "b4d340"},
    {name="Meowth", chip_GUIDs = {"f4647c","f44b97"}, model_GUID = "9e0a72", spawn_effect="Physical Attack"},
    {name="Persian", chip_GUIDs = {"d05552"}, model_GUID = "ae2934"},
    {name="Psyduck", chip_GUIDs = {"99a57b"}, model_GUID = "7b6cb0"},
    {name="Golduck", chip_GUIDs = {"fb422a"}, model_GUID = "e26653"},
    {name="Mankey", chip_GUIDs = {"bf167b","ef2290"}, model_GUID = "85b071", spawn_effect="Physical Attack"},
    {name="Primeape", chip_GUIDs = {"860de1","1430be"}, model_GUID = "2b471e", spawn_effect="Physical Attack"},
    {name="Growlithe", chip_GUIDs = {"7b2759","c03770"}, model_GUID = "b8f54f"},
    {name="Arcanine", chip_GUIDs = {"c38238","bbb43e","d2f45c"}, model_GUID = "d89ae7"},
    {name="Poliwag", chip_GUIDs = {"a57cf6"}, model_GUID = "c271fd", spawn_effect="Physical Attack"},
    {name="Poliwhirl", chip_GUIDs = {"acbd30"}, model_GUID = "865416"},
    {name="Poliwrath", chip_GUIDs = {"489b1c","8217ef"}, model_GUID = "24f90c"},
    {name="Abra", chip_GUIDs = {"91d7c9"}, model_GUID = "f338a0"},
    {name="Kadabra", chip_GUIDs = {"f531b6","a10f91"}, model_GUID = "635cd5"},
    {name="Alakazam", chip_GUIDs = {"2e4af9","1354ba","847726","e51fe9"}, model_GUID = "c3804e"},
    {name="Mega Alakazam", chip_GUIDs = {"572c85","5c0ce5","97ef39","32e508"}, model_GUID = "ef33f5", spawn_effect="Mega Evolve"},
    {name="Machop", chip_GUIDs = {"2a0ddf","0cc2dd"}, model_GUID = "4a5304", spawn_effect="Physical Attack"},
    {name="Machoke", chip_GUIDs = {"af802f","b0cbd1","6378c2"}, model_GUID = "e7cc3a"},
    {name="Machamp", chip_GUIDs = {"db5112","416d49","fdc1b8"}, model_GUID = "94b0de", spawn_effect="Physical Attack"},
    {name="Bellsprout", chip_GUIDs = {"85a7e4"}, model_GUID = "4ca9db"},
    {name="Weepinbell", chip_GUIDs = {"1d7d60","5ad9b4"}, model_GUID = "961d7f"},
    {name="Victreebell", chip_GUIDs = {"e56a32","968c44","ce3e95"}, model_GUID = "be71b4", spawn_effect="Physical Attack"},
    {name="Tentacool", chip_GUIDs = {"259826"}, model_GUID = "2aefb2"},
    {name="Tentacruel", chip_GUIDs = {"97bc59"}, model_GUID = "9dfad1"},
    {name="Geodude", chip_GUIDs = {"6eea88","d64518","c6d419","61ac7c"}, model_GUID = "df4771", spawn_effect="Physical Attack"},
    {name="Graveler", chip_GUIDs = {"a7a2dd"}, model_GUID = "f38eaa", spawn_effect="Physical Attack"},
    {name="Golem", chip_GUIDs = {"82b282","286ced"}, model_GUID = "aa0bdd", spawn_effect="Physical Attack"},
    {name="Ponyta", chip_GUIDs = {"84e7c5","389917"}, model_GUID = "a1a9ce"},
    {name="Rapidash", chip_GUIDs = {"875c77","1a13a1","3ec5b0","f4d963"}, model_GUID = "20403e"},
    {name="Slowpoke", chip_GUIDs = {"afcbfc"}, model_GUID = "f89c9b"},
    {name="Mega Slowbro", chip_GUIDs = {"dce282","48a051","88b8ad"}, model_GUID = "53ee2c", spawn_effect="Mega Evolve"},
    {name="Slowbro", chip_GUIDs = {"f2dc23","6abd9f","68a97a"}, model_GUID = "6cd8f3"},
    {name="Magnemite", chip_GUIDs = {"3b825a","ece545","870fc5","339677"}, model_GUID = "947e98"},
    {name="Magneton", chip_GUIDs = {"300a70","316b63","a8bf54","6c821e","78e572","2eb4b9"}, model_GUID = "f82ef9"},
    {name="Farfetchd", chip_GUIDs = {"48378d"}, model_GUID = "946c27", spawn_effect="Physical Attack"},
    {name="Doduo", chip_GUIDs = {"5842b7"}, model_GUID = "1bd0bc"},
    {name="Dodrio", chip_GUIDs = {"4e72c1"}, model_GUID = "299ecd"},
    {name="Seel", chip_GUIDs = {"719a88","6917b2"}, model_GUID = "954d89"},
    {name="Dewgong", chip_GUIDs = {"d26a9f","8a8be1","6d3b9c"}, model_GUID = "6fa3a7", spawn_effect="Physical Attack"},
    {name="Grimer", chip_GUIDs = {"4de9c9"}, model_GUID = "3e38eb"},
    {name="Muk", chip_GUIDs = {"10d5dd","96d753"}, model_GUID = "813b8b"},
    {name="Shellder", chip_GUIDs = {"9cf352","2a0620"}, model_GUID = "4be354", spawn_effect="Physical Attack"},
    {name="Cloyster", chip_GUIDs = {"20c05e","f4c8fe"}, model_GUID = "73e384"},
    {name="Gastly", chip_GUIDs = {"4a2b54","4565ec"}, model_GUID = "20085a"},
    {name="Haunter", chip_GUIDs = {"0ca367","82e477","0cac36","00ee6b","f16f30"}, model_GUID = "13b866"},
    {name="Gengar", chip_GUIDs = {"d67951","c6640d","603e56","cce813","990559","b2718f"}, model_GUID = "4638bc"},
    {name="Mega Gengar", chip_GUIDs = {"4c30c6","1e05f3","8b7e08","81de2e","0b8463","6c8979"}, model_GUID = "72228f", spawn_effect="Mega Evolve"},
    {name="Onix", chip_GUIDs = {"a0c1e2","333cd8","f9e711","f0cec9","71854b","f09093"}, model_GUID = "f5b6ad", custom_scale=0.7, offset={x=0,y=0,z=1.5}},
    {name="Drowzee", chip_GUIDs = {"220409"}, model_GUID = "2ceecd"},
    {name="Hypno", chip_GUIDs = {"4d1acb"}, model_GUID = "beca37"},
    {name="Krabby", chip_GUIDs = {"e96fe0"}, model_GUID = "75755a"},
    {name="Kingler", chip_GUIDs = {"60f374"}, model_GUID = "894758"},
    {name="Voltorb", chip_GUIDs = {"eac7c0","4c4f0d","161d4a"}, model_GUID = "37bc9d", spawn_effect="Physical Attack"},
    {name="Electrode", chip_GUIDs = {"91a484"}, model_GUID = "2fb88b", spawn_effect="Physical Attack"},
    {name="Exeggcute", chip_GUIDs = {"b8460a"}, model_GUID = "618cb8"},
    {name="Exeggutor", chip_GUIDs = {"296ac5","1c94bc"}, model_GUID = "cc6a34"},
    {name="Cubone", chip_GUIDs = {"567601"}, model_GUID = "e899ae", spawn_effect="Physical Attack"},
    {name="Marowak", chip_GUIDs = {"f0c3cd"}, model_GUID = "c9b2ac"},
    {name="Hitmonlee", chip_GUIDs = {"0a0b91","7d50a4","4621b6"}, model_GUID = "d821ab", spawn_effect="Physical Attack"},
    {name="Hitmonchan", chip_GUIDs = {"ac7850","c8700c","606332"}, model_GUID = "4fea7f", spawn_effect="Physical Attack"},
    {name="Lickitung", chip_GUIDs = {"d165a3"}, model_GUID = "d27c75"},
    {name="Koffing", chip_GUIDs = {"92af2c"}, model_GUID = "b853ca"},
    {name="Weezing", chip_GUIDs = {"6d1004","431a88","7fcce4","6f4fce"}, model_GUID = "921e94"},
    {name="Rhyhorn", chip_GUIDs = {"ced491","86e9c8"}, model_GUID = "99c208"},
    {name="Rhydon", chip_GUIDs = {"eb6bd7","3568f3"}, model_GUID = "2c20b7"},
    {name="Chansey", chip_GUIDs = {"2bd48a"}, model_GUID = "87d9f4"},
    {name="Tangela", chip_GUIDs = {"2883ef","817336"}, model_GUID = "9ff5ec"},
    {name="Kangaskhan", chip_GUIDs = {"a87a97"}, model_GUID = "98bc9e"},
    {name="Mega Kangaskhan", chip_GUIDs = {"fdbc51"}, model_GUID = "62422f", spawn_effect="Mega Evolve"},
    {name="Horsea", chip_GUIDs = {"0f4f3d"}, model_GUID = "16af3c"},
    {name="Seadra", chip_GUIDs = {"2ab066"}, model_GUID = "9c2ba7"},
    {name="Goldeen", chip_GUIDs = {"e767d1"}, model_GUID = "b633e8"},
    {name="Seaking", chip_GUIDs = {"5a64c5","48aa9b"}, model_GUID = "429324"},
    {name="Staryu", chip_GUIDs = {"4b9694","38b0b7"}, model_GUID = "792e94", spawn_effect="Physical Attack"},
    {name="Starmie", chip_GUIDs = {"076f02","e68806","60c518"}, model_GUID = "1072a5"},
    {name="Mr. Mime", chip_GUIDs = {"d2e30b","e3eb98","ef612b","33992c"}, model_GUID = "b25a93"},
    {name="Scyther", chip_GUIDs = {"d7867a","fb7e1e"}, model_GUID = "a98dea"},
    {name="Jynx", chip_GUIDs = {"b6f023","b2ce74","5738ba"}, model_GUID = "43c517"},
    {name="Electabuzz", chip_GUIDs = {"be16eb"}, model_GUID = "ad494f"},
    {name="Magmar", chip_GUIDs = {"fda8bd"}, model_GUID = "ff3210"},
    {name="Mega Pinsir", chip_GUIDs = {"241513"}, model_GUID = "9a4265", spawn_effect="Mega Evolve"},
    {name="Pinsir", chip_GUIDs = {"338900"}, model_GUID = "4e7512"},
    {name="Tauros", chip_GUIDs = {"53c6fa"}, model_GUID = "797eb7", spawn_effect="Physical Attack"},
    {name="Magikarp", chip_GUIDs = {"ccaba4"}, model_GUID = "48944e", spawn_effect="Physical Attack"},
    {name="Gyarados", chip_GUIDs = {"ba19f2","32a310","3ac45a","d8a599","b7146d"}, model_GUID = "bb17bd", custom_scale=0.8},
    {name="Mega Gyarados", chip_GUIDs = {"2f89d9","390914","91608f","e6606f","59bb32"}, model_GUID = "601ecc", spawn_effect="Mega Evolve", custom_scale=0.8},
    {name="Lapras", chip_GUIDs = {"eef944","8f51bd"}, model_GUID = "ae0d8f"},
    {name="Ditto", chip_GUIDs = {"b78095"}, model_GUID = "babb44"},
    {name="Eevee", chip_GUIDs = {"f5100d"}, model_GUID = "b37bcd"},
    {name="Vaporeon", chip_GUIDs = {"f482a3"}, model_GUID = "b8e8a6"},
    {name="Jolteon", chip_GUIDs = {"666f12","8cf7b6"}, model_GUID = "c86032"},
    {name="Flareon", chip_GUIDs = {"cc2002","e4c500","7a9f9c"}, model_GUID = "323c42"},
    {name="Porygon", chip_GUIDs = {"c2dea3","f07318"}, model_GUID = "7e5d3b"},
    {name="Omanyte", chip_GUIDs = {"a7c3b9"}, model_GUID = "9a4753"},
    {name="Omastar", chip_GUIDs = {"bc9191"}, model_GUID = "78ec0f"},
    {name="Kabuto", chip_GUIDs = {"a5c2f9"}, model_GUID = "f2a8a6"},
    {name="Kabutops", chip_GUIDs = {"acd4f3"}, model_GUID = "bd4880"},
    {name="Aerodactyl", chip_GUIDs = {"4dad54","cb99b1"}, model_GUID = "ba21f7"},
    {name="Mega Aerodactyl", chip_GUIDs = {"9aa94b","50f2d3"}, model_GUID = "411f15", spawn_effect="Mega Evolve"},
    {name="Snorlax", chip_GUIDs = {"5b29d5","1938a7"}, model_GUID = "829f1b", spawn_effect="Physical Attack"},
    {name="Articuno", chip_GUIDs = {"9ca881"}, model_GUID = "fd860a"},
    {name="Zapdos", chip_GUIDs = {"22907a"}, model_GUID = "0e0c94"},
    {name="Moltres", chip_GUIDs = {"7f84d6"}, model_GUID = "edf3cc", spawn_effect="Physical Attack"},
    {name="Dratini", chip_GUIDs = {"d2faa0"}, model_GUID = "1e6e11"},
    {name="Dragonair", chip_GUIDs = {"7ae9ab","1c082b","7181ba","a02647","a22180","9d01b6"}, model_GUID = "a88377"},
    {name="Dragonite", chip_GUIDs = {"556519","d23565"}, model_GUID = "fa47e5"},
    {name="Mega Mewtwo X", chip_GUIDs = {"320d31","19d894"}, model_GUID = "fb5dae", spawn_effect="Mega Evolve"},
    {name="Mega Mewtwo Y", chip_GUIDs = {"892fb5","817015"}, model_GUID = "18fa55", spawn_effect="Mega Evolve"},
    {name="Mewtwo", chip_GUIDs = {"2ff569","e19812"}, model_GUID = "8dad23"},
    {name="Mew", chip_GUIDs = {"7331d3"}, model_GUID = "5dbae0", spawn_effect="Physical Attack"},
    {name="Chikorita", chip_GUIDs = {"1229f1"}, model_GUID = "3d382c"},
    {name="Bayleef", chip_GUIDs = {"835e19"}, model_GUID = "d25fd0"},
    {name="Meganium", chip_GUIDs = {"4fafac","5dc48c"}, model_GUID = "574537"},
    {name="Cyndaquil", chip_GUIDs = {"298aaa"}, model_GUID = "75e0a9"},
    {name="Quilava", chip_GUIDs = {"e68b06"}, model_GUID = "103870"},
    {name="Typhlosion", chip_GUIDs = {"5ef8db","89816c"}, model_GUID = "ac8e3f"},
    {name="Totodile", chip_GUIDs = {"1ec714"}, model_GUID = "252f4b"},
    {name="Croconaw", chip_GUIDs = {"a7ea18"}, model_GUID = "4015b5"},
    {name="Feraligatr", chip_GUIDs = {"600f78","5fc66a"}, model_GUID = "e93ead", spawn_effect="Status Attack"},
    {name="Sentret", chip_GUIDs = {"e8a1d7"}, model_GUID = "9fb1dd"},
    {name="Furret", chip_GUIDs = {"a2fa4f"}, model_GUID = "ce5492", spawn_effect="Physical Attack"},
    {name="Hoothoot", chip_GUIDs = {"448cc2"}, model_GUID = "05738c"},
    {name="Noctowl", chip_GUIDs = {"40d511"}, model_GUID = "b09535"},
    {name="Ledyba", chip_GUIDs = {"5f9819"}, model_GUID = "3f5d68"},
    {name="Ledian", chip_GUIDs = {"aed4d3"}, model_GUID = "fa91c6"},
    {name="Spinarak", chip_GUIDs = {"196a54"}, model_GUID = "726bd6"},
    {name="Ariados", chip_GUIDs = {"b4e4fa","507d3f"}, model_GUID = "3dd86d"},
    {name="Crobat", chip_GUIDs = {"dba090","67a666","b8d849","c4d72f"}, model_GUID = "40d4cd"},
    {name="Chinchou", chip_GUIDs = {"a0d12a"}, model_GUID = "bb0ea1", spawn_effect="Physical Attack"},
    {name="Lanturn", chip_GUIDs = {"fd8631"}, model_GUID = "0a7fe3"},
    {name="Pichu", chip_GUIDs = {"14abfd"}, model_GUID = "a3f444", spawn_effect="Physical Attack"},
    {name="Cleffa", chip_GUIDs = {"92973d"}, model_GUID = "2b3bf8"},
    {name="Igglybuff", chip_GUIDs = {"5da792"}, model_GUID = "366974"},
    {name="Togepi", chip_GUIDs = {"c582fc"}, model_GUID = "6ba65d", spawn_effect="Physical Attack"},
    {name="Togetic", chip_GUIDs = {"2c8c20"}, model_GUID = "06393b"},
    {name="Natu", chip_GUIDs = {"e55a2d"}, model_GUID = "a511f7"},
    {name="Xatu", chip_GUIDs = {"bc813d","2e8caf"}, model_GUID = "a72b40"},
    {name="Mareep", chip_GUIDs = {"8d19e9"}, model_GUID = "2d0cf1"},
    {name="Flaffy", chip_GUIDs = {"fe67f2"}, model_GUID = "332cde"},
    {name="Ampharos", chip_GUIDs = {"33a0ec"}, model_GUID = "2fede5"},
    {name="Mega Ampharos", chip_GUIDs = {"1cdaec"}, model_GUID = "333b22", spawn_effect="Mega Evolve"},
    {name="Bellossom", chip_GUIDs = {"be8756"}, model_GUID = "29999f", spawn_effect="Physical Attack"},
    {name="Marill", chip_GUIDs = {"e5d7d0"}, model_GUID = "c97a48"},
    {name="Azumarill", chip_GUIDs = {"ea21c2"}, model_GUID = "f7eb12"},
    {name="Sudowoodo", chip_GUIDs = {"c57473"}, model_GUID = "b0f59f", spawn_effect="Physical Attack"},
    {name="Politoed", chip_GUIDs = {"37d452"}, model_GUID = "6bf45b"},
    {name="Hoppip", chip_GUIDs = {"b66e2b"}, model_GUID = "76212b"},
    {name="Skiploom", chip_GUIDs = {"7b2399"}, model_GUID = "4e9313", spawn_effect="Physical Attack"},
    {name="Jumpluff", chip_GUIDs = {"3b7504","a608b2"}, model_GUID = "08d23f", spawn_effect="Physical Attack"},
    {name="Aipom", chip_GUIDs = {"2863df"}, model_GUID = "89ec0d"},
    {name="Sunkern", chip_GUIDs = {"50321e"}, model_GUID = "21d5b5"},
    {name="Sunflora", chip_GUIDs = {"b52cc6"}, model_GUID = "912dc4"},
    {name="Yanma", chip_GUIDs = {"f80411"}, model_GUID = "4b8bdf", spawn_effect="Physical Attack"},
    {name="Wooper", chip_GUIDs = {"4cf99c"}, model_GUID = "5a0e02"},
    {name="Quagsire", chip_GUIDs = {"737ce8","dba0e6"}, model_GUID = "59c07d"},
    {name="Espeon", chip_GUIDs = {"a9cfeb","9fd45f"}, model_GUID = "c8a52c"},
    {name="Umbreon", chip_GUIDs = {"41a1e1","63e30b"}, model_GUID = "33d105"},
    {name="Murkrow", chip_GUIDs = {"02dc6c","a234bc"}, model_GUID = "77f5c2"},
    {name="Slowking", chip_GUIDs = {"69f2a7","17adef"}, model_GUID = "bcb9f2"},
    {name="Misdreavus", chip_GUIDs = {"a2ea4a"}, model_GUID = "01ca5e", spawn_effect="Physical Attack"},
    {name="Unown", chip_GUIDs = {"eee0d0"}, model_GUID = nil, spawn_effect="Hidden Power", states = {
        {name="Unown A", model_GUID = "d04170"},
        {name="Unown B", model_GUID = "ed76de"},
        {name="Unown C", model_GUID = "c6665d"},
        {name="Unown D", model_GUID = "35372a"},
        {name="Unown E", model_GUID = "91e3d3"},
        {name="Unown F", model_GUID = "df3c86"},
        {name="Unown G", model_GUID = "886092"},
        {name="Unown H", model_GUID = "26c1c8"},
        {name="Unown I", model_GUID = "d34c25"},
        {name="Unown J", model_GUID = "004500"},
        {name="Unown K", model_GUID = "eb5606"},
        {name="Unown L", model_GUID = "3a8530"},
        {name="Unown M", model_GUID = "fc0939"},
        {name="Unown N", model_GUID = "b9e65b"},
        {name="Unown O", model_GUID = "c85a56"},
        {name="Unown P", model_GUID = "216802"},
        {name="Unown Q", model_GUID = "914553"},
        {name="Unown R", model_GUID = "dae40e"},
        {name="Unown S", model_GUID = "9f4cfd"},
        {name="Unown T", model_GUID = "d0274b"},
        {name="Unown U", model_GUID = "55291d"},
        {name="Unown V", model_GUID = "c36683"},
        {name="Unown W", model_GUID = "6e22b3"},
        {name="Unown X", model_GUID = "c400a0"},
        {name="Unown Y", model_GUID = "4e9ce4"},
        {name="Unown Z", model_GUID = "665d7b"},
        {name="Unown Quest", model_GUID = "23760d"},
        {name="Unown Excl", model_GUID = "4566d0"}
    }},
    {name="Wobbuffet", chip_GUIDs = {"7c1946","b77826"}, model_GUID = "8b6825"},
    {name="Girafarig", chip_GUIDs = {"e82d7f"}, model_GUID = "3310c2"},
    {name="Pineco", chip_GUIDs = {"dd9bdf"}, model_GUID = "75f82e", spawn_effect="Physical Attack"},
    {name="Forretress", chip_GUIDs = {"b17665","fc50aa"}, model_GUID = "6fe49a"},
    {name="Dunsparce", chip_GUIDs = {"a05801"}, model_GUID = "13a6d1"},
    {name="Gligar", chip_GUIDs = {"19a07f"}, model_GUID = "9c099b"},
    {name="Mega Steelix", chip_GUIDs = {"6c97c3","e8dbd7","392b2e"}, model_GUID = "f5a482", spawn_effect="Physical Attack", offset={x=0,y=0,z=2.5}, custom_scale=0.7},
    {name="Steelix", chip_GUIDs = {"9f5e77","3986a8","e6be64"}, model_GUID = "a4d334", custom_scale=0.7},
    {name="Snubbull", chip_GUIDs = {"1927ee"}, model_GUID = "9de6f6"},
    {name="Granbull", chip_GUIDs = {"0ec65e"}, model_GUID = "bf082d"},
    {name="Qwilfish", chip_GUIDs = {"dc78c9"}, model_GUID = "4dc2b1"},
    {name="Mega Scizor", chip_GUIDs = {"be154b","88e634","3e3674","cb729f"}, model_GUID = "4175df", spawn_effect="Mega Evolve"},
    {name="Scizor", chip_GUIDs = {"69f1c8","6370df","51f327","cb03c5"}, model_GUID = "334cc6", spawn_effect="Status Attack"},
    {name="Shuckle", chip_GUIDs = {"ac1be0"}, model_GUID = "53a551", spawn_effect="Physical Attack"},
    {name="Heracross", chip_GUIDs = {"977900","997268","e6d185"}, model_GUID = "11f35c"},
    {name="Mega Heracross", chip_GUIDs = {"a60949","15a156","bae388"}, model_GUID = "cbdd77", spawn_effect="Mega Evolve"},
    {name="Sneasel", chip_GUIDs = {"91b0db","52ccfe"}, model_GUID = "b0f068"},
    {name="Teddiursa", chip_GUIDs = {"9e8972"}, model_GUID = "729770"},
    {name="Ursaring", chip_GUIDs = {"506f26"}, model_GUID = "aa222b"},
    {name="Slugma", chip_GUIDs = {"aa56ab","02729e","fd8e73"}, model_GUID = "964d48"},
    {name="Magcargo", chip_GUIDs = {"75fbb8"}, model_GUID = "bc7776"},
    {name="Swinub", chip_GUIDs = {"913580"}, model_GUID = "a9fba7", spawn_effect="Physical Attack"},
    {name="Piloswine", chip_GUIDs = {"ef3ca8","f324f0","2a8944"}, model_GUID = "3aabb9"},
    {name="Corsola", chip_GUIDs = {"ae941f"}, model_GUID = "cf84d4", spawn_effect="Physical Attack"},
    {name="Remoraid", chip_GUIDs = {"9cb32c"}, model_GUID = "b820cf"},
    {name="Octillery", chip_GUIDs = {"3753f9"}, model_GUID = "4c27f5"},
    {name="Delibird", chip_GUIDs = {"b44a03"}, model_GUID = "a62b3b", spawn_effect="Physical Attack"},
    {name="Mantine", chip_GUIDs = {"d0d914"}, model_GUID = "5f4b59", spawn_effect="Physical Attack"},
    {name="Skarmory", chip_GUIDs = {"2b35e0","84c974","a24aaf","4c7511"}, model_GUID = "790101", spawn_effect="Physical Attack"},
    {name="Houndour", chip_GUIDs = {"4bcd2c"}, model_GUID = "b635fd"},
    {name="Houndoom", chip_GUIDs = {"583ec5","d6c969","6d9b78"}, model_GUID = "875a93"},
    {name="Mega Houndoom", chip_GUIDs = {"c1204c","ad7f9a","e2360a"}, model_GUID = "acbb9d", spawn_effect="Mega Evolve"},
    {name="Kingdra", chip_GUIDs = {"503dc5","d63925"}, model_GUID = "fd04bb"},
    {name="Phanpy", chip_GUIDs = {"89ff4f"}, model_GUID = "1f0941"},
    {name="Donphan", chip_GUIDs = {"9d03f0"}, model_GUID = "7fa132", spawn_effect="Physical Attack"},
    {name="Porygon 2", chip_GUIDs = {"53766a"}, model_GUID = "26714f"},
    {name="Stantler", chip_GUIDs = {"a61c88"}, model_GUID = "fc0116"},
    {name="Smeargle", chip_GUIDs = {"094941"}, model_GUID = "5efe03"},
    {name="Tyrogue", chip_GUIDs = {"a7cf30"}, model_GUID = "9f3f98"},
    {name="Hitmontop", chip_GUIDs = {"32edcf","8fa5f2"}, model_GUID = "f2168b", spawn_effect="Physical Attack"},
    {name="Smoochum", chip_GUIDs = {"9fc7a5"}, model_GUID = "bb561b"},
    {name="Elekid", chip_GUIDs = {"76dee2"}, model_GUID = "9b06da"},
    {name="Magby", chip_GUIDs = {"67d958"}, model_GUID = "1649bb"},
    {name="Miltank", chip_GUIDs = {"e7d721","676498"}, model_GUID = "cd7e0e", spawn_effect="Physical Attack"},
    {name="Blissey", chip_GUIDs = {"c4152d"}, model_GUID = "7c05e0", spawn_effect="Status Attack"},
    {name="Raiku", chip_GUIDs = {"948e8e"}, model_GUID = "e1797a"},
    {name="Entei", chip_GUIDs = {"d2291a"}, model_GUID = "690ea9"},
    {name="Suicune", chip_GUIDs = {"92d6db"}, model_GUID = "7829ea"},
    {name="Larvitar", chip_GUIDs = {"aa8b81"}, model_GUID = "4f2d64"},
    {name="Pupitar", chip_GUIDs = {"ab7355"}, model_GUID = "772458"},
    {name="Mega Tyranitar", chip_GUIDs = {"1e55ba"}, model_GUID = "0bd382", spawn_effect="Mega Evolve"},
    {name="Tyranitar", chip_GUIDs = {"9ff2df"}, model_GUID = "46679d"},
    {name="Lugia", chip_GUIDs = {"b9817d"}, model_GUID = "a1d05e"},
    {name="Ho-Oh", chip_GUIDs = {"8bc973"}, model_GUID = "7be9e1"},
    {name="Celebi", chip_GUIDs = {"e0b472"}, model_GUID = "981fe7"},
	{name="Treecko", chip_GUIDs = {"08ed50"}, model_GUID = "0fb049"},
	{name="Grovyle", chip_GUIDs = {"0ff12d"}, model_GUID = "7c33a8"},
	{name="Mega Sceptile", chip_GUIDs = {"85a636"}, model_GUID = "d3060f", spawn_effect="Mega Evolve"},
	{name="Sceptile", chip_GUIDs = {"f28cf5"}, model_GUID = "cb8b63", spawn_effect="Status Attack"},
	{name="Torchic", chip_GUIDs = {"875074"}, model_GUID = "215780", spawn_effect="Physical Attack"},
	{name="Combusken", chip_GUIDs = {"c6a11b"}, model_GUID = "87fbb3", spawn_effect="Physical Attack"},
	{name="Blaziken", chip_GUIDs = {"155b75"}, model_GUID = "868292", spawn_effect="Status Attack"},
	{name="Mega Blaziken", chip_GUIDs = {"f8f265"}, model_GUID = "54893e", spawn_effect="Mega Evolve"},
	{name="Mudkip", chip_GUIDs = {"8106ee"}, model_GUID = "7bb712"},
	{name="Marshtomp", chip_GUIDs = {"639ce9"}, model_GUID = "e7b84a"},
	{name="Swampert", chip_GUIDs = {"20e951"}, model_GUID = "215a22", spawn_effect="Physical Attack"},
    {name="Mega Swampert", chip_GUIDs = {"a3336c"}, model_GUID = "436a81", spawn_effect="Mega Evolve"},
	{name="Poochyena", chip_GUIDs = {"1810ce"}, model_GUID = "0266ca"},
	{name="Mightyena", chip_GUIDs = {"8bbc69","db96fa"}, model_GUID = "4e25db"},
	{name="Zigzagoon", chip_GUIDs = {"fff6a8"}, model_GUID = "fa2eb0"},
	{name="Linoone", chip_GUIDs = {"0c4475"}, model_GUID = "1fbab6"},
	{name="Wurmple", chip_GUIDs = {"668ac1"}, model_GUID = "564350"},
	{name="Silcoon", chip_GUIDs = {"165d9f"}, model_GUID = "7b3acf", spawn_effect="Physical Attack"},
	{name="Beautifly", chip_GUIDs = {"e9c5eb"}, model_GUID = "96fd4b"},
	{name="Cascoon", chip_GUIDs = {"b3b9c3"}, model_GUID = "8e5dc9", spawn_effect="Physical Attack"},
	{name="Dustox", chip_GUIDs = {"34b298"}, model_GUID = "5e829e"},
	{name="Lotad", chip_GUIDs = {"087645"}, model_GUID = "9bf1cf"},
	{name="Lombre", chip_GUIDs = {"c50264"}, model_GUID = "f664d4"},
	{name="Ludicolo", chip_GUIDs = {"440bb6"}, model_GUID = "bb247f"},
	{name="Seedot", chip_GUIDs = {"047a95"}, model_GUID = "a55367"},
	{name="Nuzleaf", chip_GUIDs = {"4b56b0"}, model_GUID = "807cc5", spawn_effect="Physical Attack"},
	{name="Shiftry", chip_GUIDs = {"bd5174","9ba92e"}, model_GUID = "a97e03"},
	{name="Taillow", chip_GUIDs = {"20dd03"}, model_GUID = "f9efda"},
	{name="Swellow", chip_GUIDs = {"ced45f","996d11"}, model_GUID = "430ab8", spawn_effect="Physical Attack"},
	{name="Wingull", chip_GUIDs = {"357307"}, model_GUID = "2ba1b1"},
	{name="Pelipper", chip_GUIDs = {"7d91ab","c050a3"}, model_GUID = "dff0ed"},
	{name="Ralts", chip_GUIDs = {"84d05c"}, model_GUID = "3240f8"},
	{name="Kirlia", chip_GUIDs = {"283278"}, model_GUID = "9624b5", spawn_effect="Physical Attack"},
	{name="Gardevoir", chip_GUIDs = {"2f7e30","555030","53a468"}, model_GUID = "8c5fec"},
	{name="Mega Gardevoir", chip_GUIDs = {"aa433e","7f8e1e","13bd56"}, model_GUID = "17dce9", spawn_effect="Mega Evolve"},
	{name="Surskit", chip_GUIDs = {"fcba46","00a202"}, model_GUID = "15b02f"},
	{name="Masquerain", chip_GUIDs = {"ed43e4"}, model_GUID = "f71175"},
	{name="Shroomish", chip_GUIDs = {"b212c5"}, model_GUID = "f9ef26"},
	{name="Breloom", chip_GUIDs = {"a290f5"}, model_GUID = "d12809"},
	{name="Slakoth", chip_GUIDs = {"70293c"}, model_GUID = "33417e"},
	{name="Vigoroth", chip_GUIDs = {"593cbb","1445c2","d61ac2"}, model_GUID = "4d9117"},
	{name="Slaking", chip_GUIDs = {"98980b","f38368"}, model_GUID = "66bcca"},
	{name="Nincada", chip_GUIDs = {"0ffcd4"}, model_GUID = "17a5c0"},
	{name="Ninjask", chip_GUIDs = {"a96550"}, model_GUID = "ae5abc"},
	{name="Shedinja", chip_GUIDs = {"c9223f"}, model_GUID = "ceaedf"},
	{name="Whismur", chip_GUIDs = {"17eabc"}, model_GUID = "c4ec15"},
	{name="Loudred", chip_GUIDs = {"407ab8"}, model_GUID = "d404f1"},
	{name="Exploud", chip_GUIDs = {"2da74d"}, model_GUID = "1567e6"},
	{name="Makuhita", chip_GUIDs = {"b9a996","9af7f5","5e3bc1"}, model_GUID = "b12e69"},
	{name="Hariyama", chip_GUIDs = {"8236de"}, model_GUID = "25ea1f"},
	{name="Azurill", chip_GUIDs = {"1a66e0"}, model_GUID = "3c11df"},
	{name="Nosepass", chip_GUIDs = {"ba7b8e","957b40","2e9d6d"}, model_GUID = "ef03a1"},
	{name="Skitty", chip_GUIDs = {"0a09a4"}, model_GUID = "701147"},
	{name="Delcatty", chip_GUIDs = {"e7d163","0ceef9"}, model_GUID = "aa3ec0"},
	{name="Mega Sableye", chip_GUIDs = {"116990","33a604","41162b","d634fb"}, model_GUID = "6f99c4", spawn_effect="Physical Attack"},
	{name="Sableye", chip_GUIDs = {"565357","c7a843","2ea062","7063ab"}, model_GUID = "184d06", spawn_effect="Physical Attack"},
	{name="Mawile", chip_GUIDs = {"0d01c6","2e550e","8451d3"}, model_GUID = "71f869"},
	{name="Mega Mawile", chip_GUIDs = {"35154e","a0759e"}, model_GUID = "de3150", spawn_effect="Mega Evolve"},
	{name="Aron", chip_GUIDs = {"789837"}, model_GUID = "156c85", spawn_effect="Physical Attack"},
	{name="Lairon", chip_GUIDs = {"507bb3"}, model_GUID = "d48d21"},
	{name="Aggron", chip_GUIDs = {"450818"}, model_GUID = "ce1109"},
	{name="Mega Aggron", chip_GUIDs = {"da61d2"}, model_GUID = "103b64", spawn_effect="Mega Evolve"},
	{name="Meditite", chip_GUIDs = {"227a21","3d77fa"}, model_GUID = "da149f"},
	{name="Medicham", chip_GUIDs = {"b3e93b"}, model_GUID = "1e9b7e"},
	{name="Mega Medicham", chip_GUIDs = {"2b1648"}, model_GUID = "2c439f", spawn_effect="Mega Evolve"},
	{name="Electrike", chip_GUIDs = {"869f14"}, model_GUID = "b0e819"},
	{name="Manectric", chip_GUIDs = {"294e8b"}, model_GUID = "27ca2d"},
	{name="Mega Manectric", chip_GUIDs = {"7e0705"}, model_GUID = "f4b774", spawn_effect="Mega Evolve"},
	{name="Plusle", chip_GUIDs = {"75b2d4"}, model_GUID = "1ff9eb"},
	{name="Minun", chip_GUIDs = {"234b32"}, model_GUID = "401c47"},
	{name="Volbeat", chip_GUIDs = {"cb7669"}, model_GUID = "65b9bd"},
	{name="Illumise", chip_GUIDs = {"ecec5a"}, model_GUID = "0f073f"},
	{name="Roselia", chip_GUIDs = {"315ce1","579d1f"}, model_GUID = "6b1bd7"},
	{name="Gulpin", chip_GUIDs = {"af3f12"}, model_GUID = "933c2f"},
	{name="Swalot", chip_GUIDs = {"659359"}, model_GUID = "f5cf00"},
	{name="Carvanha", chip_GUIDs = {"6b19a0"}, model_GUID = "1dd6cf", spawn_effect="Physical Attack"},
	{name="Mega Sharpedo", chip_GUIDs = {"41e955","689860"}, model_GUID = "16ed43", spawn_effect="Mega Evolve"},
	{name="Sharpedo", chip_GUIDs = {"f14013","0a8a75"}, model_GUID = "129bd7", spawn_effect="Physical Attack"},
	{name="Wailmer", chip_GUIDs = {"687781"}, model_GUID = "b372d4"},
	{name="Wailord", chip_GUIDs = {"a7b75f"}, model_GUID = "8bc97a", custom_scale=0.3},
	{name="Numel", chip_GUIDs = {"269a4e"}, model_GUID = "d8750f", spawn_effect="Physical Attack"},
	{name="Camerupt", chip_GUIDs = {"f473d3"}, model_GUID = "f266b1", spawn_effect="Physical Attack"},
	{name="Mega Camerupt", chip_GUIDs = {"7e00fb"}, model_GUID = "05b840", spawn_effect="Mega Evolve"},
	{name="Torkoal", chip_GUIDs = {"d07bba","62e840","b820d6"}, model_GUID = "ad6f78", spawn_effect="Physical Attack"},
	{name="Spoink", chip_GUIDs = {"fa7035"}, model_GUID = "a84666"},
	{name="Grumpig", chip_GUIDs = {"732ee0"}, model_GUID = "fb1029"},
	{name="Spinda", chip_GUIDs = {"73eedb"}, model_GUID = "e3708f"},
	{name="Trapinch", chip_GUIDs = {"daaa5f"}, model_GUID = "835c79"},
	{name="Vibrava", chip_GUIDs = {"733635"}, model_GUID = "944da2"},
	{name="Flygon", chip_GUIDs = {"3ec094","315afe","f024f1","d8185a"}, model_GUID = "c8d0b6"},
	{name="Cacnea", chip_GUIDs = {"63c60b"}, model_GUID = "c6b9de", spawn_effect="Physical Attack"},
	{name="Cacturne", chip_GUIDs = {"8b51b9","727ff9"}, model_GUID = "3a509b"},
	{name="Swablu", chip_GUIDs = {"74a15e"}, model_GUID = "7b086f"},
	{name="Altaria", chip_GUIDs = {"d09e95","8c2e66","34edd0","f876f7","09dd82"}, model_GUID = "9a2210"},
	{name="Mega Altaria", chip_GUIDs = {"d41d32","601795","e66565","fbd862","7d32ff"}, model_GUID = "f74c1f", spawn_effect="Mega Evolve"},
	{name="Zangoose", chip_GUIDs = {"711d19"}, model_GUID = "2a763c"},
	{name="Seviper", chip_GUIDs = {"51e908"}, model_GUID = "684d0d", spawn_effect="Physical Attack"},
	{name="Lunatone", chip_GUIDs = {"328145","44510e"}, model_GUID = "de6094"},
	{name="Solrock", chip_GUIDs = {"dde416","e019be"}, model_GUID = "12e752"},
	{name="Barboach", chip_GUIDs = {"c3f0d8"}, model_GUID = "5db694"},
	{name="Whiscash", chip_GUIDs = {"31aeab","ad34e7","4900c7"}, model_GUID = "08dca8"},
	{name="Corphish", chip_GUIDs = {"55d36c"}, model_GUID = "7fe1a7"},
	{name="Crawdaunt", chip_GUIDs = {"02dc1a"}, model_GUID = "e834ac"},
	{name="Baltoy", chip_GUIDs = {"8fc0dc"}, model_GUID = "9df0cf", spawn_effect="Physical Attack"},
	{name="Claydol", chip_GUIDs = {"1de2e9"}, model_GUID = "af6fb3", spawn_effect="Physical Attack"},
	{name="Lileep", chip_GUIDs = {"62927e"}, model_GUID = "77fdf5"},
	{name="Cradily", chip_GUIDs = {"cd88e2"}, model_GUID = "a8c907"},
	{name="Anorith", chip_GUIDs = {"d7d583"}, model_GUID = "5b5bf7"},
	{name="Armaldo", chip_GUIDs = {"ecf483"}, model_GUID = "812ce0"},
	{name="Feebas", chip_GUIDs = {"47ae4b"}, model_GUID = "e88279"},
	{name="Milotic", chip_GUIDs = {"8a7abb","feea9c"}, model_GUID = "ab980b", spawn_effect="Physical Attack"},
	{name="Castform", chip_GUIDs = {"b0151e"}, model_GUID = nil, persistent_state=false, states={
    	{name="Castform Base", model_GUID = "80fd3b"},
    	{name="Rainy Castform", model_GUID = "f710a9"},
    	{name="Snowy Castform", model_GUID = "bd28db"},
    	{name="Sunny Castform", model_GUID = "e9d2d3"}
    }},
	{name="Kecleon", chip_GUIDs = {"3813be"}, model_GUID = "223e93", spawn_effect="Physical Attack"},
	{name="Shuppet", chip_GUIDs = {"01f818"}, model_GUID = "44e392", spawn_effect="Physical Attack"},
	{name="Banette", chip_GUIDs = {"66be1e","f95283"}, model_GUID = "36b0e4"},
	{name="Mega Banette", chip_GUIDs = {"7b8b5a","95e279"}, model_GUID = "cd84f5", spawn_effect="Mega Evolve"},
	{name="Duskull", chip_GUIDs = {"e241f9","e679af"}, model_GUID = "34ab19"},
	{name="Dusclops", chip_GUIDs = {"9ded8b","a92934","3e388a"}, model_GUID = "24a945"},
	{name="Tropius", chip_GUIDs = {"4e8b7e"}, model_GUID = "bdb162"},
	{name="Chimecho", chip_GUIDs = {"01476b"}, model_GUID = "85d340"},
	{name="Absol", chip_GUIDs = {"260602","12dd23"}, model_GUID = "4f2184"},
	{name="Mega Absol", chip_GUIDs = {"b0392f","cbffee"}, model_GUID = "ac5e55", spawn_effect="Mega Evolve"},
	{name="Wynaut", chip_GUIDs = {"29cead"}, model_GUID = "c16e96"},
	{name="Snorunt", chip_GUIDs = {"b93e66"}, model_GUID = "f8ae98"},
	{name="Glalie", chip_GUIDs = {"e32a7c","27713b","a1f1db"}, model_GUID = "95fc87"},
	{name="Mega Glalie", chip_GUIDs = {"7d90e0","a94f4a","6afa65"}, model_GUID = "f84e20", spawn_effect="Mega Evolve"},
	{name="Spheal", chip_GUIDs = {"c73d67"}, model_GUID = "d81c2a", spawn_effect="Physical Attack"},
	{name="Sealeo", chip_GUIDs = {"6b8be8","a950d9","946543","5448b2"}, model_GUID = "9a0533"},
	{name="Walrein", chip_GUIDs = {"390d22","d2483f"}, model_GUID = "74dec1"},
	{name="Clamperl", chip_GUIDs = {"fdd011"}, model_GUID = "b7775d"},
	{name="Huntail", chip_GUIDs = {"d9bbfd"}, model_GUID = "3a571e"},
	{name="Gorebyss", chip_GUIDs = {"dcd689"}, model_GUID = "377c59", spawn_effect="Physical Attack"},
	{name="Relicanth", chip_GUIDs = {"b83501","1430ca"}, model_GUID = "18af25"},
	{name="Luvdisc", chip_GUIDs = {"077c3f","189b18"}, model_GUID = "4b04ed", spawn_effect="Physical Attack"},
	{name="Bagon", chip_GUIDs = {"7270c1"}, model_GUID = "36437d"},
	{name="Shelgon", chip_GUIDs = {"03d3fe","406ed6"}, model_GUID = "261dc5", spawn_effect="Physical Attack"},
	{name="Mega Salamence", chip_GUIDs = {"5335be","53bc1d"}, model_GUID = "ed6dfd", spawn_effect="Mega Evolve"},
	{name="Salamence", chip_GUIDs = {"c0b795","a96a2f"}, model_GUID = "32c729"},
	{name="Beldum", chip_GUIDs = {"82ae92"}, model_GUID = "49faaa"},
	{name="Metang", chip_GUIDs = {"4a4175"}, model_GUID = "11e387"},
	{name="Mega Metagross", chip_GUIDs = {"eb748b","b2ea83"}, model_GUID = "501574", spawn_effect="Mega Evolve"},
	{name="Metagross", chip_GUIDs = {"49ecf3","691dcf"}, model_GUID = "69d7a3", spawn_effect="Status Attack"},
	{name="Regirock", chip_GUIDs = {"1909cd"}, model_GUID = "a0b835"},
	{name="Regice", chip_GUIDs = {"7abbf2"}, model_GUID = "8864ef"},
	{name="Registeel", chip_GUIDs = {"d5fe55"}, model_GUID = "7a9464", spawn_effect="Status Attack"},
	{name="Latias", chip_GUIDs = {"775d4a"}, model_GUID = "ad2b2b"},
	{name="Mega Latias", chip_GUIDs = {"11e432"}, model_GUID = "f182f8", spawn_effect="Mega Evolve"},
	{name="Latios", chip_GUIDs = {"93e0a7"}, model_GUID = "bbe9d2"},
	{name="Mega Latios", chip_GUIDs = {"329c64"}, model_GUID = "77ff84", spawn_effect="Mega Evolve"},
	{name="Kyogre", chip_GUIDs = {"682955"}, model_GUID = "a7e721"},
	{name="Primal Kyogre", chip_GUIDs = {"89e7c2"}, model_GUID = "ec46fa", spawn_effect="Primal Evolution", custom_scale=0.5},
	{name="Groudon", chip_GUIDs = {"8848e6"}, model_GUID = "2bba90"},
	{name="Primal Groudon", chip_GUIDs = {"66a041"}, model_GUID = "0e9a71", spawn_effect="Primal Evolution", custom_scale=0.5},
	{name="Mega Rayquaza", chip_GUIDs = {"e3bfc5"}, model_GUID = "147c09", spawn_effect="Mega Evolve", custom_scale=0.7},
	{name="Rayquaza", chip_GUIDs = {"84c146"}, model_GUID = "265715"},
	{name="Jirachi", chip_GUIDs = {"412e0f"}, model_GUID = "3df5b6"},
	{name="Deoxys", chip_GUIDs = {"40cded"}, model_GUID = nil, persistent_state=false, states = {
    	{name="Base Deoxys", model_GUID = "87e928", spawn_effect="Status Attack"},
    	{name="Attack Deoxys", model_GUID = "f020bf", spawn_effect="Physical Attack"},
    	{name="Defense Deoxys", model_GUID = "f257b1", spawn_effect="Status Attack"},
    	{name="Speed Deoxys", model_GUID = "a1a235", spawn_effect="Physical Attack"}
    }},
    {name="Turtwig", chip_GUIDs = {"da752f","93f603"}, model_GUID = "02eeef"},
    {name="Grotle", chip_GUIDs = {"2a9ff9"}, model_GUID = "89af8c"},
    {name="Torterra", chip_GUIDs = {"e09b1b","db8953"}, model_GUID = "542b09"},
    {name="Chimchar", chip_GUIDs = {"57a9c0"}, model_GUID = "cc51f2", spawn_effect="Physical Attack"},
    {name="Monferno", chip_GUIDs = {"2ea484"}, model_GUID = "c2e9c2", spawn_effect="Physical Attack"},
    {name="Infernape", chip_GUIDs = {"1065ab","e3c49b","f9b8cd"}, model_GUID = "b04364"},
    {name="Piplup", chip_GUIDs = {"1bd5fd"}, model_GUID = "c9b643"},
    {name="Prinplup", chip_GUIDs = {"5d22fa"}, model_GUID = "7931f6"},
    {name="Empoleon", chip_GUIDs = {"a737c1","262037"}, model_GUID = "85b6c8", spawn_effect="Status Attack"},
    {name="Starly", chip_GUIDs = {"435b24"}, model_GUID = "befb97"},
    {name="Staravia", chip_GUIDs = {"d67cb6"}, model_GUID = "2ea0b8", spawn_effect="Physical Attack"},
    {name="Staraptor", chip_GUIDs = {"8590bb","2bd04c"}, model_GUID = "88b32f"},
    {name="Bidoof", chip_GUIDs = {"e88f00"}, model_GUID = "9cf4d2", spawn_effect="Physical Attack"},
    {name="Bibarel", chip_GUIDs = {"b7e0e6"}, model_GUID = "8d4c2c"},
    {name="Kricketot", chip_GUIDs = {"534255"}, model_GUID = "196500"},
    {name="Kricketune", chip_GUIDs = {"6d22e5"}, model_GUID = "7d4ba3"},
    {name="Shinx", chip_GUIDs = {"b0d3a6"}, model_GUID = "19c7df"},
    {name="Luxio", chip_GUIDs = {"af05f8"}, model_GUID = "6a78f1"},
    {name="Luxray", chip_GUIDs = {"b30226","1bbc40"}, model_GUID = "2fe437"},
    {name="Budew", chip_GUIDs = {"e9d405"}, model_GUID = "b6d264"},
    {name="Roserade", chip_GUIDs = {"b010a9","a0da5e"}, model_GUID = "62a471", spawn_effect="Physical Attack"},
    {name="Cranidos", chip_GUIDs = {"a8b3e6","32adcd"}, model_GUID = "c3204c", spawn_effect="Physical Attack"},
    {name="Rampardos", chip_GUIDs = {"861506"}, model_GUID = "163e32", spawn_effect="Physical Attack"},
    {name="Shieldon", chip_GUIDs = {"6964e1"}, model_GUID = "1d508b"},
    {name="Bastiodon", chip_GUIDs = {"f7eeb1","9bac88"}, model_GUID = "9d8d6d"},
    {name="Burmy", chip_GUIDs = {"e922c6"}, model_GUID = nil, persistent_state=false, states = {
        {name="Burmy Plant", model_GUID = "507709"},
        {name="Burmy Sandy", model_GUID = "2d9ce9"},
        {name="Burmy Trash", model_GUID = "fda7da"}
    }},
    {name="Wormadam", chip_GUIDs = {"5a7e77"}, model_GUID = nil, persistent_state=false, states = {
        {name="Wormadam Plant", model_GUID = "b143f6"},
        {name="Wormadam Sandy", model_GUID = "a52d8f"},
        {name="Wormadam Trash", model_GUID = "56eb94"}
    }},
    {name="Mothim", chip_GUIDs = {"55b2b0"}, model_GUID = "687bf9"},
    {name="Combee", chip_GUIDs = {"6166de"}, model_GUID = "2b2090"},
    {name="Vespiquen", chip_GUIDs = {"9d4677","59a2e6"}, model_GUID = "5ab244"},
    {name="Pachirisu", chip_GUIDs = {"bce90b"}, model_GUID = "9045db"},
    {name="Buizel", chip_GUIDs = {"b746e5"}, model_GUID = "a6a1a5"},
    {name="Floatzel", chip_GUIDs = {"1a875e","936aed","6955d4"}, model_GUID = "136547"},
    {name="Cherubi", chip_GUIDs = {"631c4d"}, model_GUID = "a6957e"},
    {name="Cherrim", chip_GUIDs = {"ef8bdd","7bd97e"}, model_GUID = nil, persistent_state=false, states = {
        {name="Overcast Cherrim", model_GUID = "bad089"},
        {name="Sunshine Cherrim", model_GUID = "d4aaec", spawn_effect="Physical Attack"}
    }},
    {name="Shellos", chip_GUIDs = {"971698"}, model_GUID = nil, states = {
        {name="Shellos East", model_GUID = "a02a0e"},
        {name="Shellos West", model_GUID = "18c1c1"}
    }},
    {name="Gastrodon", chip_GUIDs = {"cc3af5","65f0ca"}, model_GUID = nil, spawn_effect="Physical Attack", states = {
        {name="Gastrodon East", model_GUID = "a2bd5c"},
        {name="Gastrodon West", model_GUID = "c75552"}
    }},
    {name="Ambipom", chip_GUIDs = {"0d6466"}, model_GUID = "c47da4"},
    {name="Drifloon", chip_GUIDs = {"4835dd"}, model_GUID = "d98126", spawn_effect="Physical Attack"},
    {name="Drifblim", chip_GUIDs = {"6ba8b8","d79158"}, model_GUID = "6fb952", spawn_effect="Physical Attack"},
    {name="Buneary", chip_GUIDs = {"892e91"}, model_GUID = "e6b1f1", spawn_effect="Physical Attack"},
    {name="Lopunny", chip_GUIDs = {"69aa4d"}, model_GUID = "48d04f", spawn_effect="Physical Attack"},
    {name="Mega Lopunny", chip_GUIDs = {"6657d8"}, model_GUID = "0308a7", spawn_effect="Mega Evolve"},
    {name="Mismagius", chip_GUIDs = {"aa262d","c27e09"}, model_GUID = "a81efd", spawn_effect="Physical Attack"},
    {name="Honchkrow", chip_GUIDs = {"e09c28"}, model_GUID = "67c739"},
    {name="Glameow", chip_GUIDs = {"770774"}, model_GUID = "bf33c1"},
    {name="Purugly", chip_GUIDs = {"d5a229"}, model_GUID = "40381a"},
    {name="Chingling", chip_GUIDs = {"7e081e"}, model_GUID = "077587"},
    {name="Stunky", chip_GUIDs = {"f6c459"}, model_GUID = "4f2c9f"},
    {name="Skuntank", chip_GUIDs = {"c7a82a"}, model_GUID = "142926"},
    {name="Bronzor", chip_GUIDs = {"7ec545"}, model_GUID = "f3570a", spawn_effect="Physical Attack"},
    {name="Bronzong", chip_GUIDs = {"e5d8cc","c3242f"}, model_GUID = "422285"},
    {name="Bonsly", chip_GUIDs = {"d5c94d"}, model_GUID = "e95207", spawn_effect="Physical Attack"},
    {name="Mime Jr.", chip_GUIDs = {"4747d9"}, model_GUID = "c86aff", spawn_effect="Physical Attack"},
    {name="Happiny", chip_GUIDs = {"4a54b2"}, model_GUID = "8f2981"},
    {name="Chatot", chip_GUIDs = {"848d53"}, model_GUID = "8a2ee8"},
    {name="Spiritomb", chip_GUIDs = {"b0062b"}, model_GUID = "0a6f0d"},
    {name="Gible", chip_GUIDs = {"0c13bb"}, model_GUID = "a7c469", spawn_effect="Physical Attack"},
    {name="Gabite", chip_GUIDs = {"de70d7"}, model_GUID = "284eb4"},
    {name="Garchomp", chip_GUIDs = {"24c07d"}, model_GUID = "232447", spawn_effect="Status Attack"},
    {name="Mega Garchomp", chip_GUIDs = {"a145cf"}, model_GUID = "233df5", spawn_effect="Mega Evolve"},
    {name="Munchlax", chip_GUIDs = {"035004"}, model_GUID = "7315d0", spawn_effect="Physical Attack"},
    {name="Riolu", chip_GUIDs = {"75d732"}, model_GUID = "b900ca"},
    {name="Lucario", chip_GUIDs = {"b11c09","67935a"}, model_GUID = "6f7542"},
    {name="Mega Lucario", chip_GUIDs = {"349f68","c27650"}, model_GUID = "a2c359", spawn_effect="Mega Evolve"},
    {name="Hippopotas", chip_GUIDs = {"7aa666"}, model_GUID = "0a645f", spawn_effect="Physical Attack"},
    {name="Hippowdon", chip_GUIDs = {"14ae20","98dafe"}, model_GUID = "b43ee9", spawn_effect="Physical Attack"},
    {name="Skorupi", chip_GUIDs = {"088765"}, model_GUID = "f7b7d8"},
    {name="Drapion", chip_GUIDs = {"de18e6","19f4a5"}, model_GUID = "5178e7"},
    {name="Croagunk", chip_GUIDs = {"5641a1"}, model_GUID = "924639"},
    {name="Toxicroak", chip_GUIDs = {"bfd8f1"}, model_GUID = "8604f0"},
    {name="Carnivine", chip_GUIDs = {"6d40dd"}, model_GUID = "f29340"},
    {name="Finneon", chip_GUIDs = {"806f6d"}, model_GUID = "ee9976"},
    {name="Lumineon", chip_GUIDs = {"c39300"}, model_GUID = "5d8ca7", spawn_effect="Physical Attack"},
    {name="Mantyke", chip_GUIDs = {"aa2b78"}, model_GUID = "ac4168", spawn_effect="Physical Attack"},
    {name="Snover", chip_GUIDs = {"aca438"}, model_GUID = "b548ea"},
    {name="Abomasnow", chip_GUIDs = {"d990db","df2a74","32e5a4"}, model_GUID = "c003e7"},
    {name="Mega Abomasnow", chip_GUIDs = {"2fb0e0","551828","257878"}, model_GUID = "b1ae77", spawn_effect="Mega Evolve"},
    {name="Weavile", chip_GUIDs = {"cc1563"}, model_GUID = "ded7dd", spawn_effect="Physical Attack"},
    {name="Magnezone", chip_GUIDs = {"c0741b","cb4b7d"}, model_GUID = "e901bd"},
    {name="Lickilicky", chip_GUIDs = {"c4b4c6"}, model_GUID = "8a81dd", spawn_effect="Physical Attack"},
    {name="Rhyperior", chip_GUIDs = {"0075df","40ed99"}, model_GUID = "860bbc", spawn_effect="Physical Attack"},
    {name="Tangrowth", chip_GUIDs = {"0f87f4"}, model_GUID = "49f976"},
    {name="Electivire", chip_GUIDs = {"4ba29f","8eaea0"}, model_GUID = "15ad3d"},
    {name="Magmortar", chip_GUIDs = {"576d28","150c2c"}, model_GUID = "1ba6f9"},
    {name="Togekiss", chip_GUIDs = {"bdf16d"}, model_GUID = "94ed87"},
    {name="Yanmega", chip_GUIDs = {"9ff570","0d386b"}, model_GUID = "5efa44"},
    {name="Leafeon", chip_GUIDs = {"56a7d1"}, model_GUID = "9f14e6"},
    {name="Glaceon", chip_GUIDs = {"2b5d73"}, model_GUID = "8884ba"},
    {name="Gliscor", chip_GUIDs = {"bc86a9","295fa3"}, model_GUID = "0389d0"},
    {name="Mamoswine", chip_GUIDs = {"d2f4c6"}, model_GUID = "3f4e29"},
    {name="Porygon-Z", chip_GUIDs = {"a88ac6"}, model_GUID = "42158e"},
    {name="Gallade", chip_GUIDs = {"0bf8f0","129794"}, model_GUID = "04f582"},
    {name="Mega Gallade", chip_GUIDs = {"073400","101ca7"}, model_GUID = "8ad8e0", spawn_effect="Mega Evolve"},
    {name="Probopass", chip_GUIDs = {"70fb62","4058d7","7e6aac"}, model_GUID = "3a1a8d"},
    {name="Dusknoir", chip_GUIDs = {"51e126"}, model_GUID = "14f5fa"},
    {name="Froslass", chip_GUIDs = {"16e8e6","ee4cbf","9bcdff"}, model_GUID = "21d87a"},
    {name="Rotom", chip_GUIDs = {"12ae20"}, model_GUID = nil, persistent_state=false, states = {
        {name="Normal Rotom", model_GUID = "480b20", spawn_effect="Physical Attack"},
        {name="Fan Rotom", model_GUID = "274d14", spawn_effect="Physical Attack"},
        {name="Frost Rotom", model_GUID = "671b5d"},
        {name="Heat Rotom", model_GUID = "154dd8"},
        {name="Mow Rotom", model_GUID = "cd771b"},
        {name="Wash Rotom", model_GUID = "c5364f"}
    }},
    {name="Uxie", chip_GUIDs = {"d4bd7f"}, model_GUID = "0d927b", spawn_effect="Physical Attack"},
    {name="Mesprit", chip_GUIDs = {"62ee91"}, model_GUID = "ac091c"},
    {name="Azelf", chip_GUIDs = {"1375a4"}, model_GUID = "5e98bd", spawn_effect="Physical Attack"},
    {name="Dialga", chip_GUIDs = {"a023e2"}, model_GUID = "a47881", spawn_effect="Physical Attack", custom_scale=0.7},
    {name="Palkia", chip_GUIDs = {"9d9d61"}, model_GUID = "42a6d1", custom_scale=0.7},
    {name="Heatran", chip_GUIDs = {"5202aa"}, model_GUID = "7343d2", spawn_effect="Status Attack"},
    {name="Regigigas", chip_GUIDs = {"8f2e0f"}, model_GUID = "0ddf47", spawn_effect="Status Attack", custom_scale=0.7},
    {name="Giratina", chip_GUIDs = {"4e5e93"}, model_GUID = nil, spawn_effect="Status Attack", persistent_state=false, states = {
        {name="Altered Giratina", model_GUID = "086e6b", custom_scale=0.7},
        {name="Origin Giratina", model_GUID = "4dd635", custom_scale=0.7},
    }},
    {name="Cresselia", chip_GUIDs = {"ecbc25"}, model_GUID = "a4dcf2"},
    {name="Phione", chip_GUIDs = {"87659f"}, model_GUID = "b0bae4", spawn_effect="Physical Attack"},
    {name="Manaphy", chip_GUIDs = {"cb657f"}, model_GUID = "5cc85d"},
    {name="Darkrai", chip_GUIDs = {"32a203"}, model_GUID = "aa2219", spawn_effect="Status Attack"},
    {name="Shaymin", chip_GUIDs = {"cb95d3"}, model_GUID = nil, persistent_state=false, states = {
        {name="Land Shaymin", model_GUID = "dca507", spawn_effect="Status Attack"},
        {name="Sky Shaymin", model_GUID = "1e692f", spawn_effect="Status Attack"}
    }},
    {name="Arceus", chip_GUIDs = {"19a7dc"}, model_GUID = nil, persistent_state=false, spawn_effect="Status Attack", states = {
        {name="Normal Arceus", model_GUID = "bf4818"},
        {name="Bug Arceus", model_GUID = "6ea200"},
        {name="Dark Arceus", model_GUID = "4cc1a4"},
        {name="Dragon Arceus", model_GUID = "527bbf"},
        {name="Electric Arceus", model_GUID = "ab97c6"},
        {name="Fairy Arceus", model_GUID = "4c5955"},
        {name="Fighting Arceus", model_GUID = "57484c"},
        {name="Fire Arceus", model_GUID = "ea2bc4"},
        {name="Flying Arceus", model_GUID = "7ede2c"},
        {name="Ghost Arceus", model_GUID = "a73ca8"},
        {name="Grass Arceus", model_GUID = "4fef88"},
        {name="Ground Arceus", model_GUID = "4d968c"},
        {name="Ice Arceus", model_GUID = "ad626e"},
        {name="Poison Arceus", model_GUID = "124c61"},
        {name="Psychic Arceus", model_GUID = "aabc1e"},
        {name="Rock Arceus", model_GUID = "d34041"},
        {name="Steel Arceus", model_GUID = "3a38c0"},
        {name="Water Arceus", model_GUID = "1efd23"}
    }},
    {name="Victini", chip_GUIDs = {"018358"}, model_GUID = "a96393", spawn_effect="Physical Attack"},
    {name="Snivy", chip_GUIDs = {"cc9ee8"}, model_GUID = "dd3268"},
    {name="Servine", chip_GUIDs = {"649cd4"}, model_GUID = "cb1ef6"},
    {name="Serperior", chip_GUIDs = {"7ac90f","d69c76"}, model_GUID = "df3e9d", spawn_effect="Physical Attack"},
    {name="Tepig", chip_GUIDs = {"070f87"}, model_GUID = "f8a647"},
    {name="Pignite", chip_GUIDs = {"c7cee7"}, model_GUID = "1142b7"},
    {name="Emboar", chip_GUIDs = {"361693","17d574"}, model_GUID = "83bed3"},
    {name="Oshawott", chip_GUIDs = {"61c08d"}, model_GUID = "26af13"},
    {name="Dewott", chip_GUIDs = {"27f92f"}, model_GUID = "4544f3", spawn_effect="Physical Attack"},
    {name="Samurott", chip_GUIDs = {"40afa6","21aa51"}, model_GUID = "027855", spawn_effect="Physical Attack"},
    {name="Patrat", chip_GUIDs = {"cadaed"}, model_GUID = "95e22a"},
    {name="Watchog", chip_GUIDs = {"7515f8","b43a95"}, model_GUID = "446cbb"},
    {name="Lillipup", chip_GUIDs = {"a32f69","a3c521"}, model_GUID = "a3d331"},
    {name="Herdier", chip_GUIDs = {"32d546","6c37f3"}, model_GUID = "fe0927"},
    {name="Stoutland", chip_GUIDs = {"afccde"}, model_GUID = "eff43c"},
    {name="Purrloin", chip_GUIDs = {"0c6cca"}, model_GUID = "ad8e41"},
    {name="Liepard", chip_GUIDs = {"d1b187","9023cb"}, model_GUID = "0d58f0"},
    {name="Pansage", chip_GUIDs = {"fd8373","d497ed"}, model_GUID = "0febb6"},
    {name="Simisage", chip_GUIDs = {"3e7175","6c4320"}, model_GUID = "962b36"},
    {name="Pansear", chip_GUIDs = {"81298e","7a2fe7"}, model_GUID = "790b25"},
    {name="Simisear", chip_GUIDs = {"8334de","308ed4"}, model_GUID = "d7786a"},
    {name="Panpour", chip_GUIDs = {"fa9044","f10ebc"}, model_GUID = "446aff"},
    {name="Simipour", chip_GUIDs = {"6936f9","723fd5"}, model_GUID = "7ca32a"},
    {name="Munna", chip_GUIDs = {"5d5087"}, model_GUID = "d7cfc6"},
    {name="Musharna", chip_GUIDs = {"9e1cf4","3f79d1"}, model_GUID = "f742b2"},
    {name="Pidove", chip_GUIDs = {"866d00"}, model_GUID = "4f6c26"},
    {name="Tranquill", chip_GUIDs = {"081e5d"}, model_GUID = "0f8928"},
    {name="Unfezant", chip_GUIDs = {"83aa02","d9bd62","ef61ad"}, model_GUID = nil, states = {
        {name="Female Unfezant", model_GUID = "8a8d28"},
        {name="Male Unfezant", model_GUID = "e4c215"}
    }},
    {name="Blitzle", chip_GUIDs = {"b1cead"}, model_GUID = "14eed5"},
    {name="Zebstrika", chip_GUIDs = {"21fb3e","718ffe"}, model_GUID = "1f6bb5", spawn_effect="Physical Attack"},
    {name="Roggenrola", chip_GUIDs = {"c84879"}, model_GUID = "5e5531"},
    {name="Boldore", chip_GUIDs = {"a6b766","cd02f5"}, model_GUID = "a80816", spawn_effect="Physical Attack"},
    {name="Gigalith", chip_GUIDs = {"5e1bbe"}, model_GUID = "0162ba"},
    {name="Woobat", chip_GUIDs = {"7f6d80"}, model_GUID = "5d9f84"},
    {name="Swoobat", chip_GUIDs = {"b3ad5b","2ea21d"}, model_GUID = "97b4e6"},
    {name="Drilbur", chip_GUIDs = {"de1851"}, model_GUID = "a49ea4"},
    {name="Excadrill", chip_GUIDs = {"6612ad","2ae05d"}, model_GUID = "dccc9b", spawn_effect="Status Attack"},
    {name="Audino", chip_GUIDs = {"7908e2"}, model_GUID = "134409"},
    {name="Mega Audino", chip_GUIDs = {"2aa323"}, model_GUID = "793f23", spawn_effect="Mega Evolve"},
    {name="Timburr", chip_GUIDs = {"76de01"}, model_GUID = "334885", spawn_effect="Physical Attack"},
    {name="Gurdurr", chip_GUIDs = {"515736"}, model_GUID = "0dfde4", spawn_effect="Physical Attack"},
    {name="Conkeldurr", chip_GUIDs = {"91c004","81168e"}, model_GUID = "04bed2"},
    {name="Tympole", chip_GUIDs = {"0931b6"}, model_GUID = "70136d"},
    {name="Palpitoad", chip_GUIDs = {"747884","7a9241"}, model_GUID = "62c539", spawn_effect="Physical Attack"},
    {name="Seismitoad", chip_GUIDs = {"0ad5ba"}, model_GUID = "f1d4fb"},
    {name="Throh", chip_GUIDs = {"6a94c9","3e64ad"}, model_GUID = "74563b"},
    {name="Sawk", chip_GUIDs = {"bcfe90","d758dc"}, model_GUID = "1f8cae"},
    {name="Sewaddle", chip_GUIDs = {"7528ff"}, model_GUID = "2a3547"},
    {name="Swadloon", chip_GUIDs = {"924462"}, model_GUID = "0e2bb5"},
    {name="Leavanny", chip_GUIDs = {"45a28f"}, model_GUID = "788aa1"},
    {name="Venipede", chip_GUIDs = {"6baaed"}, model_GUID = "07a778"},
    {name="Whirlipede", chip_GUIDs = {"9387f5","de7f57"}, model_GUID = "0f9af9", spawn_effect="Physical Attack"},
    {name="Scolipede", chip_GUIDs = {"c27a7d"}, model_GUID = "6b8b44", spawn_effect="Physical Attack"},
    {name="Cottonee", chip_GUIDs = {"b7284a"}, model_GUID = "e5da38", spawn_effect="Physical Attack"},
    {name="Whimsicott", chip_GUIDs = {"26fdd5"}, model_GUID = "2437bd", spawn_effect="Physical Attack"},
    {name="Petilil", chip_GUIDs = {"589210"}, model_GUID = "f7466a", spawn_effect="Physical Attack"},
    {name="Lilligant", chip_GUIDs = {"0bc61d"}, model_GUID = "312e7e"},
    {name="Basculin", chip_GUIDs = {"c925da"}, model_GUID = nil, states = {
        {name="Blue-Striped Basculin", model_GUID = "76e2c8"},
        {name="Red-Striped Basculin", model_GUID = "1aad2b"}
    }},
    {name="Sandile", chip_GUIDs = {"82f44e"}, model_GUID = "2e2c75"},
    {name="Krokorok", chip_GUIDs = {"fd6367","55dbc5","222710"}, model_GUID = "4cf233"},
    {name="Krookodile", chip_GUIDs = {"8af519","bc55d8"}, model_GUID = "1455fe"},
    {name="Darumaka", chip_GUIDs = {"6b518a"}, model_GUID = "5f8571", spawn_effect="Physical Attack"},
    {name="Darmanitan", chip_GUIDs = {"f79c8a"}, model_GUID = "e68195"},
    {name="Zen Darmanitan", chip_GUIDs = {}, model_GUID = "4693f5"},
    {name="Maractus", chip_GUIDs = {"a48ab3"}, model_GUID = "3832b0"},
    {name="Dwebble", chip_GUIDs = {"80f75f","4bd661"}, model_GUID = "553572"},
    {name="Crustle", chip_GUIDs = {"6cbafa"}, model_GUID = "de316c"},
    {name="Scraggy", chip_GUIDs = {"b2c872"}, model_GUID = "6e59e2"},
    {name="Scrafty", chip_GUIDs = {"251001","55d865"}, model_GUID = "b9d27b", spawn_effect="Physical Attack"},
    {name="Sigilyph", chip_GUIDs = {"debe3d","ef498e","f2302a"}, model_GUID = "71ed95"},
    {name="Yamask", chip_GUIDs = {"bdb70a"}, model_GUID = "5304a8"},
    {name="Cofagrigus", chip_GUIDs = {"b30651","1d7e17"}, model_GUID = "76a330"},
    {name="Tirtouga", chip_GUIDs = {"3e8a39"}, model_GUID = "8bbd45"},
    {name="Carracosta", chip_GUIDs = {"ac10b6"}, model_GUID = "212e44"},
    {name="Archen", chip_GUIDs = {"026020"}, model_GUID = "fe5542"},
    {name="Archeops", chip_GUIDs = {"ee398b"}, model_GUID = "e52be5", spawn_effect="Physical Attack"},
    {name="Trubbish", chip_GUIDs = {"deff95"}, model_GUID = "8eef4f"},
    {name="Garbodor", chip_GUIDs = {"e5380f"}, model_GUID = "317f1f"},
    {name="Zorua", chip_GUIDs = {"a72386"}, model_GUID = "04ebbd"},
    {name="Zoroark", chip_GUIDs = {"8f3048"}, model_GUID = "fb10d4"},
    {name="Minccino", chip_GUIDs = {"0d33eb"}, model_GUID = "dccefe", spawn_effect="Physical Attack"},
    {name="Cinccino", chip_GUIDs = {"3c8448"}, model_GUID = "320a12"},
    {name="Gothita", chip_GUIDs = {"3ac8a5"}, model_GUID = "ce5c02"},
    {name="Gothorita", chip_GUIDs = {"dda115"}, model_GUID = "afdfbd"},
    {name="Gothitelle", chip_GUIDs = {"4a8950","9a050e"}, model_GUID = "e6fa75"},
    {name="Solosis", chip_GUIDs = {"10b981"}, model_GUID = "cd2f3b"},
    {name="Duosion", chip_GUIDs = {"2c517e"}, model_GUID = "5681e1"},
    {name="Reuniclus", chip_GUIDs = {"14ea43","aeb168"}, model_GUID = "43a8c7", spawn_effect="Physical Attack"},
    {name="Ducklett", chip_GUIDs = {"66ccc3"}, model_GUID = "0ea94c"},
    {name="Swanna", chip_GUIDs = {"c8d1e9","56a0d6"}, model_GUID = "653e11"},
    {name="Vanillite", chip_GUIDs = {"68fcee"}, model_GUID = "a60d6e"},
    {name="Vanillish", chip_GUIDs = {"074c93","804902"}, model_GUID = "aa33f1", spawn_effect="Physical Attack"},
    {name="Vanilluxe", chip_GUIDs = {"70b16b"}, model_GUID = "7abd8f"},
    {name="Deerling", chip_GUIDs = {"f0e09f"}, model_GUID = nil, persistent_state=false, states = {
        {name="Autumn Deerling", model_GUID = "aa4a1c"},
        {name="Spring Deerling", model_GUID = "fe766f"},
        {name="Summer Deerling", model_GUID = "e544e1"},
        {name="Winter Deerling", model_GUID = "b1f364"}
    }},
    {name="Sawsbuck", chip_GUIDs = {"e49a8d"}, model_GUID = nil, persistent_state=false, states = {
        {name="Autumn Sawsbuck", model_GUID = "8aaef9"},
        {name="Spring Sawsbuck", model_GUID = "9b9805"},
        {name="Summer Sawsbuck", model_GUID = "8bb6ad"},
        {name="Winter Sawsbuck", model_GUID = "77fc07"}
    }},
    {name="Emolga", chip_GUIDs = {"d78ea5","37a2ec","517398","3c291a"}, model_GUID = "dad8a0", spawn_effect="Physical Attack"},
    {name="Karrablast", chip_GUIDs = {"fe0f8b"}, model_GUID = "66ec18"},
    {name="Escavalier", chip_GUIDs = {"b466ce"}, model_GUID = "e4c099"},
    {name="Foongus", chip_GUIDs = {"9eb49e"}, model_GUID = "06f3ae"},
    {name="Amoonguss", chip_GUIDs = {"ae82e3"}, model_GUID = "b1f388"},
    {name="Frillish", chip_GUIDs = {"95ff8a"}, model_GUID = nil, states = {
        {name="Female Frillish", model_GUID = "f693fb"},
        {name="Male Frillish", model_GUID = "4d5963"}
    }},
    {name="Jellicent", chip_GUIDs = {"6435eb","9ef10b"}, model_GUID = nil, states = {
        {name="Female Jellicent", model_GUID = "82eae8", spawn_effect="Physical Attack"},
        {name="Male Jellicent", model_GUID = "b219d0", spawn_effect="Physical Attack"}
    }},
    {name="Alomomola", chip_GUIDs = {"96f59c"}, model_GUID = "987107"},
    {name="Joltik", chip_GUIDs = {"ad8656"}, model_GUID = "b967c0", spawn_effect="Physical Attack"},
    {name="Galvantula", chip_GUIDs = {"2bd436"}, model_GUID = "f565f0"},
    {name="Ferroseed", chip_GUIDs = {"5f7137"}, model_GUID = "46fbe9"},
    {name="Ferrothorn", chip_GUIDs = {"5f5120"}, model_GUID = "37e34d"},
    {name="Klink", chip_GUIDs = {"c0e0aa"}, model_GUID = "f1c2b1", spawn_effect="Physical Attack"},
    {name="Klang", chip_GUIDs = {"618611"}, model_GUID = "8388d7", spawn_effect="Physical Attack"},
    {name="Klinklang", chip_GUIDs = {"c774db"}, model_GUID = "f57695"},
    {name="Tynamo", chip_GUIDs = {"0973ae"}, model_GUID = "d78a62"},
    {name="Eelektrik", chip_GUIDs = {"0c7633"}, model_GUID = "e93792"},
    {name="Eelektross", chip_GUIDs = {"614f24","9ffe7f"}, model_GUID = "155948", spawn_effect="Physical Attack"},
    {name="Elgyem", chip_GUIDs = {"453e62"}, model_GUID = "fca2da"},
    {name="Beheeyem", chip_GUIDs = {"025be5"}, model_GUID = "824281", spawn_effect="Physical Attack"},
    {name="Litwick", chip_GUIDs = {"25c015"}, model_GUID = "ed8fd6"},
    {name="Lampent", chip_GUIDs = {"d70e1e"}, model_GUID = "90d56c", spawn_effect="Physical Attack"},
    {name="Chandelure", chip_GUIDs = {"29f249","fa0682","2c4ae1"}, model_GUID = "de2e80"},
    {name="Axew", chip_GUIDs = {"ed42bc"}, model_GUID = "c209ac"},
    {name="Fraxure", chip_GUIDs = {"73f483","d0fc5e"}, model_GUID = "e5baeb"},
    {name="Haxorus", chip_GUIDs = {"999be9","0dbda1"}, model_GUID = "18b97d"},
    {name="Cubchoo", chip_GUIDs = {"ae697d"}, model_GUID = "085ee7"},
    {name="Beartic", chip_GUIDs = {"8a6dc1","6d5140"}, model_GUID = "54bff7"},
    {name="Cryogonal", chip_GUIDs = {"3ecf8c","a19010","df88f3"}, model_GUID = "37b1ac", spawn_effect="Physical Attack"},
    {name="Shelmet", chip_GUIDs = {"36dc98"}, model_GUID = "0f788a", spawn_effect="Physical Attack"},
    {name="Accelgor", chip_GUIDs = {"e3af12"}, model_GUID = "98569d"},
    {name="Stunfisk", chip_GUIDs = {"16bf55"}, model_GUID = "4a9d6e"},
    {name="Mienfoo", chip_GUIDs = {"fe3057","e0c36b"}, model_GUID = "05f772", spawn_effect="Physical Attack"},
    {name="Mienshao", chip_GUIDs = {"aebcb7","b5c501"}, model_GUID = "afd151"},
    {name="Druddigon", chip_GUIDs = {"1d692d","c67d77","190e10"}, model_GUID = "e0e534"},
    {name="Golett", chip_GUIDs = {"604051"}, model_GUID = "560b71"},
    {name="Golurk", chip_GUIDs = {"35c87b","16150e"}, model_GUID = "002dc3"},
    {name="Pawniard", chip_GUIDs = {"90ce4c"}, model_GUID = "4aa17e"},
    {name="Bisharp", chip_GUIDs = {"728a81","88fa20"}, model_GUID = "753f92"},
    {name="Bouffalant", chip_GUIDs = {"7df9cf"}, model_GUID = "3296ae"},
    {name="Rufflet", chip_GUIDs = {"caf315"}, model_GUID = "bc0883"},
    {name="Braviary", chip_GUIDs = {"5b5fb9"}, model_GUID = "3f59cc"},
    {name="Vullaby", chip_GUIDs = {"be0465"}, model_GUID = "bc6308"},
    {name="Mandibuzz", chip_GUIDs = {"80013f","036d3b"}, model_GUID = "fa9bd5"},
    {name="Heatmor", chip_GUIDs = {"861961"}, model_GUID = "571def"},
    {name="Durant", chip_GUIDs = {"028334"}, model_GUID = "0b8592"},
    {name="Deino", chip_GUIDs = {"05ab32"}, model_GUID = "119c86"},
    {name="Zweilous", chip_GUIDs = {"fb7464"}, model_GUID = "bbc115", spawn_effect="Physical Attack"},
    {name="Hydreigon", chip_GUIDs = {"7416ab"}, model_GUID = "2621b5", spawn_effect="Status Attack"},
    {name="Larvesta", chip_GUIDs = {"d228b3"}, model_GUID = "68b192"},
    {name="Volcarona", chip_GUIDs = {"1785b1"}, model_GUID = "c9ed3b"},
    {name="Cobalion", chip_GUIDs = {"80f352"}, model_GUID = "8d4715"},
    {name="Terrakion", chip_GUIDs = {"4ad731"}, model_GUID = "f48c6a"},
    {name="Virizion", chip_GUIDs = {"cdd748"}, model_GUID = "774eac"},
    {name="Tornadus", chip_GUIDs = {"18889e"}, model_GUID = nil, spawn_effect="Status Attack", persistent_state=false, states = {
        {name="Incarnate Tornadus", model_GUID = "af1db1"},
        {name="Therian Tornadus", model_GUID = "f7abb5"}
    }},
    {name="Thundurus", chip_GUIDs = {"3539ac"}, model_GUID = nil, spawn_effect="Status Attack", persistent_state=false, states = {
        {name="Incarnate Thundurus", model_GUID = "0a0b98"},
        {name="Therian Thundurus", model_GUID = "13842a"}
    }},
    {name="Reshiram", chip_GUIDs = {"c10778"}, model_GUID = "d9cc98", spawn_effect="Status Attack"},
    {name="Zekrom", chip_GUIDs = {"544bb5"}, model_GUID = "463700", spawn_effect="Status Attack"},
    {name="Landorus", chip_GUIDs = {"6fd4d7"}, model_GUID = nil, persistent_state=false, states = {
        {name="Incarnate Landorus", model_GUID = "a7e86f", spawn_effect="Status Attack"},
        {name="Therian Landorus", model_GUID = "604304"}
    }},
    {name="Kyurem", chip_GUIDs = {"fe6327","ed5bf5"}, model_GUID = nil, persistent_state=false, states = {
        {name="Basic Kyurem", model_GUID = "be9a02", spawn_effect="Status Attack"},
        {name="Black Kyurem", model_GUID = "69c378"},
        {name="Black Kyurem Overdrive", model_GUID = "1a2e53"},
        {name="White Kyurem", model_GUID = "33c20a"},
        {name="White Kyurem Overdrive", model_GUID = "74e66c"},
    }},
    {name="Keldeo", chip_GUIDs = {"449364"}, model_GUID = nil, spawn_effect="Status Attack", persistent_state=false, states = {
        {name="Ordinary Keldeo", model_GUID = "97905c"},
        {name="Resolute Keldeo", model_GUID = "4465a6"}
    }},
    {name="Meloetta", chip_GUIDs = {"2a5a01"}, model_GUID = nil, persistent_state=false, states = {
        {name="Aria Meloetta", chip_GUIDs = {}, model_GUID = "eb62fb"},
        {name="Pirouette Meloetta", chip_GUIDs = {}, model_GUID = "e83612", spawn_effect="Status Attack"}
    }},
    {name="Genesect", chip_GUIDs = {"4c3783"}, model_GUID = nil, spawn_effect="Status Attack", persistent_state=false, states = {
        {name="Normal Genesect", model_GUID = "da9000"},
        {name="Burn Drive Genesect", model_GUID = "a1e306"},
        {name="Chill Drive Genesect", model_GUID = "a35a23"},
        {name="Douse Drive Genesect", model_GUID = "e786cb"},
        {name="Shock Drive Genesect", model_GUID = "5faabd"}
    }},
    {name="Chespin", chip_GUIDs = {"d30d7a"}, model_GUID = "bfb1c9"},
    {name="Quilladin", chip_GUIDs = {"971f30"}, model_GUID = "11c320"},
    {name="Chesnaught", chip_GUIDs = {"577977"}, model_GUID = "176400"},
    {name="Fennekin", chip_GUIDs = {"be55c3"}, model_GUID = "95501e", spawn_effect="Status Attack"},
    {name="Braixen", chip_GUIDs = {"46cfb7"}, model_GUID = "61a7d9"},
    {name="Delphox", chip_GUIDs = {"ed8bda"}, model_GUID = "8879bd"},
    {name="Froakie", chip_GUIDs = {"0f284d"}, model_GUID = "8d83d9"},
    {name="Frogadier", chip_GUIDs = {"2f067a"}, model_GUID = "1c8a04"},
    {name="Greninja", chip_GUIDs = {"81e21e"}, model_GUID = nil, persistent_state=false, states = {
        {name="Normal Greninja", model_GUID = "222e7b", spawn_effect="Status Attack"},
        {name="Ash-Greninja", model_GUID = "5c7f40", spawn_effect="Physical Attack"}
    }},
    {name="Bunnelby", chip_GUIDs = {"7619c2"}, model_GUID = "0a6a17"},
    {name="Diggersby", chip_GUIDs = {"b57108"}, model_GUID = "016627"},
    {name="Fletchling", chip_GUIDs = {"6b24ab"}, model_GUID = "1a27c7"},
    {name="Fletchinder", chip_GUIDs = {"1e8a6b"}, model_GUID = "516206", spawn_effect="Physical Attack"},
    {name="Talonflame", chip_GUIDs = {"5e9819","d1f86e"}, model_GUID = "efd523", spawn_effect="Physical Attack"},
    {name="Scatterbug", chip_GUIDs = {"16049b"}, model_GUID = "2e6d23"},
    {name="Spewpa", chip_GUIDs = {"a3c66a"}, model_GUID = "a4a4dc"},
    {name="Vivillon", chip_GUIDs = {"23bb65","11d283"}, model_GUID = nil, states = {
        {name="Archipelago Vivillon", model_GUID = "44e252"},
        {name="Continental Vivillon", model_GUID = "5364cc"},
        {name="Elegant Vivillon", model_GUID = "60d274"},
        {name="Fancy Vivillon", model_GUID = "f1a603"},
        {name="Garden Vivillon", model_GUID = "8be52d"},
        {name="High Plains Vivillon", model_GUID = "d6e0d3"},
        {name="Icy Snow Vivillon", model_GUID = "63fe3c"},
        {name="Jungle Vivillon", model_GUID = "4a64bb"},
        {name="Marine Vivillon", model_GUID = "bb4196"},
        {name="Meadow Vivillon", model_GUID = "20fbb2"},
        {name="Modern Vivillon", model_GUID = "b187b8"},
        {name="Monsoon Vivillon", model_GUID = "f0f775"},
        {name="Ocean Vivillon", model_GUID = "64ebe6"},
        {name="Poké Ball Vivillon", model_GUID = "112892"},
        {name="Polar Vivillon", model_GUID = "4ea111"},
        {name="River Vivillon", model_GUID = "5349f1"},
        {name="Sandstorm Vivillon", model_GUID = "9da945"},
        {name="Savanna Vivillon", model_GUID = "be205d"},
        {name="Sun Vivillon", model_GUID = "f38437"},
        {name="Tundra Vivillon", model_GUID = "69b9b6"},
    }},
    {name="Litleo", chip_GUIDs = {"e00c5c"}, model_GUID = "61abba", spawn_effect="Physical Attack"},
    {name="Pyroar", chip_GUIDs = {"9b06d4","a7dbf8"}, model_GUID = nil, states = {
        {name="Female Pyroar", model_GUID = "122f4c"},
        {name="Male Pyroar", model_GUID = "4b1422"}
    }},
    {name="Flabébé", chip_GUIDs = {"1d6735"}, model_GUID = "84d07d"},
    {name="Floette", chip_GUIDs = {"1bca86"}, model_GUID = "51e3b5"},
    {name="Florges", chip_GUIDs = {"5048e5"}, model_GUID = "b2407e", spawn_effect="Physical Attack"},
    {name="Skiddo", chip_GUIDs = {"8e347c"}, model_GUID = "c613ad"},
    {name="Gogoat", chip_GUIDs = {"e7318a","fda64c"}, model_GUID = "88d0b0", spawn_effect="Status Attack"},
    {name="Pancham", chip_GUIDs = {"ae849e"}, model_GUID = "0c4abd"},
    {name="Pangoro", chip_GUIDs = {"a86248"}, model_GUID = "bc9f87", spawn_effect="Status Attack"},
    {name="Furfrou", chip_GUIDs = {"c958f7"}, model_GUID = "86252d"},
    {name="Espurr", chip_GUIDs = {"650805"}, model_GUID = "11f232"},
    {name="Meowstic", chip_GUIDs = {"3dfb47","bb5aa2"}, model_GUID = nil, states = {
        {name="Female Meowstic", model_GUID = "3357e1"},
        {name="Male Meowstic", model_GUID = "c2df10"}
    }},
    {name="Honedge", chip_GUIDs = {"2d99d1"}, model_GUID = "db85f6", spawn_effect="Physical Attack"},
    {name="Doublade", chip_GUIDs = {"95e815"}, model_GUID = "429a84", spawn_effect="Physical Attack"},
    {name="Aegislash", chip_GUIDs = {"b46ccf","cda97c"}, model_GUID = nil, persistent_state=false, states = {
        {name="Blade Aegislash", model_GUID = "a6b120", spawn_effect="Blade Stance"},
        {name="Shield Aegislash", model_GUID = "eed99a", idle_effect="Shield Idle", run_effect="Shield Move", spawn_effect="Shield Stance", faint_effect="Shield Faint"}
    }},
    {name="Spritzee", chip_GUIDs = {"010663"}, model_GUID = "24fb10"},
    {name="Aromatisse", chip_GUIDs = {"adff32"}, model_GUID = "c86d86"},
    {name="Swirlix", chip_GUIDs = {"68623d"}, model_GUID = "f67993"},
    {name="Slurpuff", chip_GUIDs = {"084510"}, model_GUID = "f15bb3"},
    {name="Inkay", chip_GUIDs = {"3b110c"}, model_GUID = "95c70d"},
    {name="Malamar", chip_GUIDs = {"27bc46"}, model_GUID = "76d4d2"},
    {name="Binacle", chip_GUIDs = {"3a7370"}, model_GUID = "d68ddb", spawn_effect="Physical Attack"},
    {name="Barbaracle", chip_GUIDs = {"12b2e4","f8d02b"}, model_GUID = "7acdd8"},
    {name="Skrelp", chip_GUIDs = {"cb3b2c"}, model_GUID = "47af48"},
    {name="Dragalge", chip_GUIDs = {"74ad41","4dc386"}, model_GUID = "2c5c3c"},
    {name="Clauncher", chip_GUIDs = {"1897b2"}, model_GUID = "7d382e"},
    {name="Clawitzer", chip_GUIDs = {"3b37cf","bc485a"}, model_GUID = "3bdfca"},
    {name="Helioptile", chip_GUIDs = {"cdb569"}, model_GUID = "6c28be"},
    {name="Heliolisk", chip_GUIDs = {"c750ae","e5d7e2"}, model_GUID = "7d0f27"},
    {name="Tyrunt", chip_GUIDs = {"6788a6","f9bbcd"}, model_GUID = "f6e97c"},
    {name="Tyrantrum", chip_GUIDs = {"27ff50","85599a"}, model_GUID = "e8c337", spawn_effect="Status Attack"},
    {name="Amaura", chip_GUIDs = {"714ce8","19ad5f"}, model_GUID = "73d866"},
    {name="Aurorus", chip_GUIDs = {"b9d9b4","32733e"}, model_GUID = "17494e", spawn_effect="Physical Attack"},
    {name="Sylveon", chip_GUIDs = {"1e783b","08af1d"}, model_GUID = "381a03"},
    {name="Hawlucha", chip_GUIDs = {"ff3e3a","4f6a55","435515"}, model_GUID = "c7140e", spawn_effect="Status Attack"},
    {name="Dedenne", chip_GUIDs = {"4901d2"}, model_GUID = "f90863"},
    {name="Carbink", chip_GUIDs = {"dffb35","24b8cb"}, model_GUID = "9c0cd8", spawn_effect="Physical Attack"},
    {name="Goomy", chip_GUIDs = {"941e4d"}, model_GUID = "07df6f"},
    {name="Sliggoo", chip_GUIDs = {"a81afe"}, model_GUID = "1ac031"},
    {name="Goodra", chip_GUIDs = {"2e85ca","09adf5"}, model_GUID = "35c5e9", spawn_effect="Status Attack"},
    {name="Klefki", chip_GUIDs = {"0ea6e5","6cd383"}, model_GUID = "4b6f52", spawn_effect="Physical Attack"},
    {name="Phantump", chip_GUIDs = {"b61257"}, model_GUID = "fc0113"},
    {name="Trevenant", chip_GUIDs = {"e9e820"}, model_GUID = "4a56ba"},
    {name="Pumpkaboo", chip_GUIDs = {"143c40"}, model_GUID = "c5c891"},
    {name="Gourgeist", chip_GUIDs = {"19c60a","61e2fa"}, model_GUID = "16c782"},
    {name="Bergmite", chip_GUIDs = {"60dda9"}, model_GUID = "517919", spawn_effect="Physical Attack"},
    {name="Avalugg", chip_GUIDs = {"280874","e721e9"}, model_GUID = "fc938c", spawn_effect="Status Attack"},
    {name="Noibat", chip_GUIDs = {"0cc508"}, model_GUID = "f0c07f"},
    {name="Noivern", chip_GUIDs = {"33dbd5","990c54"}, model_GUID = "212e94", spawn_effect="Status Attack"},
    {name="Xerneas", chip_GUIDs = {"7bbc8b"}, model_GUID = nil, persistent_state=false, states = {
        {name="Active Xerneas", model_GUID = "d0ee6e"},
        {name="Neutral Xerneas", model_GUID = "9f2a70"}
    }},
    {name="Yveltal", chip_GUIDs = {"dd265b"}, model_GUID = "c48f86", spawn_effect="Status Attack"},
    {name="Zygarde 10%", chip_GUIDs = {"2d0037"}, model_GUID = "c0079c", spawn_effect="Status Attack"},
    {name="Zygarde 50%", chip_GUIDs = {"6440d9"}, model_GUID = "fd9300", spawn_effect="Status Attack"},
    {name="Zygarde 100%", chip_GUIDs = {"8da170"}, model_GUID = "d70d3c", spawn_effect="Physical Attack"},
    {name="Diancie", chip_GUIDs = {"124a87"}, model_GUID = "e25fa5"},
    {name="Mega Diancie", chip_GUIDs = {"1396a7"}, model_GUID = "33befa", spawn_effect="Special Attack"},
    {name="Hoopa", chip_GUIDs = {"7efc70"}, model_GUID = "ef60ef", spawn_effect="Status Attack"},
    {name="Hoopa Unbound", chip_GUIDs = {"bd1026"}, model_GUID = "434623", custom_scale=0.7},
    {name="Volcanion", chip_GUIDs = {"23a4a3"}, model_GUID = "86e6c6"},
    {name="Rowlet", chip_GUIDs = {"0186f2"}, model_GUID = "494cbe"},
    {name="Dartrix", chip_GUIDs = {"f6ecee"}, model_GUID = "7a6739"},
    {name="Decidueye", chip_GUIDs = {"f183b0","646c5f"}, model_GUID = "d35cbf", spawn_effect="Status Attack", offset={x=0, y=0.115, z=0}},
    {name="Litten", chip_GUIDs = {"0a784d"}, model_GUID = "bc40dd"},
    {name="Torracat", chip_GUIDs = {"7b358c"}, model_GUID = "84c6db"},
    {name="Incineroar", chip_GUIDs = {"5db32b","4128e6"}, model_GUID = "bc10fb"},
    {name="Popplio", chip_GUIDs = {"d5827d"}, model_GUID = "c96d82"},
    {name="Brionne", chip_GUIDs = {"a7b181"}, model_GUID = "0aa113"},
    {name="Primarina", chip_GUIDs = {"b4f92a","b97215"}, model_GUID = "bd9d9f"},
    {name="Pikipek", chip_GUIDs = {"1babd0"}, model_GUID = "b81a90"},
    {name="Trumbeak", chip_GUIDs = {"efede3"}, model_GUID = "1d959d"},
    {name="Toucannon", chip_GUIDs = {"c01ce3","acd6b4"}, model_GUID = "2c6bc3"},
    {name="Yungoos", chip_GUIDs = {"18208f"}, model_GUID = "d77420"},
    {name="Gumshoos", chip_GUIDs = {"f8df0f"}, model_GUID = "902af6"},
    {name="Grubbin", chip_GUIDs = {"d650b2"}, model_GUID = "094700"},
    {name="Charjabug", chip_GUIDs = {"82bfef"}, model_GUID = "f5e6f0"},
    {name="Vikavolt", chip_GUIDs = {"c36d2d"}, model_GUID = "d539ed"},
    {name="Crabrawler", chip_GUIDs = {"c59ec7"}, model_GUID = "cc10c3", spawn_effect="Physical Attack"},
    {name="Crabominable", chip_GUIDs = {"3f855c","7e4ef2","ece784"}, model_GUID = "bd1445"},
    {name="Oricorio-Baile", chip_GUIDs = {"949bcb","7e10b7"}, model_GUID = "f5fc3a", spawn_effect="Physical Attack"},
    {name="Oricorio-Pa'u", chip_GUIDs = {"43375f","88a930"}, model_GUID = "19413b", spawn_effect="Physical Attack"},
    {name="Oricorio-Sensu", chip_GUIDs = {"f12a49","89fe55"}, model_GUID = "a5d034", spawn_effect="Physical Attack"},
    {name="Oricorio-Pom-Pom", chip_GUIDs = {"fac05f","ca8f57"}, model_GUID = "e5b28a", spawn_effect="Physical Attack"},
    {name="Cutiefly", chip_GUIDs = {"44c352"}, model_GUID = "7703b3"},
    {name="Ribombee", chip_GUIDs = {"f1bbe4"}, model_GUID = "a1c941", spawn_effect="Physical Attack"},
    {name="Rockruff", chip_GUIDs = {"fa3917"}, model_GUID = "aaf680"},
    {name="Lycanroc", chip_GUIDs = {"9aa6c8","ad0254","5456a0"}, model_GUID = nil, spawn_effect="Status Attack", states = {
        {name="Dusk Lycanroc", model_GUID = "be2395"},
        {name="Midday Lycanroc", model_GUID = "226b04"},
        {name="Midnight Lycanroc", model_GUID = "5e3a9d"}
    }},
    {name="Wishiwashi", chip_GUIDs = {"4eb487"}, model_GUID = "ec2225"},
    {name="Wishiwashi School", chip_GUIDs = {"6e87ad"}, model_GUID = "7da1df", custom_scale=0.5},
    {name="Mareanie", chip_GUIDs = {"79610c"}, model_GUID = "437e25", spawn_effect="Physical Attack"},
    {name="Toxapex", chip_GUIDs = {"873b78"}, model_GUID = "0abc27"},
    {name="Mudbray", chip_GUIDs = {"a32a33"}, model_GUID = "d128ab"},
    {name="Mudsdale", chip_GUIDs = {"1a9776","62e644"}, model_GUID = "28b051", spawn_effect="Physical Attack"},
    {name="Dewpider", chip_GUIDs = {"96ef2d"}, model_GUID = "51f953"},
    {name="Araquanid", chip_GUIDs = {"d450db"}, model_GUID = "049687", spawn_effect="Physical Attack"},
    {name="Fomantis", chip_GUIDs = {"6477f9"}, model_GUID = "7b424f", spawn_effect="Physical Attack"},
    {name="Lurantis", chip_GUIDs = {"183687"}, model_GUID = "8cd3d5", spawn_effect="Physical Attack"},
    {name="Morelull", chip_GUIDs = {"881291"}, model_GUID = "81ff79"},
    {name="Shiinotic", chip_GUIDs = {"378f55"}, model_GUID = "5430cf"},
    {name="Salandit", chip_GUIDs = {"c9434a"}, model_GUID = "b90790"},
    {name="Salazzle", chip_GUIDs = {"0c1f41"}, model_GUID = "29570d"},
    {name="Stufful", chip_GUIDs = {"6f482a"}, model_GUID = "556b6f", spawn_effect="Physical Attack"},
    {name="Bewear", chip_GUIDs = {"d01b11"}, model_GUID = "7d4606", spawn_effect="Status Attack"},
    {name="Bounsweet", chip_GUIDs = {"c32294"}, model_GUID = "631d93"},
    {name="Steenee", chip_GUIDs = {"c7cd01"}, model_GUID = "3d1e61"},
    {name="Tsareena", chip_GUIDs = {"32fbb4"}, model_GUID = "496a15"},
    {name="Comfey", chip_GUIDs = {"7e23aa"}, model_GUID = "91d031"},
    {name="Oranguru", chip_GUIDs = {"0416a2"}, model_GUID = "474004"},
    {name="Passimian", chip_GUIDs = {"d9945c"}, model_GUID = "afbf79"},
    {name="Wimpod", chip_GUIDs = {"cd22f9"}, model_GUID = "3b177b"},
    {name="Golisopod", chip_GUIDs = {"96ea10"}, model_GUID = "f4e89c"},
    {name="Sandygast", chip_GUIDs = {"6ed0e6"}, model_GUID = "c2f276"},
    {name="Palossand", chip_GUIDs = {"38d0aa","f05509"}, model_GUID = "d2d150"},
    {name="Pyukumuku", chip_GUIDs = {"bf26ac"}, model_GUID = "22fc93"},
    {name="Type: Null", chip_GUIDs = {"08a0ed"}, model_GUID = "f0496f"},
    {name="Silvally", chip_GUIDs = {"23673d"}, model_GUID = nil, spawn_effect="Status Attack", persistent_state=false, states = {
        {name="Normal Silvally", model_GUID = "7d0a3d"},
        {name="Bug Silvally", model_GUID = "47128d"},
        {name="Dark Silvally", model_GUID = "70f252"},
        {name="Dragon Silvally", model_GUID = "ea150e"},
        {name="Electric Silvally", model_GUID = "55656a"},
        {name="Fairy Silvally", model_GUID = "60adcb"},
        {name="Fighting Silvally", model_GUID = "dd039f"},
        {name="Fire Silvally", model_GUID = "2d17fc"},
        {name="Flying Silvally", model_GUID = "dffe6f"},
        {name="Ghost Silvally", model_GUID = "501503"},
        {name="Grass Silvally", model_GUID = "c457e4"},
        {name="Ground Silvally", model_GUID = "4981ad"},
        {name="Ice Silvally", model_GUID = "5735da"},
        {name="Poison Silvally", model_GUID = "820d1a"},
        {name="Psychic Silvally", model_GUID = "311ba0"},
        {name="Rock Silvally", model_GUID = "228266"},
        {name="Steel Silvally", model_GUID = "c05365"},
        {name="Water Silvally", model_GUID = "260ab9"},
    }},
    {name="Minior", chip_GUIDs = {"ba8025"}, model_GUID = nil, persistent_state=false, states = {
        {name="Meteor Minior", model_GUID = "1bfee7"},
        {name="Blue Core Minior", model_GUID = "13f374"},
        {name="Green Core Minior", model_GUID = "28c71b"},
        {name="Indigo Core Minior", model_GUID = "526005"},
        {name="Orange Core Minior", model_GUID = "f8af2c"},
        {name="Red Core Minior", model_GUID = "f9ea82"},
        {name="Violet Core Minior", model_GUID = "d484f2"},
        {name="Yellow Core Minior", model_GUID = "81e7d3"}
    }},
    {name="Komala", chip_GUIDs = {"cec9ad","81f307"}, model_GUID = "c45c5f", spawn_effect="Physical Attack"},
    {name="Turtonator", chip_GUIDs = {"65658b"}, model_GUID = "4c274a", spawn_effect="Status Attack"},
    {name="Togedemaru", chip_GUIDs = {"c7fd59"}, model_GUID = "e7484b"},
    {name="Mimikyu", chip_GUIDs = {"d97d42"}, model_GUID = nil, persistent_state=false, states = {
        {name="Normal Mimikyu", model_GUID = "48082a"},
        {name="Broken Mimikyu", model_GUID = "33d614", idle_effect="Broken Idle", run_effect="Broken Run", spawn_effect="Broken Special Attack", faint_effect="Broken Faint"}
    }},
    {name="Bruxish", chip_GUIDs = {"644cb1"}, model_GUID = "70f19d"},
    {name="Drampa", chip_GUIDs = {"da1661"}, model_GUID = "d1667b"},
    {name="Dhelmise", chip_GUIDs = {"006368","f307b1"}, model_GUID = "af1f36", spawn_effect="Physical Attack"},
    {name="Jangmo-o", chip_GUIDs = {"f80f9e"}, model_GUID = "c693f2"},
    {name="Hakamo-o", chip_GUIDs = {"b0b77d"}, model_GUID = "54dfcc"},
    {name="Kommo-o", chip_GUIDs = {"c4b1b2"}, model_GUID = "cbb201", spawn_effect="Status Attack"},
    {name="Tapu Koko", chip_GUIDs = {"6dfd32"}, model_GUID = "0cf91d"},
    {name="Tapu Lele", chip_GUIDs = {"d53477"}, model_GUID = "ff3a30", spawn_effect="Physical Attack"},
    {name="Tapu Bulu", chip_GUIDs = {"d957d2"}, model_GUID = "c5f3b1", spawn_effect="Status Attack"},
    {name="Tapu Fini", chip_GUIDs = {"b49c59"}, model_GUID = "0ade9c", spawn_effect="Status Attack"},
    {name="Cosmog", chip_GUIDs = {"588e86"}, model_GUID = "1ba3f7", spawn_effect="Physical Attack"},
    {name="Cosmoem", chip_GUIDs = {"7e8b08"}, model_GUID = "02834c"},
    {name="Solgaleo", chip_GUIDs = {"6a74d8"}, model_GUID = "aedf14", custom_scale=0.7},
    {name="Radiant-Sun Solgaleo", chip_GUIDs = {"235a58"}, model_GUID = "bbd295", custom_scale=0.7},
    {name="Lunala", chip_GUIDs = {"598e54"}, model_GUID = "20b6ae", spawn_effect="Status Attack", custom_scale=0.7},
    {name="Full-Moon Lunala", chip_GUIDs = {"d6bbb2"}, model_GUID = "567ea3", spawn_effect="Status Attack", custom_scale=0.7},
    {name="Nihilego", chip_GUIDs = {"ae6020"}, model_GUID = "24211e"},
    {name="Buzzwole", chip_GUIDs = {"0c46fb"}, model_GUID = "d4bad7"},
    {name="Pheromosa", chip_GUIDs = {"21383e"}, model_GUID = "9c9628"},
    {name="Xurkitree", chip_GUIDs = {"c0bed9"}, model_GUID = "dd3362", spawn_effect="Status Attack"},
    {name="Celesteela", chip_GUIDs = {"52cffe"}, model_GUID = "4ed33c", custom_scale=0.3},
    {name="Kartana", chip_GUIDs = {"2aff94"}, model_GUID = "96978b", spawn_effect="Status Attack"},
    {name="Guzzlord", chip_GUIDs = {"8159ad"}, model_GUID = "2754d6", spawn_effect="Status Attack", custom_scale=0.3},
    {name="Necrozma", chip_GUIDs = {"5716b0"}, model_GUID = "907d48", spawn_effect="Status Attack"},
    {name="DawnWings Necrozma", chip_GUIDs = {"0f3dad"}, model_GUID = "94ea0e", spawn_effect="Status Attack", custom_scale=0.7},
    {name="DuskMane Necrozma", chip_GUIDs = {"de12e6"}, model_GUID = "9aed20", custom_scale=0.7},
    {name="Ultra-Dream Necrozma", chip_GUIDs = {"b80c5d"}, model_GUID = "6becd1", custom_scale=0.5},
    {name="Magearna", chip_GUIDs = {"89a3df"}, model_GUID = nil, states = {
        {name="Normal Magearna", model_GUID = "318474"},
        {name="Original Color Magearna", model_GUID = "cdad62"}
    }},
    {name="Marshadow", chip_GUIDs = {"851f49"}, model_GUID = nil, persistent_state=false, states = {
        {name="Gloomdweller Marshadow", model_GUID = "bd5b48"},
        {name="Zenith Marshadow", model_GUID = "a7c4b7"}
    }},
    {name="Poipole", chip_GUIDs = {"c08365"}, model_GUID = "94b4e8", spawn_effect="Physical Attack"},
    {name="Naganadel", chip_GUIDs = {"32f05e"}, model_GUID = "e435dd", spawn_effect="Physical Attack"},
    {name="Stakataka", chip_GUIDs = {"72b966"}, model_GUID = "2e57e4", spawn_effect="Status Attack", custom_scale=0.3},
    {name="Blacephalon", chip_GUIDs = {"66a9f1"}, model_GUID = "d38e89"},
    {name="Zeraora", chip_GUIDs = {"1c8ea1"}, model_GUID = "7451bb"},
    {name="Alolan Rattata", chip_GUIDs = {"b3c8fd"}, model_GUID = "53c2a4"},
    {name="Alolan Raticate", chip_GUIDs = {"08e204"}, model_GUID = "905fd9"},
    {name="Alolan Raichu", chip_GUIDs = {"b87a08","d315b4"}, model_GUID = "ac9e94", spawn_effect="Physical Attack"},
    {name="Alolan Sandshrew", chip_GUIDs = {"c8977d"}, model_GUID = "52ee91"},
    {name="Alolan Sandslash", chip_GUIDs = {"3ba326","03df2a"}, model_GUID = "2b6482"},
    {name="Alolan Vulpix", chip_GUIDs = {"727b25"}, model_GUID = "cbe5dc"},
    {name="Alolan Ninetales", chip_GUIDs = {"4faf74"}, model_GUID = "654f90"},
    {name="Alolan Diglett", chip_GUIDs = {"5a9e65"}, model_GUID = "714593"},
    {name="Alolan Dugtrio", chip_GUIDs = {"b78e7d","868d79","a00579"}, model_GUID = "461e52"},
    {name="Alolan Meowth", chip_GUIDs = {"3ea183"}, model_GUID = "bcee34", spawn_effect="Physical Attack"},
    {name="Alolan Persian", chip_GUIDs = {"67adc2","4ba8dd"}, model_GUID = "0684cc"},
    {name="Alolan Geodude", chip_GUIDs = {"49a1b3"}, model_GUID = "0b6e11"},
    {name="Alolan Graveler", chip_GUIDs = {"6cec8a"}, model_GUID = "27bcd3"},
    {name="Alolan Golem", chip_GUIDs = {"b3b053","cffdb7"}, model_GUID = "18e42a", spawn_effect="Physical Attack"},
    {name="Alolan Grimer", chip_GUIDs = {"7e4ad3"}, model_GUID = "9b4419"},
    {name="Alolan Muk", chip_GUIDs = {"dabda1"}, model_GUID = "7e8a7f"},
    {name="Alolan Exeggutor", chip_GUIDs = {"0beae9"}, model_GUID = "e56668"},
    {name="Alolan Marowak", chip_GUIDs = {"a1ca2a"}, model_GUID = "245f21"},
    {name="Shiny Bulbasaur", chip_GUIDs = {"2e93ba"}, model_GUID = "ce107a", spawn_effect="Physical Attack"}, -- shinies start here
    {name="Shiny Ivysaur", chip_GUIDs = {"d30a23"}, model_GUID = "4be127", offset={x=0, y=0.075, z=0}},
    {name="Shiny Mega Venusaur", chip_GUIDs = {"a4de57"}, model_GUID = "2a7035", spawn_effect="Mega Evolve"},
    {name="Shiny Venusaur", chip_GUIDs = {"499b57"}, model_GUID = "200def", spawn_effect="Physical Attack"},
    {name="Shiny Charmander", chip_GUIDs = {"6e8b88"}, model_GUID = "5c53a0"},
    {name="Shiny Charmeleon", chip_GUIDs = {"fc976b"}, model_GUID = "a92175"},
    {name="Shiny Charizard", chip_GUIDs = {"016bb7"}, model_GUID = "d9c7c4", spawn_effect="Status Attack"},
    {name="Shiny Mega Charizard X", chip_GUIDs = {"022768"}, model_GUID = "af5e03", spawn_effect="Mega Evolve"},
    {name="Shiny Mega Charizard Y", chip_GUIDs = {"4f3869"}, model_GUID = "5dcfa8", spawn_effect="Mega Evolve"},
    {name="Shiny Squirtle", chip_GUIDs = {"7fcb69"}, model_GUID = "e446b9"},
    {name="Shiny Wartortle", chip_GUIDs = {"0fa126"}, model_GUID = "19594e"},
    {name="Shiny Blastoise", chip_GUIDs = {"62bf40"}, model_GUID = "8e8367"},
    {name="Shiny Mega Blastoise", chip_GUIDs = {"a81621"}, model_GUID = "89413e", spawn_effect="Mega Evolve"},
    {name="Shiny Caterpie", chip_GUIDs = {"7d14f1"}, model_GUID = "5e9618"},
    {name="Shiny Metapod", chip_GUIDs = {"608781"}, model_GUID = "0576bd"},
    {name="Shiny Butterfree", chip_GUIDs = {"374750"}, model_GUID = "4a10c4"},
    {name="Shiny Weedle", chip_GUIDs = {"d749d0"}, model_GUID = "88a17f"},
    {name="Shiny Kakuna", chip_GUIDs = {"d83e3a"}, model_GUID = "e31542"},
    {name="Shiny Beedrill", chip_GUIDs = {"d9d7b6"}, model_GUID = "3200d4", spawn_effect="Status Attack"},
    {name="Shiny Mega Beedrill", chip_GUIDs = {"54dd16"}, model_GUID = "b92c69", spawn_effect="Mega Evolve"},
    {name="Shiny Pidgey", chip_GUIDs = {"a8f8c4"}, model_GUID = "916538"},
    {name="Shiny Pidgeotto", chip_GUIDs = {"e9ef3b"}, model_GUID = "980fcf", spawn_effect="Physical Attack"},
    {name="Shiny Mega Pidgeot", chip_GUIDs = {"592695"}, model_GUID = "1ca0ee", spawn_effect="Mega Evolve"},
    {name="Shiny Pidgeot", chip_GUIDs = {"b67953"}, model_GUID = "f77969"},
    {name="Shiny Rattata", chip_GUIDs = {"20b5a1"}, model_GUID = "356a8e"},
    {name="Shiny Raticate", chip_GUIDs = {"4e89e1"}, model_GUID = "9df4c9"},
    {name="Shiny Spearow", chip_GUIDs = {"54b437"}, model_GUID = "d65ca8"},
    {name="Shiny Fearow", chip_GUIDs = {"67b606"}, model_GUID = "dc62af"},
    {name="Shiny Ekans", chip_GUIDs = {"29a7e1"}, model_GUID = "868356"},
    {name="Shiny Arbok", chip_GUIDs = {"8c0dfe"}, model_GUID = "581137"},
    {name="Shiny Pikachu", chip_GUIDs = {"fdd5db"}, model_GUID = "0813a3", spawn_effect="Status Attack"},
    {name="Shiny Raichu", chip_GUIDs = {"33bc26"}, model_GUID = "9e168b"},
    {name="Shiny Sandshrew", chip_GUIDs = {"eff68f"}, model_GUID = "7897ae"},
    {name="Shiny Sandslash", chip_GUIDs = {"cdbd4d"}, model_GUID = "dc4e80"},
    {name="Shiny Nidoran F", chip_GUIDs = {"d3b78c"}, model_GUID = "d57fe7", spawn_effect="Physical Attack"},
    {name="Shiny Nidorina", chip_GUIDs = {"549304"}, model_GUID = "8259b1"},
    {name="Shiny Nidoqueen", chip_GUIDs = {"d211a9"}, model_GUID = "c3d25c"},
    {name="Shiny Nidoran M", chip_GUIDs = {"c1c211"}, model_GUID = "856c3a", spawn_effect="Physical Attack"},
    {name="Shiny Nidorino", chip_GUIDs = {"e26b58"}, model_GUID = "9eebbc"},
    {name="Shiny Nidoking", chip_GUIDs = {"324503"}, model_GUID = "8e9063"},
    {name="Shiny Clefairy", chip_GUIDs = {"d90240"}, model_GUID = "0b4be2"},
    {name="Shiny Clefable", chip_GUIDs = {"8caf3b"}, model_GUID = "f9e97d"},
    {name="Shiny Vulpix", chip_GUIDs = {"e1c5ed"}, model_GUID = "77515c"},
    {name="Shiny Ninetales", chip_GUIDs = {"b37474"}, model_GUID = "f93407"},
    {name="Shiny Jigglypuff", chip_GUIDs = {"9029b8"}, model_GUID = "d17351"},
    {name="Shiny Wigglytuff", chip_GUIDs = {"7e7c0e"}, model_GUID = "b88e1f", spawn_effect="Physical Attack"},
    {name="Shiny Zubat", chip_GUIDs = {"3493ea"}, model_GUID = "fc68e4", spawn_effect="Physical Attack"},
    {name="Shiny Golbat", chip_GUIDs = {"03dc37"}, model_GUID = "d8e4d1"},
    {name="Shiny Oddish", chip_GUIDs = {"3b526d"}, model_GUID = "9377a5"},
    {name="Shiny Gloom", chip_GUIDs = {"7ddedb"}, model_GUID = "8e146d", spawn_effect="Physical Attack"},
    {name="Shiny Vileplume", chip_GUIDs = {"83bae2"}, model_GUID = "e44d78"},
    {name="Shiny Paras", chip_GUIDs = {"7eb930"}, model_GUID = "f8dfb3", spawn_effect="Physical Attack"},
    {name="Shiny Parasect", chip_GUIDs = {"cd665a"}, model_GUID = "a4e788"},
    {name="Shiny Venonat", chip_GUIDs = {"ad1558"}, model_GUID = "7b0f04"},
    {name="Shiny Venomoth", chip_GUIDs = {"d38e3b"}, model_GUID = "434604"},
    {name="Shiny Diglett", chip_GUIDs = {"6b7d04"}, model_GUID = "b1e655"},
    {name="Shiny Dugtrio", chip_GUIDs = {"0d7b9e"}, model_GUID = "dc3cf3"},
    {name="Shiny Meowth", chip_GUIDs = {"3bdee3"}, model_GUID = "5aaf52", spawn_effect="Physical Attack"},
    {name="Shiny Persian", chip_GUIDs = {"74752a"}, model_GUID = "eadc0d"},
    {name="Shiny Psyduck", chip_GUIDs = {"c2b89e"}, model_GUID = "0128eb"},
    {name="Shiny Golduck", chip_GUIDs = {"1da8b9"}, model_GUID = "cb9ff3"},
    {name="Shiny Mankey", chip_GUIDs = {"9764c0"}, model_GUID = "cba5af", spawn_effect="Physical Attack"},
    {name="Shiny Primeape", chip_GUIDs = {"295c32"}, model_GUID = "5efaed", spawn_effect="Physical Attack"},
    {name="Shiny Growlithe", chip_GUIDs = {"e9138f"}, model_GUID = "05f9d8"},
    {name="Shiny Arcanine", chip_GUIDs = {"f1774f"}, model_GUID = "817577"},
    {name="Shiny Poliwag", chip_GUIDs = {"05a5ab"}, model_GUID = "6bacb5", spawn_effect="Physical Attack"},
    {name="Shiny Poliwhirl", chip_GUIDs = {"0062a6"}, model_GUID = "7b9714"},
    {name="Shiny Poliwrath", chip_GUIDs = {"97e0a2"}, model_GUID = "82369a"},
    {name="Shiny Abra", chip_GUIDs = {"ddf9a6"}, model_GUID = "cef513"},
    {name="Shiny Kadabra", chip_GUIDs = {"756e29"}, model_GUID = "2be0c1"},
    {name="Shiny Alakazam", chip_GUIDs = {"9b3a03"}, model_GUID = "d3b763"},
    {name="Shiny Mega Alakazam", chip_GUIDs = {"26e50a"}, model_GUID = "9db6ff", spawn_effect="Mega Evolve"},
    {name="Shiny Machop", chip_GUIDs = {"3ac388"}, model_GUID = "9893dc", spawn_effect="Physical Attack"},
    {name="Shiny Machoke", chip_GUIDs = {"fc1252"}, model_GUID = "cdebfd"},
    {name="Shiny Machamp", chip_GUIDs = {"deca35"}, model_GUID = "b8d22b", spawn_effect="Physical Attack"},
    {name="Shiny Bellsprout", chip_GUIDs = {"d6cab5"}, model_GUID = "6ab8f8"},
    {name="Shiny Weepinbell", chip_GUIDs = {"c70406"}, model_GUID = "199435"},
    {name="Shiny Victreebell", chip_GUIDs = {"300fdf"}, model_GUID = "6f209a", spawn_effect="Physical Attack"},
    {name="Shiny Tentacool", chip_GUIDs = {"332e74"}, model_GUID = "9e2aa7"},
    {name="Shiny Tentacruel", chip_GUIDs = {"a3c62a"}, model_GUID = "4be01c"},
    {name="Shiny Geodude", chip_GUIDs = {"197ee0"}, model_GUID = "fb3e5e", spawn_effect="Physical Attack"},
    {name="Shiny Graveler", chip_GUIDs = {"2fa5fd"}, model_GUID = "20223a", spawn_effect="Physical Attack"},
    {name="Shiny Golem", chip_GUIDs = {"dcd076"}, model_GUID = "a5ba0c", spawn_effect="Physical Attack"},
    {name="Shiny Ponyta", chip_GUIDs = {"20d40a"}, model_GUID = "51b75a"},
    {name="Shiny Rapidash", chip_GUIDs = {"99928e"}, model_GUID = "e05be1"},
    {name="Shiny Slowpoke", chip_GUIDs = {"fa3fc7"}, model_GUID = "e3e18e"},
    {name="Shiny Mega Slowbro", chip_GUIDs = {"85e73d"}, model_GUID = "bbe440", spawn_effect="Mega Evolve"},
    {name="Shiny Slowbro", chip_GUIDs = {"d8f0ac"}, model_GUID = "dd9fbd"},
    {name="Shiny Magnemite", chip_GUIDs = {"5f64f4"}, model_GUID = "f732d3"},
    {name="Shiny Magneton", chip_GUIDs = {"6ec9bd"}, model_GUID = "7088a6"},
    {name="Shiny Farfetchd", chip_GUIDs = {"c9767a"}, model_GUID = "51f36f", spawn_effect="Physical Attack"},
    {name="Shiny Doduo", chip_GUIDs = {"f105bb"}, model_GUID = "2f40a5"},
    {name="Shiny Dodrio", chip_GUIDs = {"a1d4e8"}, model_GUID = "58184b"},
    {name="Shiny Seel", chip_GUIDs = {"833804"}, model_GUID = "72c49c"},
    {name="Shiny Dewgong", chip_GUIDs = {"cb8a03"}, model_GUID = "da364d", spawn_effect="Physical Attack"},
    {name="Shiny Grimer", chip_GUIDs = {"676de6"}, model_GUID = "d62fbf"},
    {name="Shiny Muk", chip_GUIDs = {"7c97f6"}, model_GUID = "3cfcc4"},
    {name="Shiny Shellder", chip_GUIDs = {"f936cc"}, model_GUID = "2634f4", spawn_effect="Physical Attack"},
    {name="Shiny Cloyster", chip_GUIDs = {"f7cf89"}, model_GUID = "a11b62"},
    {name="Shiny Gastly", chip_GUIDs = {"404e07"}, model_GUID = "5739a2"},
    {name="Shiny Haunter", chip_GUIDs = {"88469d"}, model_GUID = "56f3c2"},
    {name="Shiny Gengar", chip_GUIDs = {"ffd304","c6640d"}, model_GUID = "bd3d92"},
    {name="Shiny Mega Gengar", chip_GUIDs = {"27f678"}, model_GUID = "5f3fbc", spawn_effect="Mega Evolve"},
    {name="Shiny Onix", chip_GUIDs = {"668908"}, model_GUID = "55561b", custom_scale=0.7, offset={x=0,y=0,z=1.5}},
    {name="Shiny Drowzee", chip_GUIDs = {"70ccb9"}, model_GUID = "b6768e"},
    {name="Shiny Hypno", chip_GUIDs = {"8927ec"}, model_GUID = "b8ddf1"},
    {name="Shiny Krabby", chip_GUIDs = {"54b29c"}, model_GUID = "77f102"},
    {name="Shiny Kingler", chip_GUIDs = {"8bc43e"}, model_GUID = "801efe"},
    {name="Shiny Voltorb", chip_GUIDs = {"eec976"}, model_GUID = "3a7746", spawn_effect="Physical Attack"},
    {name="Shiny Electrode", chip_GUIDs = {"43b2cc"}, model_GUID = "f5e890", spawn_effect="Physical Attack"},
    {name="Shiny Exeggcute", chip_GUIDs = {"9f90fb"}, model_GUID = "ac5805"},
    {name="Shiny Exeggutor", chip_GUIDs = {"a865ce"}, model_GUID = "9bf3d1"},
    {name="Shiny Cubone", chip_GUIDs = {"7d8ea9"}, model_GUID = "c7dcf7", spawn_effect="Physical Attack"},
    {name="Shiny Marowak", chip_GUIDs = {"4cc3fb"}, model_GUID = "4ace60"},
    {name="Shiny Hitmonlee", chip_GUIDs = {"2756a6"}, model_GUID = "82d623", spawn_effect="Physical Attack"},
    {name="Shiny Hitmonchan", chip_GUIDs = {"edae9a"}, model_GUID = "cc6ef1", spawn_effect="Physical Attack"},
    {name="Shiny Lickitung", chip_GUIDs = {"f688ab"}, model_GUID = "d245d6"},
    {name="Shiny Koffing", chip_GUIDs = {"7bc6da"}, model_GUID = "21f4d8"},
    {name="Shiny Weezing", chip_GUIDs = {"01ac71"}, model_GUID = "ab61d6"},
    {name="Shiny Rhyhorn", chip_GUIDs = {"e98e88"}, model_GUID = "c00df6"},
    {name="Shiny Rhydon", chip_GUIDs = {"0df483"}, model_GUID = "9162c8"},
    {name="Shiny Chansey", chip_GUIDs = {"4f9a7a"}, model_GUID = "a13745"},
    {name="Shiny Tangela", chip_GUIDs = {"fd0569"}, model_GUID = "716adc"},
    {name="Shiny Kangaskhan", chip_GUIDs = {"9458d8"}, model_GUID = "238adc"},
    {name="Shiny Mega Kangaskhan", chip_GUIDs = {"660f55"}, model_GUID = "62422f", spawn_effect="Mega Evolve"},
    {name="Shiny Horsea", chip_GUIDs = {"133cdc"}, model_GUID = "4f467c"},
    {name="Shiny Seadra", chip_GUIDs = {"5c26b4"}, model_GUID = "5d7ef1"},
    {name="Shiny Goldeen", chip_GUIDs = {"d14cc9"}, model_GUID = "ed87e7"},
    {name="Shiny Seaking", chip_GUIDs = {"ecbaca"}, model_GUID = "f6c5eb"},
    {name="Shiny Staryu", chip_GUIDs = {"047337"}, model_GUID = "5c7655", spawn_effect="Physical Attack"},
    {name="Shiny Starmie", chip_GUIDs = {"7a9565"}, model_GUID = "466695"},
    {name="Shiny Mr. Mime", chip_GUIDs = {"e714b1"}, model_GUID = "2667ff"},
    {name="Shiny Scyther", chip_GUIDs = {"0223a7"}, model_GUID = "03cf09"},
    {name="Shiny Jynx", chip_GUIDs = {"d1c936"}, model_GUID = "fadfa4"},
    {name="Shiny Electabuzz", chip_GUIDs = {"5a127b"}, model_GUID = "ba0139"},
    {name="Shiny Magmar", chip_GUIDs = {"494358"}, model_GUID = "1782d2"},
    {name="Shiny Mega Pinsir", chip_GUIDs = {"3b6668"}, model_GUID = "879011", spawn_effect="Mega Evolve"},
    {name="Shiny Pinsir", chip_GUIDs = {"7d11f1"}, model_GUID = "9929fe"},
    {name="Shiny Tauros", chip_GUIDs = {"2cc48b"}, model_GUID = "3c3ab0", spawn_effect="Physical Attack"},
    {name="Shiny Magikarp", chip_GUIDs = {"2739f6"}, model_GUID = "b2685b", spawn_effect="Physical Attack"},
    {name="Shiny Gyarados", chip_GUIDs = {"f177eb"}, model_GUID = "a9abeb", custom_scale=0.8},
    {name="Shiny Mega Gyarados", chip_GUIDs = {"e66ab2"}, model_GUID = "55036d", spawn_effect="Mega Evolve", custom_scale=0.8},
    {name="Shiny Lapras", chip_GUIDs = {"4ffdf1"}, model_GUID = "59749d"},
    {name="Shiny Ditto", chip_GUIDs = {"64d845"}, model_GUID = "b1b7b1"},
    {name="Shiny Eevee", chip_GUIDs = {"415e89"}, model_GUID = "e61016"},
    {name="Shiny Vaporeon", chip_GUIDs = {"6dff11"}, model_GUID = "50b824"},
    {name="Shiny Jolteon", chip_GUIDs = {"aae0bd"}, model_GUID = "1575fc"},
    {name="Shiny Flareon", chip_GUIDs = {"a2b688"}, model_GUID = "81d0f0"},
    {name="Shiny Porygon", chip_GUIDs = {"d2127f"}, model_GUID = "50b4a7"},
    {name="Shiny Omanyte", chip_GUIDs = {"7a3c3f"}, model_GUID = "965d80"},
    {name="Shiny Omastar", chip_GUIDs = {"72f26d"}, model_GUID = "d3b126"},
    {name="Shiny Kabuto", chip_GUIDs = {"9fc7a6"}, model_GUID = "c6da4c"},
    {name="Shiny Kabutops", chip_GUIDs = {"fcaf84"}, model_GUID = "a3e4a4"},
    {name="Shiny Aerodactyl", chip_GUIDs = {"85b1e9"}, model_GUID = "704548"},
    {name="Shiny Mega Aerodactyl", chip_GUIDs = {"53b824"}, model_GUID = "455c47", spawn_effect="Mega Evolve"},
    {name="Shiny Snorlax", chip_GUIDs = {"5a04fc"}, model_GUID = "d5c0c0", spawn_effect="Physical Attack"},
    {name="Shiny Dratini", chip_GUIDs = {"2d2848"}, model_GUID = "e8a2a0"},
    {name="Shiny Dragonair", chip_GUIDs = {"ae8ef7"}, model_GUID = "24c3db"},
    {name="Shiny Dragonite", chip_GUIDs = {"5bafae"}, model_GUID = "33a9dc"},
    {name="Shiny Mew", chip_GUIDs = {"6eb4bf"}, model_GUID = "6f68b4", spawn_effect="Physical Attack"},
    {name="Shiny Mewtwo", chip_GUIDs = {"d18e70"}, model_GUID = "e26cda"},
    {name="Shiny Mega Mewtwo X", chip_GUIDs = {"4692cf"}, model_GUID = "4b3d7e", spawn_effect="Mega Evolve"},
    {name="Shiny Mega Mewtwo Y", chip_GUIDs = {"6a96a8"}, model_GUID = "2572ec", spawn_effect="Mega Evolve"},
    {name="Shiny Zapdos", chip_GUIDs = {"6c50c4"}, model_GUID = "d2fe8e"},
    {name="Shiny Moltres", chip_GUIDs = {"1a7b84"},  model_GUID = "0fe6bd", spawn_effect="Physical Attack"},
    {name="Shiny Articuno", chip_GUIDs = {"ca5e04"}, model_GUID = "f4f334"},
    {name="Shiny Chikorita", chip_GUIDs = {"7bbc8b"}, model_GUID = "6a8ff3"},
    {name="Shiny Bayleef", chip_GUIDs = {"b0afca"}, model_GUID = "5172c7"},
    {name="Shiny Meganium", chip_GUIDs = {"ecffa6"}, model_GUID = "a6d672"},
    {name="Shiny Cyndaquil", chip_GUIDs = {"612771"}, model_GUID = "c253e6"},
    {name="Shiny Quilava", chip_GUIDs = {"5bf288"}, model_GUID = "3b8963"},
    {name="Shiny Typhlosion", chip_GUIDs = {"9fa03b"}, model_GUID = "ad941e"},
    {name="Shiny Totodile", chip_GUIDs = {"f810f3"}, model_GUID = "2ca6c0"},
    {name="Shiny Croconaw", chip_GUIDs = {"6fc207"}, model_GUID = "afba48"},
    {name="Shiny Feraligatr", chip_GUIDs = {"b3098f"}, model_GUID = "6d39ce", spawn_effect="Status Attack"},
    {name="Shiny Sentret", chip_GUIDs = {"e98768"}, model_GUID = "cbc534"},
    {name="Shiny Furret", chip_GUIDs = {"546907"}, model_GUID = "299b7a", spawn_effect="Physical Attack"},
    {name="Shiny Hoothoot", chip_GUIDs = {"8f2e0f"}, model_GUID = "1cdfac"},
    {name="Shiny Noctowl", chip_GUIDs = {"99c106"}, model_GUID = "5824c7"},
    {name="Shiny Ledyba", chip_GUIDs = {"262cfe"}, model_GUID = "284a12"},
    {name="Shiny Ledian", chip_GUIDs = {"297f90"}, model_GUID = "9330b8"},
    {name="Shiny Spinarak", chip_GUIDs = {"4f8061"}, model_GUID = "7b3b9a"},
    {name="Shiny Ariados", chip_GUIDs = {"7351fc"}, model_GUID = "b95f6d"},
    {name="Shiny Crobat", chip_GUIDs = {"f39f76"}, model_GUID = "bb8170"},
    {name="Shiny Chinchou", chip_GUIDs = {"38169f"}, model_GUID = "2b2fea", spawn_effect="Physical Attack"},
    {name="Shiny Lanturn", chip_GUIDs = {"e643a6"}, model_GUID = "4c90fa"},
    {name="Shiny Pichu", chip_GUIDs = {"766c23"}, model_GUID = "933586", spawn_effect="Physical Attack"},
    {name="Shiny Cleffa", chip_GUIDs = {"8d2b3f"}, model_GUID = "4e5ec3"},
    {name="Shiny Igglybuff", chip_GUIDs = {"9cdc0c"}, model_GUID = "f63e03"},
    {name="Shiny Togepi", chip_GUIDs = {"efdf43"}, model_GUID = "f40222", spawn_effect="Physical Attack"},
    {name="Shiny Togetic", chip_GUIDs = {"8ab93b"}, model_GUID = "ffdf79"},
    {name="Shiny Natu", chip_GUIDs = {"c52fe6"}, model_GUID = "51f082"},
    {name="Shiny Xatu", chip_GUIDs = {"23a4a3"}, model_GUID = "d0856a"},
    {name="Shiny Mareep", chip_GUIDs = {"544bb5"}, model_GUID = "4676e5"},
    {name="Shiny Flaffy", chip_GUIDs = {"90eae1"}, model_GUID = "efc02b"},
    {name="Shiny Ampharos", chip_GUIDs = {"6addc1"}, model_GUID = "267924"},
    {name="Shiny Mega Ampharos", chip_GUIDs = {"a0b3ac"}, model_GUID = "18c344", spawn_effect="Mega Evolve"},
    {name="Shiny Bellossom", chip_GUIDs = {"eda8a6"}, model_GUID = "ab26b7", spawn_effect="Physical Attack"},
    {name="Shiny Marill", chip_GUIDs = {"7563b6"}, model_GUID = "52a72f"},
    {name="Shiny Azumarill", chip_GUIDs = {"4710f0"}, model_GUID = "cc3ef1"},
    {name="Shiny Sudowoodo", chip_GUIDs = {"a6be7e"}, model_GUID = "97345e", spawn_effect="Physical Attack"},
    {name="Shiny Politoed", chip_GUIDs = {"901c89"}, model_GUID = "8ea3e6"},
    {name="Shiny Hoppip", chip_GUIDs = {"875ba7"}, model_GUID = "34522a"},
    {name="Shiny Skiploom", chip_GUIDs = {"7ae798"}, model_GUID = "5dc901", spawn_effect="Physical Attack"},
    {name="Shiny Jumpluff", chip_GUIDs = {"7e70c4"}, model_GUID = "35de88", spawn_effect="Physical Attack"},
    {name="Shiny Aipom", chip_GUIDs = {"c4e9c6"}, model_GUID = "d4e8ff"},
    {name="Shiny Sunkern", chip_GUIDs = {"3539ac"}, model_GUID = "43f9da"},
    {name="Shiny Sunflora", chip_GUIDs = {"17a850"}, model_GUID = "76a35f"},
    {name="Shiny Yanma", chip_GUIDs = {"17d3f7"}, model_GUID = "174c68", spawn_effect="Physical Attack"},
    {name="Shiny Wooper", chip_GUIDs = {"cdd748"}, model_GUID = "80411b"},
    {name="Shiny Quagsire", chip_GUIDs = {"88504e"}, model_GUID = "830cc3"},
    {name="Shiny Espeon", chip_GUIDs = {"dc8122"}, model_GUID = "dbe3f1"},
    {name="Shiny Umbreon", chip_GUIDs = {"e9ce3c"}, model_GUID = "957e5c"},
    {name="Shiny Murkrow", chip_GUIDs = {"8450df"}, model_GUID = "83f6d6"},
    {name="Shiny Slowking", chip_GUIDs = {"442054"}, model_GUID = "c481e4"},
    {name="Shiny Misdreavus", chip_GUIDs = {"3dcfc2"}, model_GUID = "aa041d", spawn_effect="Physical Attack"},
    {name="Shiny Unown", chip_GUIDs = {"cd5b16"}, model_GUID = "acb946", spawn_effect="Hidden Power"},
    {name="Shiny Wobbuffet", chip_GUIDs = {"43b303"}, model_GUID = "37d06e"},
    {name="Shiny Girafarig", chip_GUIDs = {"db1afc"}, model_GUID = "515969"},
    {name="Shiny Pineco", chip_GUIDs = {"68334e"}, model_GUID = "221866", spawn_effect="Physical Attack"},
    {name="Shiny Forretress", chip_GUIDs = {"5acd1e"}, model_GUID = "68a3a4"},
    {name="Shiny Dunsparce", chip_GUIDs = {"93e285"}, model_GUID = "fca349"},
    {name="Shiny Gligar", chip_GUIDs = {"0260bf"}, model_GUID = "f870ba"},
    {name="Shiny Mega Steelix", chip_GUIDs = {"b22e5e"}, model_GUID = "0fa1be", spawn_effect="Physical Attack", offset={x=0,y=0,z=2.5}, custom_scale=0.7},
    {name="Shiny Steelix", chip_GUIDs = {"268600"}, model_GUID = "c676c0", custom_scale=0.7},
    {name="Shiny Snubbull", chip_GUIDs = {"19c6f4"}, model_GUID = "a87211"},
    {name="Shiny Granbull", chip_GUIDs = {"a0f307"}, model_GUID = "fbdd73"},
    {name="Shiny Qwilfish", chip_GUIDs = {"f8d724"}, model_GUID = "b5940a"},
    {name="Shiny Mega Scizor", chip_GUIDs = {"b6a4a4"}, model_GUID = "0f3bd3", spawn_effect="Mega Evolve"},
    {name="Shiny Scizor", chip_GUIDs = {"0641f5"}, model_GUID = "f2574f", spawn_effect="Status Attack"},
    {name="Shiny Shuckle", chip_GUIDs = {"80f352"}, model_GUID = "58a469", spawn_effect="Physical Attack"},
    {name="Shiny Heracross", chip_GUIDs = {"362aa3"}, model_GUID = "a66c76"},
    {name="Shiny Mega Heracross", chip_GUIDs = {"9dc75c"}, model_GUID = "2cbc00", spawn_effect="Mega Evolve"},
    {name="Shiny Sneasel", chip_GUIDs = {"2a5a01"}, model_GUID = "17ce1e"},
    {name="Shiny Teddiursa", chip_GUIDs = {"fd9b25"}, model_GUID = "3cb310"},
    {name="Shiny Ursaring", chip_GUIDs = {"00a834"}, model_GUID = "3f19f5"},
    {name="Shiny Slugma", chip_GUIDs = {"d00e3b"}, model_GUID = "f7f4ca"},
    {name="Shiny Magcargo", chip_GUIDs = {"da84a2"}, model_GUID = "a20540"},
    {name="Shiny Swinub", chip_GUIDs = {"40ae8a"}, model_GUID = "8f6378", spawn_effect="Physical Attack"},
    {name="Shiny Piloswine", chip_GUIDs = {"e1cfb0"}, model_GUID = "1fc0f5"},
    {name="Shiny Corsola", chip_GUIDs = {"1b6dc0"}, model_GUID = "2ba029", spawn_effect="Physical Attack"},
    {name="Shiny Remoraid", chip_GUIDs = {"bf1b2d"}, model_GUID = "a875ff"},
    {name="Shiny Octillery", chip_GUIDs = {"6c40ff"}, model_GUID = "b0d2e0"},
    {name="Shiny Delibird", chip_GUIDs = {"11cd84"}, model_GUID = "40c692", spawn_effect="Physical Attack"},
    {name="Shiny Mantine", chip_GUIDs = {"2d0037"}, model_GUID = "122e11", spawn_effect="Physical Attack"},
    {name="Shiny Skarmory", chip_GUIDs = {"e52582"}, model_GUID = "6083bc", spawn_effect="Physical Attack"},
    {name="Shiny Houndour", chip_GUIDs = {"b44b3b"}, model_GUID = "e09c46"},
    {name="Shiny Houndoom", chip_GUIDs = {"613494"}, model_GUID = "415a2d"},
    {name="Shiny Mega Houndoom", chip_GUIDs = {"4eab34"}, model_GUID = "1077eb", spawn_effect="Mega Evolve"},
    {name="Shiny Kingdra", chip_GUIDs = {"5e4dcb"}, model_GUID = "b71d17"},
    {name="Shiny Phanpy", chip_GUIDs = {"96e6d9"}, model_GUID = "5f5eeb"},
    {name="Shiny Donphan", chip_GUIDs = {"447a8b"}, model_GUID = "a1cd66", spawn_effect="Physical Attack"},
    {name="Shiny Porygon 2", chip_GUIDs = {"ed25e8"}, model_GUID = "dc6f1d"},
    {name="Shiny Stantler", chip_GUIDs = {"220bfb"}, model_GUID = "13fd48"},
    {name="Shiny Smeargle", chip_GUIDs = {"90b60c"}, model_GUID = "39b410"},
    {name="Shiny Tyrogue", chip_GUIDs = {"358d99"}, model_GUID = "2aeabe"},
    {name="Shiny Hitmontop", chip_GUIDs = {"455c93"}, model_GUID = "d90d3a", spawn_effect="Physical Attack"},
    {name="Shiny Smoochum", chip_GUIDs = {"778c27"}, model_GUID = "011ab9"},
    {name="Shiny Elekid", chip_GUIDs = {"201543"}, model_GUID = "148c51"},
    {name="Shiny Magby", chip_GUIDs = {"4fa577"}, model_GUID = "acd188"},
    {name="Shiny Miltank", chip_GUIDs = {"a69c97"}, model_GUID = "dd32b2", spawn_effect="Physical Attack"},
    {name="Shiny Blissey", chip_GUIDs = {"ea86e5"}, model_GUID = "3124c5", spawn_effect="Status Attack"},
    {name="Shiny Raiku", chip_GUIDs = {"274dd4"}, model_GUID = "9ab103"},
    {name="Shiny Entei", chip_GUIDs = {"57d0e5"}, model_GUID = "0b780c"},
    {name="Shiny Suicune", chip_GUIDs = {"afe772"}, model_GUID = "50b743"},
    {name="Shiny Larvitar", chip_GUIDs = {"23673d"}, model_GUID = "83f145"},
    {name="Shiny Pupitar", chip_GUIDs = {"c8386a"}, model_GUID = "b77f9d"},
    {name="Shiny Mega Tyranitar", chip_GUIDs = {"e56007"}, model_GUID = "79436c", spawn_effect="Mega Evolve"},
    {name="Shiny Tyranitar", chip_GUIDs = {"681b42"}, model_GUID = "39e1fc"},
    {name="Shiny Lugia", chip_GUIDs = {"b76c61"}, model_GUID = "fb00ac"},
    {name="Shiny Ho-Oh", chip_GUIDs = {"012e22"}, model_GUID = "97f9ed"},
    {name="Shiny Celebi", chip_GUIDs = {"06a572"}, model_GUID = "97af89"},
    {name="Shiny Azurill", chip_GUIDs = {"c10778"}, model_GUID = "f02a60"},
    {name="Shiny Wynaut", chip_GUIDs = {"6a4b33"}, model_GUID = "d153fa"},
    {name="Shiny Ambipom", chip_GUIDs = {"b345e0"}, model_GUID = "93ff19"},
    {name="Shiny Mismagius", chip_GUIDs = {"dbde27"}, model_GUID = "4e7856", spawn_effect="Physical Attack"},
    {name="Shiny Honchkrow", chip_GUIDs = {"5eb858"}, model_GUID = "6234c8"},
    {name="Shiny Bonsly", chip_GUIDs = {"2c14f5"}, model_GUID = "d157ef", spawn_effect="Physical Attack"},
    {name="Shiny Mime Jr.", chip_GUIDs = {"6e09af"}, model_GUID = "781934", spawn_effect="Physical Attack"},
    {name="Shiny Happiny", chip_GUIDs = {"e0303c"}, model_GUID = "91142e"},
    {name="Shiny Munchlax", chip_GUIDs = {"9f3bf0"}, model_GUID = "d1f1b7", spawn_effect="Physical Attack"},
    {name="Shiny Riolu", chip_GUIDs = {"b7d829"}, model_GUID = "0f2152"},
    {name="Shiny Lucario", chip_GUIDs = {'19cab6'}, model_GUID = "67e6e2"},
    {name="Shiny Mega Lucario", chip_GUIDs = {"2e12cc"}, model_GUID = "78a34d", spawn_effect="Mega Evolve"},
    {name="Shiny Mantyke", chip_GUIDs = {"ffdded"}, model_GUID = "3a9c1b", spawn_effect="Physical Attack"},
    {name="Shiny Weavile", chip_GUIDs = {"4335e5"}, model_GUID = "23d7c4", spawn_effect="Physical Attack"},
    {name="Shiny Magnezone", chip_GUIDs = {"ad864a"}, model_GUID = "384096"},
    {name="Shiny Lickilicky", chip_GUIDs = {"e6c7ef"}, model_GUID = "ecb850", spawn_effect="Physical Attack"},
    {name="Shiny Rhyperior", chip_GUIDs = {"e6539f"}, model_GUID = "48a5b4", spawn_effect="Physical Attack"},
    {name="Shiny Tangrowth", chip_GUIDs = {"007b78"}, model_GUID = "6b20db"},
    {name="Shiny Electivire", chip_GUIDs = {"980e08"}, model_GUID = "6b5280"},
    {name="Shiny Magmortar", chip_GUIDs = {"abaa02"}, model_GUID = "26aa87"},
    {name="Shiny Togekiss", chip_GUIDs = {"7b2f86"}, model_GUID = "387a37"},
    {name="Shiny Yanmega", chip_GUIDs = {"5b9816"}, model_GUID = "f75197"},
    {name="Shiny Leafeon", chip_GUIDs = {"9339d3"}, model_GUID = "cbf8ca"},
    {name="Shiny Glaceon", chip_GUIDs = {"78704f"}, model_GUID = "51f53d"},
    {name="Shiny Gliscor", chip_GUIDs = {"b5a713"}, model_GUID = "eb9911"},
    {name="Shiny Mamoswine", chip_GUIDs = {"3265e8"}, model_GUID = "ce52eb"},
    {name="Shiny Porygon-Z", chip_GUIDs = {"b9202e"}, model_GUID = "f3afbf"},
    {name="Shiny Sylveon", chip_GUIDs = {"3ce339"}, model_GUID = "c75fe3"},
    {name="Shiny Treecko", chip_GUIDs = {"7efc70"}, model_GUID = "877f25"},
	{name="Shiny Grovyle", chip_GUIDs = {"368658"}, model_GUID = "403d04"},
    {name="Shiny Sceptile", chip_GUIDs = {"ed990e"}, model_GUID = "f53ee1", spawn_effect="Status Attack"},
	{name="Shiny Mega Sceptile", chip_GUIDs = {"506972"}, model_GUID = "68f6a8", spawn_effect="Mega Evolve"},
	{name="Shiny Torchic", chip_GUIDs = {"0e21db"}, model_GUID = "2e4107", spawn_effect="Physical Attack"},
	{name="Shiny Combusken", chip_GUIDs = {"4bd614"}, model_GUID = "2421f9", spawn_effect="Physical Attack"},
    {name="Shiny Blaziken", chip_GUIDs = {"3a5193"}, model_GUID = "0665cd", spawn_effect="Status Attack"},
	{name="Shiny Mega Blaziken", chip_GUIDs = {"0dfcd3"}, model_GUID = "4d4112", spawn_effect="Mega Evolve"},
	{name="Shiny Mudkip", chip_GUIDs = {"d6f122"}, model_GUID = "0d9c6b"},
	{name="Shiny Marshtomp", chip_GUIDs = {"d220ee"}, model_GUID = "68c183"},
	{name="Shiny Swampert", chip_GUIDs = {"b78bda"}, model_GUID = "5f5579", spawn_effect="Physical Attack"},
    {name="Shiny Mega Swampert", chip_GUIDs = {"f151c6"}, model_GUID = "a166cf", spawn_effect="Mega Evolve"},
	{name="Shiny Poochyena", chip_GUIDs = {"dd265b"}, model_GUID = "6cb694"},
	{name="Shiny Mightyena", chip_GUIDs = {"42a6a6"}, model_GUID = "f3c111"},
	{name="Shiny Zigzagoon", chip_GUIDs = {"4e5e93"}, model_GUID = "2347dc"},
	{name="Shiny Linoone", chip_GUIDs = {"4edb68"}, model_GUID = "0663f8"},
	{name="Shiny Wurmple", chip_GUIDs = {"51d4b6"}, model_GUID = "8b1001"},
	{name="Shiny Silcoon", chip_GUIDs = {"974b2c"}, model_GUID = "188128", spawn_effect="Physical Attack"},
	{name="Shiny Beautifly", chip_GUIDs = {"0c7cbd"}, model_GUID = "d65b1b"},
	{name="Shiny Cascoon", chip_GUIDs = {"305853"}, model_GUID = "2bc361", spawn_effect="Physical Attack"},
	{name="Shiny Dustox", chip_GUIDs = {"d83b03"}, model_GUID = "f783a6"},
	{name="Shiny Lotad", chip_GUIDs = {"da5480"}, model_GUID = "d576a7"},
	{name="Shiny Lombre", chip_GUIDs = {"c88db7"}, model_GUID = "d4061e"},
	{name="Shiny Ludicolo", chip_GUIDs = {"2bf1de"}, model_GUID = "1d8093"},
	{name="Shiny Seedot", chip_GUIDs = {"5202aa"}, model_GUID = "8c2db4"},
	{name="Shiny Nuzleaf", chip_GUIDs = {"152903"}, model_GUID = "2f31d0", spawn_effect="Physical Attack"},
	{name="Shiny Shiftry", chip_GUIDs = {"84b7ac"}, model_GUID = "c4f1a3"},
	{name="Shiny Taillow", chip_GUIDs = {"ad694d"}, model_GUID = "223de2"},
	{name="Shiny Swellow", chip_GUIDs = {"0811d9"}, model_GUID = "ff81c4", spawn_effect="Physical Attack"},
	{name="Shiny Wingull", chip_GUIDs = {"9d9d61"}, model_GUID = "e912ab"},
	{name="Shiny Pelipper", chip_GUIDs = {"1d3968"}, model_GUID = "12092f"},
	{name="Shiny Ralts", chip_GUIDs = {"5dc4ab"}, model_GUID = "09c38c"},
	{name="Shiny Kirlia", chip_GUIDs = {"a8ae05"}, model_GUID = "76402e", spawn_effect="Physical Attack"},
	{name="Shiny Gardevoir", chip_GUIDs = {"36b0ce"}, model_GUID = "740cd8"},
	{name="Shiny Mega Gardevoir", chip_GUIDs = {"1fa0a1"}, model_GUID = "1042fa", spawn_effect="Mega Evolve"},
	{name="Shiny Surskit", chip_GUIDs = {"1375a4"}, model_GUID = "18b120"},
	{name="Shiny Masquerain", chip_GUIDs = {"e0c539"}, model_GUID = "faa1d3"},
	{name="Shiny Shroomish", chip_GUIDs = {"6fd4d7"}, model_GUID = "b9bbcb"},
	{name="Shiny Breloom", chip_GUIDs = {"b1e748"}, model_GUID = "51d267"},
	{name="Shiny Slakoth", chip_GUIDs = {"62ee91"}, model_GUID = "565da6"},
	{name="Shiny Vigoroth", chip_GUIDs = {"10b3a1"}, model_GUID = "bd1aa9"},
	{name="Shiny Slaking", chip_GUIDs = {"89211d"}, model_GUID = "0f4fdc"},
	{name="Shiny Nincada", chip_GUIDs = {"f77da2"}, model_GUID = "429736"},
	{name="Shiny Ninjask", chip_GUIDs = {"176af9"}, model_GUID = "e4ef53"},
	{name="Shiny Shedinja", chip_GUIDs = {"8e4142"}, model_GUID = "fbb17c"},
	{name="Shiny Whismur", chip_GUIDs = {"aa82ec"}, model_GUID = "54600d"},
	{name="Shiny Loudred", chip_GUIDs = {"26a5c1"}, model_GUID = "29bdab"},
	{name="Shiny Exploud", chip_GUIDs = {"cc797d"}, model_GUID = "14453d"},
	{name="Shiny Makuhita", chip_GUIDs = {"ec04e4"}, model_GUID = "b36317"},
	{name="Shiny Hariyama", chip_GUIDs = {"d03b86"}, model_GUID = "859c4c"},
	{name="Shiny Azurill", chip_GUIDs = {"1a66e0"}, model_GUID = "3c11df"},
	{name="Shiny Nosepass", chip_GUIDs = {"421978"}, model_GUID = "62afed"},
	{name="Shiny Skitty", chip_GUIDs = {"6be049"}, model_GUID = "521e8c"},
	{name="Shiny Delcatty", chip_GUIDs = {"daa870"}, model_GUID = "2dc056"},
    {name="Shiny Sableye", chip_GUIDs = {"e08a2c"}, model_GUID = "1d2b7f", spawn_effect="Physical Attack"},
	{name="Shiny Mega Sableye", chip_GUIDs = {"eb229d"}, model_GUID = "7c0c9e", spawn_effect="Physical Attack"},
	{name="Shiny Mawile", chip_GUIDs = {"5378f4"}, model_GUID = "b40c0e"},
	{name="Shiny Mega Mawile", chip_GUIDs = {"7b1208"}, model_GUID = "a05761", spawn_effect="Mega Evolve"},
	{name="Shiny Aron", chip_GUIDs = {"18889e"}, model_GUID = "106594", spawn_effect="Physical Attack"},
	{name="Shiny Lairon", chip_GUIDs = {"3f9c1d"}, model_GUID = "4f0fca"},
	{name="Shiny Aggron", chip_GUIDs = {"38c600"}, model_GUID = "e0f530"},
	{name="Shiny Mega Aggron", chip_GUIDs = {"3dbd64"}, model_GUID = "ab4b96", spawn_effect="Mega Evolve"},
	{name="Shiny Meditite", chip_GUIDs = {"7e22ce"}, model_GUID = "15561a"},
	{name="Shiny Medicham", chip_GUIDs = {"421e08"}, model_GUID = "8720ef"},
	{name="Shiny Mega Medicham", chip_GUIDs = {"7c21e9"}, model_GUID = "f6ed1b", spawn_effect="Mega Evolve"},
	{name="Shiny Electrike", chip_GUIDs = {"5737c7"}, model_GUID = "da23c2"},
	{name="Shiny Manectric", chip_GUIDs = {"ed8dff"}, model_GUID = "85a06f"},
	{name="Shiny Mega Manectric", chip_GUIDs = {"ecec89"}, model_GUID = "63a511", spawn_effect="Mega Evolve"},
	{name="Shiny Plusle", chip_GUIDs = {"30b9d9"}, model_GUID = "1ff9eb"},
	{name="Shiny Minun", chip_GUIDs = {"77fe32"}, model_GUID = "e7c3ed"},
	{name="Shiny Volbeat", chip_GUIDs = {"c61040"}, model_GUID = "2892a4"},
	{name="Shiny Illumise", chip_GUIDs = {"436345"}, model_GUID = "beb733"},
	{name="Shiny Roselia", chip_GUIDs = {"edf0c1"}, model_GUID = "3ca801"},
	{name="Shiny Gulpin", chip_GUIDs = {"a176cf"}, model_GUID = "276177"},
	{name="Shiny Swalot", chip_GUIDs = {"57eaeb"}, model_GUID = "e16850"},
	{name="Shiny Carvanha", chip_GUIDs = {"a773c5"}, model_GUID = "44a443", spawn_effect="Physical Attack"},
    {name="Shiny Sharpedo", chip_GUIDs = {"411fc2"}, model_GUID = "8783cd", spawn_effect="Physical Attack"},
	{name="Shiny Mega Sharpedo", chip_GUIDs = {"e5fac5"}, model_GUID = "cfd038", spawn_effect="Mega Evolve"},
	{name="Shiny Wailmer", chip_GUIDs = {"a92535"}, model_GUID = "6fe9fb"},
	{name="Shiny Wailord", chip_GUIDs = {"82ac41"}, model_GUID = "b8683a", custom_scale=0.3},
	{name="Shiny Numel", chip_GUIDs = {"4ad731"}, model_GUID = "d4181b", spawn_effect="Physical Attack"},
	{name="Shiny Camerupt", chip_GUIDs = {"56a397"}, model_GUID = "25f6f0", spawn_effect="Physical Attack"},
	{name="Shiny Mega Camerupt", chip_GUIDs = {"c28fbf"}, model_GUID = "eea973", spawn_effect="Mega Evolve"},
	{name="Shiny Torkoal", chip_GUIDs = {"6e0368"}, model_GUID = "29eb43", spawn_effect="Physical Attack"},
	{name="Shiny Spoink", chip_GUIDs = {"6ef99e"}, model_GUID = "5b9992"},
	{name="Shiny Grumpig", chip_GUIDs = {"6cb09c"}, model_GUID = "fcd6c8"},
	{name="Shiny Spinda", chip_GUIDs = {"d063fb"}, model_GUID = "c6aa78"},
	{name="Shiny Trapinch", chip_GUIDs = {"cae951"}, model_GUID = "e43112"},
	{name="Shiny Vibrava", chip_GUIDs = {"82695c"}, model_GUID = "f2e2a8"},
	{name="Shiny Flygon", chip_GUIDs = {"278617"}, model_GUID = "0bb3b6"},
	{name="Shiny Cacnea", chip_GUIDs = {"edd7c5"}, model_GUID = "8c204d", spawn_effect="Physical Attack"},
	{name="Shiny Cacturne", chip_GUIDs = {"369198"}, model_GUID = "725808"},
	{name="Shiny Swablu", chip_GUIDs = {"bdb347"}, model_GUID = "3a1765"},
	{name="Shiny Altaria", chip_GUIDs = {"ddcd9a"}, model_GUID = "4f0b3b"},
	{name="Shiny Mega Altaria", chip_GUIDs = {"84d3ea"}, model_GUID = "35e49f", spawn_effect="Mega Evolve"},
	{name="Shiny Zangoose", chip_GUIDs = {"d05692"}, model_GUID = "165520"},
	{name="Shiny Seviper", chip_GUIDs = {"8a4739"}, model_GUID = "97c948", spawn_effect="Physical Attack"},
	{name="Shiny Lunatone", chip_GUIDs = {"449364"}, model_GUID = "827df9"},
	{name="Shiny Solrock", chip_GUIDs = {"cd998e"}, model_GUID = "567381"},
	{name="Shiny Barboach", chip_GUIDs = {"4c3783"}, model_GUID = "193ecc"},
	{name="Shiny Whiscash", chip_GUIDs = {"4d3911"}, model_GUID = "2bec01"},
	{name="Shiny Corphish", chip_GUIDs = {"ae25f0"}, model_GUID = "3f7ad3"},
	{name="Shiny Crawdaunt", chip_GUIDs = {"d5c5b8"}, model_GUID = "a6dc6b"},
	{name="Shiny Baltoy", chip_GUIDs = {"124a87"}, model_GUID = "367eeb", spawn_effect="Physical Attack"},
	{name="Shiny Claydol", chip_GUIDs = {"a632d6"}, model_GUID = "d0fade", spawn_effect="Physical Attack"},
	{name="Shiny Lileep", chip_GUIDs = {"82e10e"}, model_GUID = "e42954"},
	{name="Shiny Cradily", chip_GUIDs = {"e6a261"}, model_GUID = "23fcfe"},
	{name="Shiny Anorith", chip_GUIDs = {"54f946"}, model_GUID = "2249cf"},
	{name="Shiny Armaldo", chip_GUIDs = {"661e46"}, model_GUID = "eebfd1"},
	{name="Shiny Feebas", chip_GUIDs = {"269836"}, model_GUID = "f69f61"},
	{name="Shiny Milotic", chip_GUIDs = {"020af9"}, model_GUID = "465100", spawn_effect="Physical Attack"},
	{name="Shiny Castform", chip_GUIDs = {"738d22"}, model_GUID = nil, persistent_state=false, states={
    	{name="Castform Base", model_GUID = "67ddf2"},
    	{name="Rainy Castform", model_GUID = "b507f8"},
    	{name="Snowy Castform", model_GUID = "da4464"},
    	{name="Sunny Castform", model_GUID = "7cefe4"}
    }},
	{name="Shiny Kecleon", chip_GUIDs = {"86b425"}, model_GUID = "4af59a", spawn_effect="Physical Attack"},
	{name="Shiny Shuppet", chip_GUIDs = {"dd95ac"}, model_GUID = "46848e", spawn_effect="Physical Attack"},
	{name="Shiny Banette", chip_GUIDs = {"799e14"}, model_GUID = "77c122"},
	{name="Shiny Mega Banette", chip_GUIDs = {"d1342c"}, model_GUID = "eebe8c", spawn_effect="Mega Evolve"},
	{name="Shiny Duskull", chip_GUIDs = {"7d30b7"}, model_GUID = "1d4297"},
	{name="Shiny Dusclops", chip_GUIDs = {"6c3866"}, model_GUID = "69f383"},
	{name="Shiny Tropius", chip_GUIDs = {"c92ba7"}, model_GUID = "4532a1"},
	{name="Shiny Chimecho", chip_GUIDs = {"c66474"}, model_GUID = "556142"},
	{name="Shiny Absol", chip_GUIDs = {"bc58f3"}, model_GUID = "79765d"},
	{name="Shiny Mega Absol", chip_GUIDs = {"f84b4d"}, model_GUID = "7bd7e2", spawn_effect="Mega Evolve"},
	{name="Shiny Wynaut", chip_GUIDs = {"29cead"}, model_GUID = "c16e96"},
	{name="Shiny Snorunt", chip_GUIDs = {"403c45"}, model_GUID = "601fb7"},
	{name="Shiny Glalie", chip_GUIDs = {"dad34d"}, model_GUID = "1aa54e"},
	{name="Shiny Mega Glalie", chip_GUIDs = {"2b3179"}, model_GUID = "83fcf2", spawn_effect="Mega Evolve"},
	{name="Shiny Spheal", chip_GUIDs = {"fe6327"}, model_GUID = "5cd73e", spawn_effect="Physical Attack"},
	{name="Shiny Sealeo", chip_GUIDs = {"8328ec"}, model_GUID = "e1b8ad"},
	{name="Shiny Walrein", chip_GUIDs = {"3bdee6"}, model_GUID = "37118f"},
	{name="Shiny Clamperl", chip_GUIDs = {"b3fef3"}, model_GUID = "a8f43c"},
	{name="Shiny Huntail", chip_GUIDs = {"fa4d5d"}, model_GUID = "b139f1"},
	{name="Shiny Gorebyss", chip_GUIDs = {"7c1f83"}, model_GUID = "61b61a", spawn_effect="Physical Attack"},
	{name="Shiny Relicanth", chip_GUIDs = {"218c0a"}, model_GUID = "75c281"},
	{name="Shiny Luvdisc", chip_GUIDs = {"f64df3"}, model_GUID = "5393f8", spawn_effect="Physical Attack"},
	{name="Shiny Bagon", chip_GUIDs = {"a023e2"}, model_GUID = "05964b"},
	{name="Shiny Shelgon", chip_GUIDs = {"0dbfd0"}, model_GUID = "b7fc9f", spawn_effect="Physical Attack"},
	{name="Shiny Salamence", chip_GUIDs = {"6ed2c6"}, model_GUID = "a3cb6f"},
    {name="Shiny Mega Salamence", chip_GUIDs = {"a81d04"}, model_GUID = "9d934d", spawn_effect="Mega Evolve"},
	{name="Shiny Beldum", chip_GUIDs = {"124a87"}, model_GUID = "ad12cb"},
	{name="Shiny Metang", chip_GUIDs = {"8cd8ae"}, model_GUID = "d7ac8f"},
    {name="Shiny Metagross", chip_GUIDs = {"623a49"}, model_GUID = "a28092", spawn_effect="Status Attack"},
	{name="Shiny Mega Metagross", chip_GUIDs = {"2c1bef"}, model_GUID = "081dbe", spawn_effect="Mega Evolve"},
	{name="Shiny Regirock", chip_GUIDs = {"5ca410"}, model_GUID = "ac6107"},
	{name="Shiny Regice", chip_GUIDs = {"f076e1"}, model_GUID = "d7f436"},
	{name="Shiny Registeel", chip_GUIDs = {"a989db"}, model_GUID = "cbbbc9", spawn_effect="Status Attack"},
	{name="Shiny Latias", chip_GUIDs = {"d2d5fd"}, model_GUID = "387511"},
	{name="Shiny Mega Latias", chip_GUIDs = {"42c2f5"}, model_GUID = "bdcf3e", spawn_effect="Mega Evolve"},
	{name="Shiny Latios", chip_GUIDs = {"73683f"}, model_GUID = "be8ba0"},
	{name="Shiny Mega Latios", chip_GUIDs = {"192d18"}, model_GUID = "0cccdc", spawn_effect="Mega Evolve"},
    {name="Shiny Kyogre", chip_GUIDs = {"e4a304", "3c2e5d"}, model_GUID = "d90937"},
	{name="Shiny Primal Kyogre", chip_GUIDs = {"47960a"}, model_GUID = "50238a", spawn_effect="Primal Evolution", custom_scale=0.5},
	{name="Shiny Groudon", chip_GUIDs = {"5c2899"}, model_GUID = "efad60"},
	{name="Shiny Primal Groudon", chip_GUIDs = {"112d53"}, model_GUID = "a68978", spawn_effect="Primal Evolution", custom_scale=0.5},
	{name="Shiny Mega Rayquaza", chip_GUIDs = {"180492"}, model_GUID = "fd32a9", spawn_effect="Mega Evolve", custom_scale=0.7},
	{name="Shiny Rayquaza", chip_GUIDs = {"db9c04"}, model_GUID = "ec9902"},
	{name="Shiny Jirachi", chip_GUIDs ={"6455ad"}, model_GUID = "963775"},
	{name="Shiny Deoxys", chip_GUIDs = {"a48139"}, model_GUID = nil, persistent_state=false, states = {
    	{name="Shiny Base Deoxys", model_GUID = "380e3d", spawn_effect="Status Attack"},
    	{name="Shiny Speed Deoxys", model_GUID = "21c080", spawn_effect="Physical Attack"}
    }},
}

function read_write_guidlist(message)
    if message == "Init" then
        guid_list = {}
        guid_nbr = 0
    elseif message == "Init Names" then
        guid_list = {}
        names_list = {}
        guid_nbr = 0
        guid_list_with_names = true
    elseif message == "Delete" then
        guid_list[guid_nbr] = nil
        guid_nbr = guid_nbr - 1
    elseif message == "Print" then
        local notebook_text = ""
        for i=1,guid_nbr do
            if guid_list_with_names and names_list[i] then
                notebook_text = notebook_text .. names_list[i] .. ";" .. guid_list[i] .. "\n"
            else
                notebook_text = notebook_text .. guid_list[i] .. "\n"
            end
        end
        parameters = {
            title = "GUID List",
            body = notebook_text,
            color = "Grey"
        }
        Notes.addNotebookTab(parameters)
    end
end

function read_write_modelfill(message)
    if message == "Clear" then
        clear_container(battlemodels)
    elseif message == "Put" then
        put_models_into_container(battlemodels)
    end
end

function clear_container(container)
    local objList = container.getObjects()
    for _,objTbl in pairs(objList) do
        local p = {
          position = {x=0, y=0, z=0},
          rotation = {x=0,y=0,z=0},
          guid = objTbl.guid,
          smooth = false,
          callback_function = function(obj) destroyObject(obj) end
        }
        battlemodels.takeObject(p)
    end
end

function put_models_into_container(container)
    for _,pokemon_base in pairs(all_pokemon) do
        if pokemon_base.model_GUID then
            model = getObjectFromGUID(pokemon_base.model_GUID)
            if model then
                container.putObject(model)
            end
        end
        if pokemon_base.states then
            for _, pokemon_state in pairs(pokemon_base.states) do
                model = getObjectFromGUID(pokemon_state.model_GUID)
                if model then
                    container.putObject(model)
                end
            end
        end
    end
end

function onObjectSpawn(obj)
    --battlemodels.putObject(obj)
end

function check_correctness(message)
--    if message == "Assert" then
--        for key,pokemon in pairs(all_pokemon) do
--            if pokemon.model ~= getObjectFromGUID(pokemon.model_GUID) or pokemon.chip ~= getObjectFromGUID(pokemon.chip_GUID) then
--                print(pokemon.name .. " model=", pokemon.model, " chip=", pokemon.chip, " ### Actual model=", getObjectFromGUID(pokemon.model_GUID), " chip=", getObjectFromGUID(pokemon.chip_GUID))
--            end
--        end
--    elseif message == "Ensure" then
--        for key,pokemon in pairs(all_pokemon) do
--            pokemon.model = getObjectFromGUID(pokemon.model_GUID)
--            pokemon.chip = getObjectFromGUID(pokemon.chip_GUID)
--        end
--    else
        pokemon = get_pokemon_by_GUID(message)
        if pokemon then
            if pokemon.state then
                print(pokemon.state.name, " (", pokemon.base.name, ")")
            else
                print(pokemon.base.name)
            end
            print("Chip: ", pokemon.chip_GUID, " ", pokemon.chip)
            if pokemon.model then
                print("Model: ", pokemon.state.model_GUID, " ", pokemon.model.getGUID(), " ", pokemon.model)
            elseif pokemon.state then
                print("Model: ", pokemon.state.model_GUID, " ", pokemon.model)
            elseif pokemon.base.model_GUID then
                print("Model: ", pokemon.base.model_GUID, " ", pokemon.model)
            else
                print("Model: ", pokemon.model)
            end

            if pokemon.state then
                print(pokemon.picked_up, " ", pokemon.in_creation, " ", pokemon.base.in_creation, " ", pokemon.state.created_before)
            else
                print(pokemon.picked_up, " ", pokemon.in_creation, " ", pokemon.base.in_creation, " nil")
            end

            if pokemon.model then
                try_activate_effect(pokemon.model, "Physical Attack")
            end
        end
--    end
    obj = getObjectFromGUID(message)
    if obj then
        print(obj.getPosition())
    end
end

function toggle_spawn_scale(message)
    if message == "Toggle Scale" then
        scale_on_spawn = not scale_on_spawn
    end
end

function print_save(message)
    if message == "Print Save" then
        print(generate_save_string())
    end
end