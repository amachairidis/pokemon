local xLoc = 57
local yLoc = 1.5
local zLoc = -30
local balls = {"52a142","97fba7","1c139b","950dc9","4e3b10"} --make the starter ball aware of the other balls.
starterLocations = {{23.65,0.64,-59.60}, {-12.15, 0.71, -59.05}, {-45.68, 0.77, -59.61}, {-45.65, 0.78, 41.38}, {-11.02, 0.72, 41.4}, {23.61, 0.65, 41.40}}
local input
function onLoad()
end

function move()
  self.setPosition({xLoc, yLoc, zLoc})
end

function activate(input_parameters)
  input = input_parameters
  if(input.region == 6)then
    local gen6Cart = getObjectFromGUID("d7d21b")
    local params = {}
    params.position = {-36.96, 1.41, -19.44}
    params.rotation = {0,180,0}
    params.guid = "fbceab"
    gen6Cart.takeObject(params)
  end
  self.createButton({ --Apply settings button
      label="Deal", click_function="deal_starters",
      function_owner=self, tooltip="Click to deal starters to seated players and proceed with setup.",
      position={0,0,3}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
  self.createButton({ --Apply settings button
      label="Draw", click_function="change_buttons",
      function_owner=self, tooltip="Click if you want to draw your own starters from the bag and then proceed with setup.",
      position={0,0,5}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
end

function deal_starters()
  local players = getSeatedPlayers()
   for i=1,#players do
     local current = players[i]
     local param = {}
     local position = {}
     if current == "White" then
       position = starterLocations[1]
     elseif current == "Yellow" then
       position = starterLocations[2]
     elseif current == "Red" then
       position = starterLocations[3]
     elseif current == "Green" then
       position = starterLocations[4]
     elseif current == "Teal" then
       position = starterLocations[5]
     elseif current == "Blue" then
       position = starterLocations[6]
     else
       print(current)
     end
     param.position = position
     self.takeObject(param)
     change_buttons()
   end
end

function change_buttons()
   self.clearButtons()
   self.createButton({ --Apply settings button
       label="Merge", click_function="merge_starters",
       function_owner=self, tooltip="Click to merge remaining starters into the pink ball.",
       position={0,0,3}, rotation={0,0,0}, height=440, width=1400, font_size=200,
   })
   self.createButton({ --Apply settings button
       label="Delete", click_function="delete_starters",
       function_owner=self, tooltip="Click if you want to delete the remaining starters, making them uncatchable for the rest of the game.",
       position={0,0,5}, rotation={0,0,0}, height=440, width=1400, font_size=200,
   })
end

function merge_starters()
   pink = getObjectFromGUID(balls[1])
   for i=1, self.getQuantity() do
     pog = self.takeObject({})
     pink.putObject(pog)
   end
   pink.shuffle()
   delete_starters()
end

function delete_starters()
   self.destruct()
   for i=1, #balls do
     getObjectFromGUID(balls[i]).call("activate", input)
   end
end