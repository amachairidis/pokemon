
leaderContainers = {"4c99d1","a3d587","044b8e","2ac9ae","d6351d","eaebbc","376db3"}
numOpponentPokemon = 0
kantoRivals = {{"Gym Leader Brock","9033d0","333cd8","6eeb1d"},{"Gym Leader Misty","6a6db7","e68806","77b5be"},{"Gym Leader Lt. Surge","3057a0","65e9c9","77f936"},{"Gym Leader Erika","06deb5","415482","410e1b"},{"Gym Leader Janine","329bc2","faef47","dcbf39"},{"Gym Leader Sabrina","35498b","1354ba","067302"},{"Gym Leader Blaine","f82ae8","1a13a1","1a52af"},{"Gym Leader Giovanni","7e2226","3568f3","5da907"},
{"Elite Four Lorelei","61419a","8f51bd","3137cf"},{"Elite Four Bruno","29ee8d","416d49","f654c5"},{"Elite Four Agatha","e43da6","603e56","936b63"},{"Elite Four Lance","a09289","d23565","d55df8"},{"Rival Gary","6d6db3","d2f45c","07e2d4"}}
kantoEmblems = {"1949fd","3cd518","a6f90e","b71fe4","400ad4","8f5615","f93a57","718871","6eeb1d","61c21c","ae6f41","2d2ea1","cd5309"}

johtoRivals = {{"Gym Leader Falkner","7fbadb","5d6857","2970f8"},{"Gym Leader Bugsy","552869","6370df","09c612"},{"Gym Leader Whitney","6433c6","676498","e0dc54"},{"Gym Leader Morty","9603c9","b2718f","3a07da"},{"Gym Leader Chuck","98117a","8217ef","3c7421"},{"Gym Leader Jasmine","461832","3986a8","46facd"},{"Gym Leader Pryce","d0f276","ef3ca8","7d87d9"},{"Gym Leader Claire","2385cf","503dc5","fdfa8a"},
{"Elite Four Will","a11180","2e8caf","71d6ab"},{"Elite Four Koga","c595e5","b8d849","048b6f"},{"Elite Four Bruno","f4c39a","fdc1b8","cfd11d"},{"Elite Four Karen","4a14a4","d6c969","9acb48"},{"Rival Silver","d9b653","e19812","cb4e5b"}}
johtoEmblems = {"5abff8","511f00","66aa94","d400ff","70d295","965eaf","dc351a","97bdb7","23cfba","c9cc02","2f0f81","d7f9a8","cbf14f"}

hoennRivals = {{"Gym Leader Roxanne","537419","957b40","915301"},{"Gym Leader Brawly","c1254f","9af7f5","c3a361"},{"Gym Leader Wattson","3aa1a3","a8bf54","5da225"},{"Gym Leader Flannery","be0637","62e840","0d48f6"},{"Gym Leader Norman","de3163","f38368","03d9d6"},{"Gym Leader Winona","4577ea","34edd0","7df5d1"},{"Gym Leader Tate & Liza","e7a149","dde416","1365a1"},{"Gym Leader Wallace","2a306e","8a7abb","d2701b"},
{"Elite Four Sidney","e37fb7","260602","130b65"},{"Elite Four Phoebe","e7e8a7","9ded8b","116861"},{"Elite Four Glacia","532918","390d22","341a3e"},{"Elite Four Drake","fc1383","c0b795","32cdf5"},{"Rival Wally","2ff315","555030","1fcd19"}}
hoennEmblems = {"dfd966","ce5b15","e376e4","1dc651","df278d","24c59e","74c83a","2a4807","2d7b05","ee3c91","da81aa","2e557b","9acce4"}

sinnohRivals = {{"Gym Leader Roark","bf028a","a8b3e6","8124ab"},{"Gym Leader Gardenia","b78ca0","b010a9","1d4b7d"},{"Gym Leader Maylene","112fe3","67935a","101c58"},{"Gym Leader Crasher Wake","36ef88","936aed","f59a19"},{"Gym Leader Fantina","31ce5c","aa262d","a1e8c6"},{"Gym Leader Byron","91e9bd","f7eeb1","dc584b"},{"Gym Leader Candice","7cd775","ee4cbf","697626"},{"Gym Leader Volkner","983369","4ba29f","11124b"},
{"Elite Four Aaron","0962a1","de18e6","312908"},{"Elite Four Bertha","b0c580","0075df","9ad82d"},{"Elite Four Flint","29cb59","576d28","488bf5"},{"Elite Four Lucian","7afc29","129794","b3f897"},{"Rival Barry","7bbd0b","1938a7","3ff1cc"}}
sinnohEmblems = {"e045df","d7b7bd","fae7ec","20c298","82bea9","73e046","f92b73","e39de7","24725a","d3056e","46c1c9","41bafa","963fa2"}

unovaRivals = {{"Gym Leader Cilian/Chili/Cress","c81b28","d497ed","37c132"},{"Gym Leader Lenora","2c95bc","7515f8","08d4fd"},{"Gym Leader Burgh","3f8eca","de7f57","5b5419"},{"Gym Leader Elesa","30fe66","718ffe","d80c22"},{"Gym Leader Clay","0d1657","2ae05d","4e9cc3"},{"Gym Leader Skyla","9a4f83","56a0d6","317885"},{"Gym Leader Brycen","c97ef8","a19010","62038a"},{"Gym Leader Drayden","beb176","0dbda1","8c8189"},
{"Elite Four Shauntal","e202f9","16150e","9a22b9"},{"Elite Four Marshal","945fd1","81168e","6f0f4c"},{"Elite Four Grimsley","e78e58","88fa20","bdb59e"},{"Elite Four Caitlin","bc4101","9a050e","c999ba"},{"Rival Hugh","d72d69","ed5bf5","35171a"}}
unovaEmblems = {"4cbcd5","6f08d4","dfca65","fdef4f","7637fa","be1c3d","e71d3c","978640","97e0dd","8e9a18","ecf292","922b42","bfb78b"}

kalosRivals = {{"Gym Leader Viola","3026d5","11d283","9502e1"},{"Gym Leader Grant","3947b3","f9bbcd","599ab9"},{"Gym Leader Korrina","e23827","435515","e030ce"},{"Gym Leader Ramos","496dbe","fda64c","5635bb"},{"Gym Leader Clemont","e50392","e5d7e2","72b118"},{"Gym Leader Valerie","27034c","08af1d","a91a55"},{"Gym Leader Olympia","3e5864","3dfb47","5ae9eb"},{"Gym Leader Wulfric","56449a","e721e9","92e5e9"},
{"Elite Four Malva","6064b1","d1f86e","77955d"},{"Elite Four Siebold","311467","f8d02b","f4fb4d"},{"Elite Four Wilkstrom","31e7dd","cda97c","fb68f0"},{"Elite Four Dransa","0c9e0e","990c54","9d3cd0"},{"Champion Diantha","9afc14","53a468","135a5e"}}
kalosEmblems = {"471f99","bb52aa","e96585","7f8125","76adce","a43229","2a89d8","24264e","cb20e0","204fe8","1699b0","13e06c","4eac4c"}

alolaRivals = {{"Kahuna Hapu","adfcee","62e644","361759"},{"Kahuna Hala","7fb64c","ece784","b6db52"},{"Kahuna Olivia","bb293a","9aa6c8","03f302"},{"Kahuna Nanu","98b492","4ba8dd","028e22"},
{"Elite Four Olivia","8e3f40","b3b053","2a4b24"},
{"Elite Four Molayne","30b528","a00579","0ba674"},{"Elite Four Acerola","fdf8de","f05509","856068"},{"Elite Four Kahili","24dd91","acd6b4","69eff0"},{"Rival Hau","f53c25","b87a08","e3d1ca"}}
alolaEmblems = {"05d9d8","c3f38b","650c5e","2273ec","0e5f98","4ce311","abbc1d","1be552","e011e4"}

teamRocketRivalCard = "0a41ce"
teamRocket = {"Team Rocket","bbeaca","fbb4d9"}
isRocket = false

--fellowship stuff
fellowshipContainer = "490f1e"
fellowshipRivals = {{"Gym Leader Brock","9033d0","333cd8","6eeb1d"},{"Gym Leader Misty","6a6db7","e68806","77b5be"},{"Gym Leader Lt. Surge","3057a0","65e9c9","77f936"},{"Gym Leader Erika","06deb5","415482","410e1b"},{"Gym Leader Janine","329bc2","faef47","dcbf39"},{"Gym Leader Sabrina","35498b","1354ba","067302"},{"Gym Leader Blaine","f82ae8","1a13a1","1a52af"},{"Gym Leader Giovanni","7e2226","3568f3","5da907"},
{"Elite Four Lorelei","61419a","8f51bd","3137cf"},{"Elite Four Bruno","29ee8d","416d49","f654c5"},{"Elite Four Agatha","e43da6","603e56","936b63"},{"Elite Four Lance","a09289","d23565","d55df8"},{"Rival Gary","6d6db3","d2f45c","07e2d4"},
{"Gym Leader Falkner","7fbadb","5d6857","2970f8"},{"Gym Leader Bugsy","552869","6370df","09c612"},{"Gym Leader Whitney","6433c6","676498","e0dc54"},{"Gym Leader Morty","9603c9","b2718f","3a07da"},{"Gym Leader Chuck","98117a","8217ef","3c7421"},{"Gym Leader Jasmine","461832","3986a8","46facd"},{"Gym Leader Pryce","d0f276","ef3ca8","7d87d9"},{"Gym Leader Claire","2385cf","503dc5","fdfa8a"},
{"Elite Four Will","a11180","2e8caf","71d6ab"},{"Elite Four Koga","c595e5","b8d849","048b6f"},{"Elite Four Bruno","f4c39a","fdc1b8","cfd11d"},{"Elite Four Karen","4a14a4","d6c969","9acb48"},{"Rival Silver","d9b653","e19812","cb4e5b"}}
fellowshipEmblems = {"1949fd","3cd518","a6f90e","b71fe4","400ad4","8f5615","f93a57","718871","6eeb1d","61c21c","ae6f41","2d2ea1","cd5309","5abff8","511f00","66aa94","d400ff","70d295","965eaf","dc351a","97bdb7","23cfba","c9cc02","2f0f81","d7f9a8","cbf14f"}

rivalSets = {kantoRivals,johtoRivals,hoennRivals,sinnohRivals,unovaRivals,kalosRivals,alolaRivals,teamRocket}
emblemSets = {kantoEmblems,johtoEmblems,hoennEmblems,sinnohEmblems,unovaEmblems,kalosEmblems,alolaEmblems}
regionBall = nil
regionRivals = nil
regionEmblems = nil
emblemUsed = 0
emblem = nil
rivalCardLocation = {-58.06, 0.62, -15.37}
rivalPokemonLocations = {{-73.88, 1.79, -20.13},{-67.27, 1.79, -18.24},{-60.48, 1.79, -20.13}}
opponent = nil
opponentName = nil
opponentBall = nil
opponentCard = nil
opponentBallLocation = {-77.38, 1.47, 5.40}
inBattle = false
triple = false
boardGeneration = 0
dealtPokemon = {}
dealtRival = nil
badgeLocations = {White = {{22,0.54,-40},{25,0.54,-40},{28,0.54,-40},{31,0.54,-40},{22,0.54,-43},{25,0.54,-43},{28,0.54,-43},{31,0.54,-43}},
Yellow = {{-15, 0.61, -40},{-12, 0.61,-40},{-9, 0.61,-40},{-6, 0.61,-40},{-15, 0.61,-43},{-12, 0.61,-43},{-9, 0.61,-43},{-6, 0.61,-43}},
Red = {{-47, 0.67, -40},{-44, 0.67,-40},{-41, 0.67,-40},{-38, 0.67,-40},{-47, 0.67,-43},{-44, 0.67,-43},{-41, 0.67,-43},{-38, 0.67,-43}},
Green = {{-47, 0.68, 61},{-44, 0.68, 61},{-41, 0.68, 61},{-38, 0.68, 61},{-47, 0.68, 58},{-44, 0.68, 58},{-41, 0.68, 58},{-38, 0.68, 58}},
Teal = {{-15, 0.62, 61},{-12, 0.62, 61},{-9, 0.62, 61},{-6, 0.62, 61},{-15, 0.62, 58},{-12, 0.62, 58},{-9, 0.62, 58},{-6, 0.62, 58}},
Blue = {{22,0.55,61},{25,0.55,61},{28,0.55,61},{31,0.55,61},{22,0.55,58},{25,0.55,58},{28,0.55,58},{31,0.54,-43}}}

function onLoad()
end

function activate(input_parameters)
    input = input_parameters
    boardGeneration = input.region
    
    if fellowshipExpansions then
        regionBall = fellowshipContainer
        regionRivals = fellowshipRivals
        regionEmblems = fellowshipEmblems
    else
        regionBall = leaderContainers[boardGeneration]
        regionRivals = rivalSets[boardGeneration]
        regionEmblems = emblemSets[boardGeneration]
    end
    --move calls to setup those buttons to separate function (makeStartBattlebuttons)
    makeStartBattleButtons()
end

function makeStartBattleButtons()
    self.createButton({ --Apply settings button
        label="1v1", click_function="startOneOnOne",
        function_owner=self, tooltip="Begin a 1v1 battle with the selected Rival",
        position={-0.75,0,0.275}, rotation={0,180,0}, height=150, width=150, font_size=60,
    })
    self.createButton({ --Apply settings button
        label="3v3", click_function="startThreeOnThree",
        function_owner=self, tooltip="Begin a 3v3 battle with the selected Rival",
        position={-0.75,0,-0.275}, rotation={0,180,0}, height=150, width=150, font_size=60,
    })
end

function makeWinLoseButtons()
    self.clearButtons()
    self.createButton({ --Apply settings button
        label="Win", click_function="winBattle",
        function_owner=self, tooltip="Click if the player has defeated the leader or rival",
        position={-0.75,0,0.275}, rotation={0,180,0}, height=150, width=150, font_size=60, font_color={0,1,0}
    })
    self.createButton({ --Apply settings button
        label="Lose", click_function="loseBattle",
        function_owner=self, tooltip="Click if the leader or rival has defeated the player",
        position={-0.75,0,-0.275}, rotation={0,180,0}, height=150, width=150, font_size=60, font_color={1,0,0}
    })
end

function startOneOnOne() startBattle(false) end
function startThreeOnThree() startBattle(true) end

function startBattle(tripleBattle)
    triple = tripleBattle
    local stuffInZone = detectEmblem()
    if stuffInZone then
        local emblem = getEmblem()
        opponent = getOpponent(emblem)
        if opponent != nil then
            dealtPokemon = {}
            opponentName = opponent[1]
        	opponentBall = opponent[2]
            opponentPkmn = opponent[3]
            if not isRocket then
                opponentCard = opponent[4]

      		    local regionBallObject = getObjectFromGUID(regionBall)
      		    local ball = {}
      		    ball.position = opponentBallLocation
      		    ball.guid = opponentBall
      		    opponentBallObject = regionBallObject.takeObject(ball)
      		    local rival = {}
                rival.guid = opponentCard
    	        rival.position = rivalCardLocation
    		    rival.rotation = {0,180,0}
    		    dealtRival = opponentBallObject.takeObject(rival)
            else
                local rocketRivalCard = getObjectFromGUID(emblem)
                opponentBallObject = getObjectFromGUID(opponentBall)
                rocketRivalCard.setPosition(rivalCardLocation)
            end
            if triple then
                broadcastToAll("Starting 3v3 Battle Against " .. opponentName, {0,1,0})
                opponentBallObject.shuffle()
                numOpponentPokemon = opponentBallObject.getQuantity()
                local count = 3
                if numOpponentPokemon < 3 then
                    count = numOpponentPokemon
                end
                for i=1,count do
                    local pog = {}
                    pog.position = rivalPokemonLocations[i]
                    pog.rotation = {180,0,0}
                    table.insert(dealtPokemon, opponentBallObject.takeObject(pog))
                end
            else
                broadcastToAll("Starting 1v1 Battle Against " .. opponentName, {0,1,0})
                local pog = {}
                pog.position = rivalPokemonLocations[2]
                pog.rotation = {180,0,180}
                pog.guid = opponentPkmn
                table.insert(dealtPokemon, opponentBallObject.takeObject(pog))
            end
            inBattle = true
            makeWinLoseButtons()
      else
          broadcastToAll("Invalid battle token.", {1,0,0})
      end
    end
end

function detectEmblem()
    local emblems = getObjectFromGUID("4d1fa7").getObjects()
    local numInZone = 0
    for k,v in pairs(emblems) do
        numInZone = numInZone + 1
    end
    if numInZone == 0 then
        broadcastToAll("No Rival Selected", {1,0,0})
        return false
    elseif numInZone > 1 then
        broadcastToAll("Only One Badge or Rival Card on Note", {1,0,0})
        return false
    else
        return true
    end
end

--returns the guid of the card/badge on the note
function getEmblem()
    local emblems = getObjectFromGUID("4d1fa7").getObjects()
    emblem = nil
    for _,obj in pairs(emblems) do
        emblem = obj.getGUID()
    end
    return emblem
end

function getOpponent(emblemGUID)
	emblemUsed = 0
    if emblemGUID == teamRocketRivalCard then
        isRocket = true
        return teamRocket
    end
	for i=1,#regionEmblems do
		if emblemGUID == regionEmblems[i] then
			emblemUsed = i
			break
		end
	end

	return regionRivals[emblemUsed]
end

function winBattle(object, color) endBattle("win", object, color) end
function loseBattle(object, color) endBattle("lose", object, color) end

function endBattle(result, object, color)
	if result == "win" then
        if(string.match(opponentName, "Gym Leader") or string.match(opponentName, "Kahuna")) then
          broadcastToAll(color .. " player Victory!",{0,1,0})
        local badge = {}
        
        local tmp = emblemUsed
        if tmp > 13 then
            tmp = tmp - 13
        end

        local pos = badgeLocations[color][tmp]
        badge.position = pos
        getObjectFromGUID(emblem).clone(badge)
		--clone badge of rival if it's a gym leader and place that on the challenger's badge case based on color of who clicked winButton
        elseif isRocket then
            broadcastToAll(color .. " player Defeated Team Rocket! Reclaim Your Stolen Pokemon!",{0,1,0})
          else
            broadcastToAll(color .. " player Victory!",{0,1,0})
        end
	elseif result == "lose" then
		broadcastToAll(color .. " player Loss.",{1,0,0})
	end
	local ray = {}

	local regionBallObject = getObjectFromGUID(regionBall)
	local opponentBallObject = getObjectFromGUID(opponentBall)
    if triple then
        for i=1, #dealtPokemon do
          opponentBallObject.putObject(dealtPokemon[i])
        end
    else
        opponentBallObject.putObject(dealtPokemon[1])
    end

    if not isRocket then
	  opponentBallObject.putObject(dealtRival)
	  regionBallObject.putObject(opponentBallObject)
    end
    self.clearButtons()
    makeStartBattleButtons()
    in1v1 = false
    in3v3 = false
    isRocket = false
end