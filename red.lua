local xLoc = 67
local yLoc = 1.5
local zLoc = -40
local color = "red"
local input
local chipY = 2.61
local kantoLocations = {{-18.01,9.05},{-18.89,3.77},{-18.85,-1.5},{-18.95,-8.6},{-9.73,-18.14},{-5.34,-20.97},{3.11,-23.47}}
local jhotoLocations = {{6.17,21.56},{12.2,21.76},{19.97,20.66},{21.79,15.77},{18.21,5.4},{11.75,1.25},{14.08,-4.03},{24.82,-2.87},{23.72,-10.25},{28.43,-12.07},{31.88,-5.05},{35.94,0.45}}
local hoennLocations = {{18.49,16.69},{27.57,16.18},{32.02,6.19},{20.14,10.15},{23.41,-3.36},{28.03,-20.35},{21.73,-12.45},{18.17,-4.99},{6.96,-10.85},{3.39,-4.5},{-2.2,-4.58},{-7.6,-4.72}}
local sinnohLocations = {{-3.45,-7.96},{-1.93,-16.38},{5.98,-11.85},{15.11,-10.93},{20.59,-16},{18.95,-20.55},{24.47,-5.26},{26.05,-15.47},{34.33,-6.83},{33.38,-1.62}}
local unovaLocations = {{-19.64,22.23},{-14.12,20.41},{-10.96,11.96},{-6.75,17.73},{8.84,13.91},{-0.35,19.25},{3.62,22.91},{10.56,21.99},{18.58,11.46},{10.78,-2.07}}
local kalosLocations = {{9.08,1.98},{23.46,3.16},{16.43,-2.38},{21.45,-6.63},{19.2,-12.69},{25.03,-12.41},{30.42,-9.5},{34.81,-5.71},{32.34,0.85}}
local alolaLocations = {{19.11,7.51},{15.06,-0.26},{17.34,-8.69},{25.01,-7.99},{27.41,0.09},{33.52,4.16},{30.67,-3.57},{36.79,-11.71},{34.03,-16.45},{30.85,-20.58},{21.34,-23.38}}
local locations = {kantoLocations, jhotoLocations, hoennLocations, sinnohLocations, unovaLocations, kalosLocations, alolaLocations}

function onLoad()

end

function move()
  self.setPosition({xLoc, yLoc, zLoc})
end

function activate(input_params)
    input = input_params
  self.createButton({ --Apply settings button
      label="Deal", click_function="deal_chips",
      function_owner=self, tooltip="Click to deal " .. color .. " pokemon chips to the unoccupied " .. color .." spaces.",
      position={0,0,3}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
  self.createButton({ --Apply settings button
      label="Shuffle", click_function="shuffle_chips",
      function_owner=self, tooltip="Click to shuffle contents of the " .. color .. " pokeball.",
      position={0,0,5}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
  self.createButton({ --Apply settings button
      label="Collect", click_function="collectchips",
      function_owner=self, tooltip="Click to return the " .. color .. " chips to the " .. color .. " pokeball.",
      position={0,0,7}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
end

function deal_chips()
  local currentLocations = locations[input.region]
  for i=1, #currentLocations do
   local param = {}
   param.origin = {currentLocations[i][1], chipY, currentLocations[i][2]}
   param.direction = {0,-1,0}
   param.type = 1
   param.max_distance = 1
   param.debug = false
   hits = Physics.cast(param)
   if(#hits == 0) then
     if self.getQuantity() > 0 then
       local param2 = {}
       param2.position = param.origin
       param2.rotation = {180,0,0}
       self.takeObject(param2)
     end
   end
  end
end

function shuffle_chips()
   self.shuffle()
 end

 function collectchips()
   local currentLocations = locations[input.region]
   for i=1, #currentLocations do
     local param = {}
     param.origin = {currentLocations[i][1], chipY, currentLocations[i][2]}
     param.direction = {0,-1,0}
     param.type = 1
     param.max_distance = 1
     param.debug = false
     hits = Physics.cast(param)
     if #hits ~= 0 then
        if hits[2] ~= nil then
            self.putObject(getObjectFromGUID(hits[2].hit_object.guid))
        else
            self.putObject(getObjectFromGUID(hits[1].hit_object.guid))
        end
    end
   end
 end