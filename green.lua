local xLoc = 77
local yLoc = 1.5
local zLoc = -30
local color = "green"
local input
local chipY = 2.61
local kantoLocations = {{-9.69,14.4},{-5.71,23.81},{-0.58,23.24},{9.64,23.36},{18.53,22.23},{21.96,14.29},{31.62,16.15},{16.12,11.81},{-0.76,5.74},{-0.3,13.45}}
local jhotoLocations = {{-21.16,8.02},{-26.02,8.02},{-35.89,8.42},{-34.25,13.91},{-29.6,16.23},{-22.04,17.16},{-17.03,12.71},{-12.22,14.44},{-1.22,14.95},{11.83,14.53}}
local hoennLocations = {{-24.27,-17.48},{-18.63,-22.64},{-13.18,-20.2},{-12.24,-11.71},{-11.92,-0.71},{-8.03,9.1},{-4.43,2.25},{3.08,8.91},{9.02,2.58},{12.72,9}}
local sinnohLocations = {{-15.27,8.17},{-14.52,-3.55},{-21.02,-0.66},{-19.8,4.22},{-19.29,14.38},{-9.52,9.96},{-13.13,17.14},{-10.17,22.38},{-5.46,15.91},{-4.13,9.18}}
local unovaLocations = {{4.16,-18.46},{-4.49,-23.28},{-5.82,-16.11},{-11.13,-22.61},{-20.4,-18.13},{-24.37,-14.56},{-29.6,-9.37},{-28.3,-17.95},{-37.54,-17.67},{-37.73,-9.48},{-30.92,-2.29}}
local kalosLocations = {{-2.82,2.23},{-4.55,-2.37},{-6.5,-6.52},{-14.2,-18.71},{-11.46,-7.45},{-15.97,-5.95},{-7.32,3.29},{-15.86,9.71},{-27.73,-2.04},{-35.53,3.16},{-4.99,-13.77}}
local alolaLocations = {{-22.67,11.55},{-10.56,7.83},{-4.26,11.3},{-2.03,17.59},{-16.81,16.22},{-23.48,18.2},{-23.04,23.25},{-11.03,20.1},{15.18,-13.57}}
local locations = {kantoLocations, jhotoLocations, hoennLocations, sinnohLocations, unovaLocations, kalosLocations, alolaLocations}

function onLoad()

end

function move()
  self.setPosition({xLoc, yLoc, zLoc})
end


function activate(input_params)
    input = input_params
  self.createButton({ --Apply settings button
      label="Deal", click_function="deal_chips",
      function_owner=self, tooltip="Click to deal " .. color .. " pokemon chips to the unoccupied " .. color .." spaces.",
      position={0,0,3}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
  self.createButton({ --Apply settings button
      label="Shuffle", click_function="shuffle_chips",
      function_owner=self, tooltip="Click to shuffle contents of the " .. color .. " pokeball.",
      position={0,0,5}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
  self.createButton({ --Apply settings button
      label="Collect", click_function="collectchips",
      function_owner=self, tooltip="Click to return the " .. color .. " chips to the " .. color .. " pokeball.",
      position={0,0,7}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
end

function deal_chips()
  local currentLocations = locations[input.region]
  for i=1, #currentLocations do
   local param = {}
   param.origin = {currentLocations[i][1], chipY, currentLocations[i][2]}
   param.direction = {0,-1,0}
   param.type = 1
   param.max_distance = 1
   param.debug = false
   hits = Physics.cast(param)
   if(#hits == 0) then
     if self.getQuantity() > 0 then
       local param2 = {}
       param2.position = param.origin
       param2.rotation = {180,0,0}
       self.takeObject(param2)
     end
   end
  end
end

function shuffle_chips()
   self.shuffle()
 end

 function collectchips()
   local currentLocations = locations[input.region]
   for i=1, #currentLocations do
     local param = {}
     param.origin = {currentLocations[i][1], chipY, currentLocations[i][2]}
     param.direction = {0,-1,0}
     param.type = 1
     param.max_distance = 1
     param.debug = false
     hits = Physics.cast(param)
     if #hits ~= 0 then
        if hits[2] ~= nil then
            self.putObject(getObjectFromGUID(hits[2].hit_object.guid))
        else
            self.putObject(getObjectFromGUID(hits[1].hit_object.guid))
        end
    end
   end
 end