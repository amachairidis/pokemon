gameCartridges = {"ac351d","d92a3c","02d796","42fa54","65c30a", "d7d21b", "d62804", "d62804"}
gen1pokeballs = {"27df7c","7ccffc","ce6c51","ee52b9","efe7aa","c2142a"}
gen2pokeballs = {"82a6b4","df04e7","0d789d","5f2652","a949b1","3574d7"}
gen3pokeballs = {"c99fc2","befa2b","660eca","f38e22","7f1aec","822f88"}
gen4pokeballs = {"5aae94","06af82","8bfc08","3b036a","69b215","2a0745"}
gen5pokeballs = {"a9f4d9","71bba4","b38e44","fe821d","5834f9","cd59b2"}
gen6pokeballs = {"658296","d64fe4","6eca2e","bb221a","2566c0","504e73"}
gen7pokeballs = {"f2dbc0","3f181f","14e6a8","f3be56","03335a","a1cca1"}
alolapokeballs = {"55b8c9","d6333e","e97709","7798dd","1ea5f6","43f690"}
pokeballs = {gen1pokeballs,gen2pokeballs,gen3pokeballs,gen4pokeballs,gen5pokeballs, gen6pokeballs, gen7pokeballs, alolapokeballs}
usedBalls = {"118e0e","52a142","97fba7","1c139b","950dc9","4e3b10"}
usedBallsCount = {0,0,0,0,0,0}

kantoBadgeSpots = {{-19.65, 1.46, 17.02},{8.14, 1.46, 19.84},{8.75, 1.46, -0.49},{-5.81, 1.46, 9.45},{2.12, 1.46, -15.71},{8.32, 1.46, 9.68},{-13.28, 1.46, -16.78},{-22.42, 1.46, -0.01}}
kantoBadgeGUIDs = {"1949fd","3cd518","a6f90e","b71fe4","400ad4","8f5615","f93a57","718871"}
johtoBadgeSpots = {{3.63, 1.46, 5.47},{-1.15, 1.46, -14.99},{-6.35, 1.46, -3.64},{-4.30, 1.46, 11.34},{-31.70, 1.46, 1.52},{-17.25, 1.46, 3.41},{7.58, 1.46, 11.69},{21.95, 1.46, 12.50}}
johtoBadgeGUIDs = {"5abff8","511f00","66aa94","d400ff","70d295","965eaf","dc351a","97bdb7"}
hoennBadgeSpots = {{-31.70, 1.46, 4.06},{-31.24, 1.46, -17.63},{-12.06, 1.46, 7.97},{-25.69, 1.46, 16.72},{-28.75, 1.46, -4.46},{-0.39, 1.46, 21.87},{35.92, 1.46, 14.75},{18.15, 1.46, 5.27}}
hoennBadgeGUIDs = {"dfd966","ce5b15","e376e4","1dc651","df278d","24c59e","74c83a","2a4807"}
sinnohBadgeSpots = {{-7.48, 1.46, -9.01},{-6.25, 1.46, 6.56},{24.57, 1.46, 1.15},{13.83, 1.46, -16.95},{-0.92, 1.46, -4.01},{-28.66, 1.46, -8.13},{1.21, 1.46, 24.61},{35.61, 1.46, -10.21}}
sinnohBadgeGUIDs = {"e045df","d7b7bd","fae7ec","20c298","82bea9","73e046","f92b73","e39de7"}
unovaBadgeSpots = {{35.70, 1.46, -2.68},{22.69, 1.46, -7.49},{4.33, 1.46, -11.83},{2.30, 1.46, 0.26},{-24.74, 1.46, -0.70},{-37.57, 1.46, 8.76},{-25.49, 1.46, 17.78},{2.01, 1.46, 15.88}}
unovaBadgeGUIDs = {"4cbcd5","6f08d4","dfca65","fdef4f","7637fa","be1c3d","e71d3c","978640"}
kalosBadgeSpots = {{6.77, 1.46, -0.88},{-18.22, 1.46, -0.88},{-22.74, 1.46, 9.64},{-12.24, 1.46, 12.12},{-0.03, 1.46, 9.07},{3.24, 1.46, 19.85},{26.46, 1.46, 7.16},{24.80, 1.46, -1.28}}
kalosBadgeGUIDs = {"471f99","bb52aa","e96585","7f8125","76adce","a43229","2a89d8","24264e"}
alolaBadgeSpots = {{-31.63, 1.46, 15.71},{-7.33, 1.46, 21.77},{29.74, 1.46, 10.96},{16.16, 1.46, -16.54}}
alolaBadgeGUIDs = {"05d9d8","c3f38b","650c5e","2273ec"}
badges = {{kantoBadgeSpots,kantoBadgeGUIDs},{johtoBadgeSpots,johtoBadgeGUIDs},{hoennBadgeSpots,hoennBadgeGUIDs},{sinnohBadgeSpots,sinnohBadgeGUIDs},{unovaBadgeSpots,unovaBadgeGUIDs},{kalosBadgeSpots,kalosBadgeGUIDs},{alolaBadgeSpots,alolaBadgeGUIDs}}

--Usage of generations: gens 1, 2, 3, 4, 5, 6, 7, and Alolan Variants (since they aren't technically in national dex)
generationsUsed = {false, false, false, false, false, false, false, false}
--expansion ordering: Megas, Team Rocket, Types, 3v3 battles, Villainous Teams, Random Encounters, Who's That Pokemon, Extra Cards, Gym Leaders
expansionsUsed = {false, false, false, false, false, false, false, false, false}
--expansion cartridge contents: cartridge first, then info, then cards
expansionContents = {{"471588","3c3827","713955","f43de1"},{"190127","623d4a","667145","2cb7eb"},{"2a1e08","21a5e1"},{"ae448c","38acc3"},{"f6c48f","14b3f5","96ea16"},{"3de425","aba7e2","79785d"},{"6e8e41","aedb61","5dd390"},
{"8e18a9","0b1ae6","e335e3"},{"cdf5e4","4f0242"}}
--expansion cartridge contents to be placed on the table
expansionPlacedContents = {{"3c3827","01189a"},{"623d4a","0a41ce","3ffa57","da969e","bbeaca"},{"21a5e1"},{"38acc3"},{"14b3f5","d43d0a"
,"d78505","b3dd6d","3158b2","4f5ec3","bb8389","9b8d0e","9eae7a"},{"aba7e2","3d8cb5"},{"aedb61","20692e"},{"94c679","2332ae"},{"4f0242"}}
--locations to place the expansion items aside from the info card
expansionLocations = {{{-48.75, 1.06, 25.82},{-48.50, 2.16, 26.19}},{{48.75, 1.06, 4.00},{48.12, 2.33, 12.00},{50.82, 1.33, 10.96},{47.36, 2.16, 1.08},{49.67, 2.16, 1.08}},{{-60.06, 0.56, 8.64}},{{-48.75, 1.07, -30.24}},
{{48.75, 1.06, -11.00},{47.36, 2.21, -9.72},{47.36, 2.16, -7.56},{49.67, 2.16, -7.56},{49.67, 2.16, -9.72},{47.36, 2.16, -11.88},{49.67, 2.16, -11.88},{47.36, 2.16, -14.04},{49.67, 2.16, -14.04}},{{-48.75, 1.07, 13.00},{-48.75, 1.97, 10.81}},{{-48.75, 1.07, 0.06},{-48.73, 1.56, -4.30}},
{{46.52, 0.96, -3.42},{49.67, 1.13, -3.24}},{{-69.30, 0.56, 8.64}}}
--GUIDs of event cards in the expansionContents table
expansionEvents = {expansionContents[1][3],expansionContents[2][3],"none","none",expansionContents[5][3],expansionContents[6][3],expansionContents[7][3],expansionContents[8][2],"none"}
--GUIDs of item cards in the expansionContents table
expansionItems = {expansionContents[1][4],expansionContents[2][4],"none","none","none","none","none",expansionContents[8][3],"none"}

boardGeneration = 0
boardObject = nil
gameRegions = {"Kanto", "Johto", "Hoenn", "Sinnoh", "Unova", "Kalos", "Alola"}
toggleRegionSelect = false
eventCards = "6e0a58"
eventCardDeckLocations = {{4.68, 1.44, -7.46},{5.21, 1.44, -18.58},{8.51, 1.43, -2.48},{-5.49, 1.43, -21.93},{-14.71, 1.43, -12.09},{-8.79, 1.43, -20.42},{-10.35, 1.43, -7.75}}
itemCards = "494294"
itemCardDeckLocations = {{9.70, 1.44, -7.46},{10.10, 1.44, -18.58},{13.37, 1.43, -2.48},{-0.60, 1.43, -21.93},{-10.05, 1.43, -12.09},{-4.09, 1.43, -20.42},{-5.67, 1.43, -7.75}}

rivalCardLocations = {{-6.04, 2.44, -4.74},{33.00, 2.44, 10.72},{35.14, 2.43, -4.13},{29.76, 2.43, 17.93},{33.71, 2.43, 18.13},{22.80, 2.43, 16.88},{3.39, 2.43, -11.60}}
rivalCardGUIDs = {"bc6a01","8563cc","f58503","889010","add4f1","106774","b22ef1"}

-- Fellowship variables --
fellowshipExpansions = false
shinyExpansion = false
shinyGensAvailable = 3
shinyAmount = {4, 3, 2, 2, 1} -- pink, green, blue, red, legend
fellowshipBag = '2aefdc'
extraContainers = {"96f892", "6fb02f", "c87d89", "5d772b", "7d0172"} -- pink, green, blue, red, legend
fellowShipItems = '49de29'
fellowShipEvents = '4d278c'
shinyBag = '7e3e0c'
shinyEvents = '078433'
shinyItems = '27937d'
shinyPokeballs = {{"9b132c","fcb837","4686e8","b55e41","86ca92"}, {"b5d030", "5e2b48", "701392", "0fee27", "513543"}, {"6d8884", "65a08f", "37e8ab", "8545ce", "711840"}}
shinyFinalPokeballs = {"aaf823", "6ea1b9", "02f225", "43cb78", "1adea1"}
shinyMegas = '01189a'
leftButtons_GUID = 'bf100f'
rightButtons_GUID = '62b976'
eventRevealPosition = {0.0, 10.0, -20.0}
discardEventLocations = {{34.52, 5, 6.26}, {-39.30, 5, 21.56}, {10.45, 5, -20.33}, {5.79, 5, 20.59}, {37.68, 5, 6.89}, {-24.81, 5, -16.13}, {28.34, 5, 15.96}}
tableMap_GUIDs = {"b431b5", "7ab4f9", "bd2c93", "86ce1f", "7d47ec", "f25fce", "039273"}
eventZones = {"11d8d0", "a0ceb0", "884044", "e77a18", "5fc018", "9d5274", "6ab751"}
eventDiscardZones = {"61cd6d", "341386", "795461", "e9dff4", "f18eaf", "dbb552", "c8ea30"}
itemZones = {"a8b5bc", "b6addb", "8c64aa", "c9a46f", "cdbded", "71e852", "517419"}
itemDiscardZones = {"33af7e", "26bdcd", "e19a36", "60d976", "b5d7c4", "24e0eb", "105d08"}
-- Fellowship variables --


function onLoad()
    --print("loaded setup button")
    self.setPosition({56, 1.5, -26.5})
    local tooltip = "Start Setup"
    self.createButton({
        click_function="click_openControlPanel",
        function_owner=self,
        rotation={0,0,0},
        position={0,0,0},
        height=1000,
        width=1000,
        color={0,0,0,0},
        tooltip=tooltip
        })
end

function click_openControlPanel()
    self.createButton({ --Label for Generation half of setup panel
        label="Generations Setup", click_function="splash",
        position={5,0,2.35}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=300, font_color={1,0.85,0}
    })
    self.createButton({ --Label for Expansion half of setup panel
        label="Expansions Setup", click_function="splash",
        position={20,0,2.35}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=300, font_color={0,0.25,1}
    })
    self.createButton({ --Apply settings button
        label="Apply & Setup", click_function="click_apply",
        function_owner=self, tooltip="Apply Settings and Run Setup",
        position={19,0,21}, scale={2.5,2.5,2.5}, height=440, width=1400, font_size=200,
    })

    --GENERATIONS
    local labelX = 3.5
    local genXLoc = 5
    local genZLoc = 3.8
    local genZIncrement = 1.5

    self.createButton({ --Use Generation 1 label
        label="Gen 1:", click_function="splash",alignment=2,
        position={labelX,0,genZLoc}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Generation 1 checkbox
        label="", click_function="gen1",
        function_owner=self, tooltip="Check to use Generation 1 pokemon",
        position={genXLoc+1,0,genZLoc}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Generation 2 label
        label="Gen 2:", click_function="splash",alignment=2,
        position={labelX,0,genZLoc+genZIncrement}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Generation 2 checkbox
        label="", click_function="gen2",
        function_owner=self, tooltip="Check to use Generation 2 pokemon",
        position={genXLoc+1,0,genZLoc+genZIncrement}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Generation 3 label
        label="Gen 3:", click_function="splash",alignment=2,
        position={labelX,0,genZLoc+genZIncrement*2}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Generation 3 checkbox
        label="", click_function="gen3",
        function_owner=self, tooltip="Check to use Generation 3 pokemon",
        position={genXLoc+1,0,genZLoc+genZIncrement*2}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Generation 4 label
        label="Gen 4:", click_function="splash",alignment=2,
        position={labelX,0,genZLoc+genZIncrement*3}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Generation 4 checkbox
        label="", click_function="gen4",
        function_owner=self, tooltip="Check to use Generation 4 pokemon",
        position={genXLoc+1,0,genZLoc+genZIncrement*3}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Generation 5 label
        label="Gen 5:", click_function="splash",alignment=2,
        position={labelX,0,genZLoc+genZIncrement*4}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Generation 5 checkbox
        label="", click_function="gen5",
        function_owner=self, tooltip="Check to use Generation 5 pokemon",
        position={genXLoc+1,0,genZLoc+genZIncrement*4}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Generation 6 label
        label="Gen 6:", click_function="splash",alignment=2,
        position={labelX,0,genZLoc+genZIncrement*5}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Generation 6 checkbox
        label="", click_function="gen6",
        function_owner=self, tooltip="Check to use Generation 6 pokemon",
        position={genXLoc+1,0,genZLoc+genZIncrement*5}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Generation 7 label
        label="Gen 7:", click_function="splash",alignment=2,
        position={labelX,0,genZLoc+genZIncrement*6}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Generation 7 checkbox
        label="", click_function="gen7",
        function_owner=self, tooltip="Check to use Generation 7 pokemon",
        position={genXLoc+1,0,genZLoc+genZIncrement*6}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Alolan label
        label="Alolan:", click_function="splash",alignment=2,
        position={labelX-0.25,0,genZLoc+genZIncrement*7}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Alolan checkbox
        label="", click_function="alola",
        function_owner=self, tooltip="Check to use Alolan Variants",
        position={genXLoc+1,0,genZLoc+genZIncrement*7}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Board Generation label
        label="Board Region:", click_function="splash", alignment=2,
        position={labelX-1.3,0,genZLoc+genZIncrement*8}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Board Generation input
        label="--Select--", click_function="generateRegionOptions", function_owner=self,
        alignment=3, position={genXLoc+2.5,0,genZLoc+genZIncrement*8+0.15}, scale={2.5,2.5,2.5}, height=250, width=750, font_size=150,
        tooltip="Click to select generation for game board.",
    })

    --EXPANSIONS
    local expXLoc = 20
    local expZLoc = 3.8
    local expZIncrement= 1.5

    self.createButton({ --Use Mega Evolutions Expansion label
        label="Mega Evolutions:", click_function="splash",
        position={expXLoc-0.55,0,expZLoc}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Mega Evolutions Expansion checkbox
        label="", click_function="exp1",
        function_owner=self, tooltip="Check to use Mega Evolutions Expansion",
        position={expXLoc+4.2,0,expZLoc}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Team Rocket Expansion label
        label="Team Rocket:", click_function="splash",
        position={expXLoc+0.25,0,expZLoc+expZIncrement}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Team Rocket Expansion checkbox
        label="", click_function="exp2",
        function_owner=self, tooltip="Check to use Team Rocket Expansion",
        position={expXLoc+4.2,0,expZLoc+expZIncrement}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Pokemon Types Expansion label
        label="Pokémon Types:", click_function="splash",
        position={expXLoc-0.4,0,expZLoc+expZIncrement*2}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Pokemon Types Expansion checkbox
        label="", click_function="exp3",
        function_owner=self, tooltip="Check to use Pokémon Types Expansion",
        position={expXLoc+4.2,0,expZLoc+expZIncrement*2}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use 3v3 Enhanced Battles Expansion label
        label="3v3 Enhanced Battles:", click_function="splash",
        position={expXLoc-1.65,0,expZLoc+expZIncrement*3}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use 3v3 Enhanced Battles Expansion checkbox
        label="", click_function="exp4",
        function_owner=self, tooltip="Check to use 3v3 Enhanced Battles Expansion",
        position={expXLoc+4.2,0,expZLoc+expZIncrement*3}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Villainous Team Expansion label
        label="Villainous Team:", click_function="splash",
        position={expXLoc-0.45,0,expZLoc+expZIncrement*4}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Villainous Team Expansion checkbox
        label="", click_function="exp5",
        function_owner=self, tooltip="Check to use Villainous Team Expansion",
        position={expXLoc+4.2,0,expZLoc+expZIncrement*4}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Random Encounters Expansion label
        label="Random Encounters:", click_function="splash",
        position={expXLoc-1.3,0,expZLoc+expZIncrement*5}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Random Encounters Expansion checkbox
        label="", click_function="exp6",
        function_owner=self, tooltip="Check to use Random Encounters Expansion",
        position={expXLoc+4.2,0,expZLoc+expZIncrement*5}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Who's that Pokémon Expansion label
        label="Who's that Pokémon:", click_function="splash",
        position={expXLoc-1.45,0,expZLoc+expZIncrement*6}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Who's that Pokémon Expansion checkbox
        label="", click_function="exp7",
        function_owner=self, tooltip="Check to use Who's that Pokémon Expansion",
        position={expXLoc+4.2,0,expZLoc+expZIncrement*6}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Extra Cards Expansion label
        label="Extra Cards:", click_function="splash",
        position={expXLoc+0.55,0,expZLoc+expZIncrement*7}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Extra Cards Expansion checkbox
        label="", click_function="exp8",
        function_owner=self, tooltip="Check to use Extra Cards Expansion",
        position={expXLoc+4.2,0,expZLoc+expZIncrement*7}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Gym Leader Expansion label
        label="Gym Leader:", click_function="splash",
        position={expXLoc+0.5,0,expZLoc+expZIncrement*8}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({ --Use Gym Leader Expansion checkbox
        label="", click_function="exp9",
        function_owner=self, tooltip="Check to use Gym Leader Expansion",
        position={expXLoc+4.2,0,expZLoc+expZIncrement*8}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --Use Shinies
        label="Shinies:", click_function="splash",
        position={expXLoc+0.90,0,expZLoc+expZIncrement*9}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({
        label="", click_function="shinyButton",
        function_owner=self, tooltip="Check to use shinies",
        position={expXLoc+4.2,0,expZLoc+expZIncrement*9}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
    self.createButton({ --The Fellowship Version
        label="The Fellowship:", click_function="splash",
        position={expXLoc-0.15,0,expZLoc+expZIncrement*10}, scale={2.5,2.5,2.5}, height=0, width=0, font_size=200, font_color={1,1,1}
    })
    self.createButton({
        label="", click_function="fellowshipButton",
        function_owner=self, tooltip="Check to use the Fellowship Expansion",
        position={expXLoc+4.2,0,expZLoc+expZIncrement*10}, scale={2.5,2.5,2.5}, height=224, width=224, font_size=200, color={1,0,0}
    })
end

function runSetup()
  local xStart = 56.5 --xlocation for the first column of pokeballs
  local yLoc = 1.75 -- y location for pokeballs
  local zStart = -30 -- zlocation for the first row of pokeballs
  local countcarts = #gameCartridges
  local withLegendaries = 6 --yes

  if fellowshipExpansions then -- skip normal legendaries and use ours
    withLegendaries = 5 --no
  end

  for i=1,countcarts do --for each gameCartidge
    if generationsUsed[i] == true then --if the generation was selected
        local obj = getObjectFromGUID(gameCartridges[i])
        for j=1,withLegendaries do --for each of the 6 pokeballs for each generation
            local param = {} --params for each pokeball
            param.guid = pokeballs[i][j] --guid for the pokeball
            param.position = {xStart + (3 * j), yLoc, zStart} --set position of the pokeball temporarily for merging.
            local tempBall = obj.takeObject(param) --pull out the ball
            for k=1,tempBall.getQuantity() do --for each object in the table
                local pog = tempBall.takeObject({})--pull the first pog from the ball
                getObjectFromGUID(usedBalls[j]).putObject(pog)--add it to the gen 1 ball
            end
            tempBall.destruct() -- destroy empty pokeballs
        end
        xStart = xStart + 4 --update xLocation for new row
    end
  end

  fellowshipSetup()
  includeExpansions()
  moveDecks()
  placeBadges()
  setupCardZones()

  for l=1,6 do
    getObjectFromGUID(usedBalls[l]).shuffle() --shuffle the pokemon
  end
  
  if fellowshipExpansions then
    if boardGeneration < 3 then
        placeRivals(1)
        placeRivals(2)
    else
        placeRivals(boardGeneration)
    end
  else
    placeRivals(boardGeneration)
  end

end

--Bring out the contents of the selected expansions
function includeExpansions()
    mixInCards()
    for i=1,#expansionsUsed do
        local cartridge = getObjectFromGUID(expansionContents[i][1])
        local params = {}
        if expansionsUsed[i] == true then
            for j=1,#expansionPlacedContents[i] do
                params.guid = expansionPlacedContents[i][j]
                params.position = expansionLocations[i][j]
                if i == 7  and j > 1 then
                    params.rotation = {0,90,180}
                else
                    params.rotation = {0,180,0}
                end
                local part = cartridge.takeObject(params)
                if j == 1 then part.setLock(true) end
            end
        end
    end
end

function mixInCards()
    local eventTempSpot = {46.41, 1.03, 3.97}
    local itemTempSpot = {50.41, 1.03, 3.97}
    local eventDeck = getObjectFromGUID(eventCards)
    local itemDeck = getObjectFromGUID(itemCards)

    for i=1,#expansionsUsed - 1 do
        if expansionsUsed[i] == true then
            local params = {}
            if expansionEvents[i] != "none" then
                params.guid = expansionEvents[i]
                params.position = eventTempSpot
                local event = getObjectFromGUID(expansionContents[i][1]).takeObject(params)
                for j=1,event.getQuantity() do
                    local card = event.takeObject({})
                    eventDeck.putObject(card)
                end
            end
            if expansionItems[i] != "none" then
                params.guid = expansionItems[i]
                params.position = itemTempSpot
                local item = getObjectFromGUID(expansionContents[i][1]).takeObject(params)
                for j=1,item.getQuantity() do
                    local card = item.takeObject({})
                    itemDeck.putObject(card)
                end
            end
        end
    end

    if shinyExpansion == true then
        shinyCards()
    else
        local shiny = getObjectFromGUID(shinyBag)
        shiny.destruct()

        shiny = getObjectFromGUID(shinyMegas)
        shiny.destruct()
    end
end

function moveDecks()
    local eventDeck = getObjectFromGUID(eventCards)
    local itemDeck = getObjectFromGUID(itemCards)
    local eventPosition = eventCardDeckLocations[boardGeneration]
    local itemPosition = itemCardDeckLocations[boardGeneration]

    eventDeck.shuffle()
    itemDeck.shuffle()
    eventDeck.setPosition(eventPosition)
    itemDeck.setPosition(itemPosition)
end

function placeBadges()
    local locations = badges[boardGeneration][1]
    local guids = badges[boardGeneration][2]
    local cartridge = getObjectFromGUID(gameCartridges[boardGeneration])
    local params = {}

    for i=1, #locations do
        params.position = locations[i]
        params.guid = guids[i]
        params.rotation = {0,180,0}
        cartridge.takeObject(params)
    end
end

function placeRivals(id)
    local cartridge = getObjectFromGUID(gameCartridges[id])
    local params = {}
    params.position = rivalCardLocations[boardGeneration]
    params.guid = rivalCardGUIDs[id]
    params.rotation = {0.00, 180.00, 180.00}

    cartridge.takeObject(params)
end

--Functions from the generation checkboxes that call to mark checkbox and apply flag
function gen1() click_checkGen(1) end
function gen2() click_checkGen(2) end
function gen3() click_checkGen(3) end
function gen4() click_checkGen(4) end
function gen5() click_checkGen(5) end
function gen6() click_checkGen(6) end
function gen7() click_checkGen(7) end
function alola() click_checkGen(8) end

--Applies flag to generationsUsed based on checkbox selected
function click_checkGen(gen)
    local find_func
    if gen == 8 then
        find_func = function(o) return o.click_function==("alola") end
    else
        find_func = function(o) return o.click_function==("gen" .. gen) end
    end
    local buttonEntry = findButton(self, find_func)
    if generationsUsed[gen] == true then
        generationsUsed[gen] = false
        self.editButton({index=buttonEntry.index, label="", color={1,0,0}})
    else
        generationsUsed[gen] = true
        self.editButton({index=buttonEntry.index, label=string.char(10004), color={0,1,0}})
    end
end

--Functions from the expansion checkboxes that call to mark checkbox and apply flag
function exp1() click_checkExp(1) end
function exp2() click_checkExp(2) end
function exp3() click_checkExp(3) end
function exp4() click_checkExp(4) end
function exp5() click_checkExp(5) end
function exp6() click_checkExp(6) end
function exp7() click_checkExp(7) end
function exp8() click_checkExp(8) end
function exp9() click_checkExp(9) end

--Applies flag to expansionsUsed based on checkbox selected
function click_checkExp(exp)
    local find_func = function(o) return o.click_function==("exp" .. exp) end
    local buttonEntry = findButton(self, find_func)
    if expansionsUsed[exp] == true then
        expansionsUsed[exp] = false
        self.editButton({index=buttonEntry.index, label="", color={1,0,0}})
    else
        expansionsUsed[exp] = true
        self.editButton({index=buttonEntry.index, label=string.char(10004), color={0,1,0}})
    end
end

--Locates a button with a helper function
function findButton(obj, func)
    if func==nil then error("No func supplied to findButton") end
    for _, v in ipairs(obj.getButtons()) do
        if func(v) then
            return v
        end
    end
    return nil
end

--Applies the settings from the control panel and tells setup to run
function click_apply()
  local genSelected = false
     local gen = 1
     while not genSelected do
         if generationsUsed[gen] == true then
             genSelected = true
             break
         end
         gen = gen + 1
         if gen == 8 then
             broadcastToAll("No Generation Selected", {1, 0, 0})
             break
         end
     end
     if boardGeneration == 0 then
         broadcastToAll("No Region Selected", {1, 0, 0})
     end
     if genSelected == true and boardGeneration > 0 then
         broadcastToAll("Setting Up", {0, 1, 0})
             cleanup()
             runSetup()
             createCardButtons()
             call_move()
    end
end

--Empty function to provide something for input fields to point to at creation
function splash()
    --But nothing happened!
end

function call_move()
  for i=1, #usedBalls do
    getObjectFromGUID(usedBalls[i]).call("move")
  end
  local activate_params = {}
  activate_params.region = boardGeneration
  getObjectFromGUID(usedBalls[1]).call("activate", activate_params)
  getObjectFromGUID("4dba05").call("activate", activate_params)
end

function generateRegionOptions()
   if toggleRegionSelect then
     removeButton("setRegion1")
     removeButton("setRegion2")
     removeButton("setRegion3")
     removeButton("setRegion4")
     removeButton("setRegion5")
     removeButton("setRegion6")
     removeButton("setRegion7")
      toggleRegionSelect = false
   else
       local xOffset = 7.5
       local zOffset = 17.2
       local zIncrement = 1.25
        self.createButton({ --Board Generation input
            label=gameRegions[1], click_function="setRegion1", function_owner=self,
            alignment=3, position={xOffset,0,zOffset}, scale={2.5,2.5,2.5}, height=250, width=750, font_size=150,
            tooltip="Click to select generation for game board.",
        })
        self.createButton({ --Board Generation input
            label=gameRegions[2], click_function="setRegion2", function_owner=self,
            alignment=3, position={xOffset,0,zOffset+zIncrement}, scale={2.5,2.5,2.5}, height=250, width=750, font_size=150,
            tooltip="Click to select generation for game board.",
        })
        self.createButton({ --Board Generation input
            label=gameRegions[3], click_function="setRegion3", function_owner=self,
            alignment=3, position={xOffset,0,zOffset+zIncrement*2}, scale={2.5,2.5,2.5}, height=250, width=750, font_size=150,
            tooltip="Click to select generation for game board.",
        })
        self.createButton({ --Board Generation input
            label=gameRegions[4], click_function="setRegion4", function_owner=self,
            alignment=3, position={xOffset,0,zOffset+zIncrement*3}, scale={2.5,2.5,2.5}, height=250, width=750, font_size=150,
            tooltip="Click to select generation for game board.",
        })
        self.createButton({ --Board Generation input
            label=gameRegions[5], click_function="setRegion5", function_owner=self,
            alignment=3, position={xOffset,0,zOffset+zIncrement*4}, scale={2.5,2.5,2.5}, height=250, width=750, font_size=150,
            tooltip="Click to select generation for game board.",
        })
        self.createButton({ --Board Generation input
            label=gameRegions[6], click_function="setRegion6", function_owner=self,
            alignment=3, position={xOffset,0,zOffset+zIncrement*5}, scale={2.5,2.5,2.5}, height=250, width=750, font_size=150,
            tooltip="Click to select generation for game board.",
        })
        self.createButton({ --Board Generation input
            label=gameRegions[7], click_function="setRegion7", function_owner=self,
            alignment=3, position={xOffset,0,zOffset+zIncrement*6}, scale={2.5,2.5,2.5}, height=250, width=750, font_size=150,
            tooltip="Click to select generation for game board.",
        })
        toggleRegionSelect = true
   end


end

function setRegion1() updateRegion(1) end
function setRegion2() updateRegion(2) end
function setRegion3() updateRegion(3) end
function setRegion4() updateRegion(4) end
function setRegion5() updateRegion(5) end
function setRegion6() updateRegion(6) end
function setRegion7() updateRegion(7) end

function updateRegion(regionIndex)
  --update selectedRegion
  local find_func = function(o) return o.click_function=="generateRegionOptions" end
  local buttonEntry = findButton(self, find_func)
  boardGeneration = regionIndex
  buttonEntry.label = gameRegions[regionIndex]
  self.editButton(buttonEntry)

  --now remove options
  removeButton("setRegion1")
  removeButton("setRegion2")
  removeButton("setRegion3")
  removeButton("setRegion4")
  removeButton("setRegion5")
  removeButton("setRegion6")
  removeButton("setRegion7")
  toggleRegionSelect = false

  local table = {}
  table.origin = initialLocation
  table.direction = {0,1,0}
  table.type = 1
  table.max_distance = 5
  local hits = Physics.cast(table)

  boardObject = hits[1].hit_object
  if boardObject.getStateId() != boardGeneration then boardObject.setState(boardGeneration) end
end

function removeButton(clickFunction)
  find_func = function(o) return o.click_function==clickFunction end
  buttonEntry = findButton(self, find_func)
  self.removeButton(buttonEntry.index)
end

function fellowshipButton()
    local find_func = function(o) return o.click_function==("fellowshipButton") end
    local buttonEntry = findButton(self, find_func)
    if fellowshipExpansions == true then
        fellowshipExpansions = false
        self.editButton({index=buttonEntry.index, label="", color={1,0,0}})
    else
        fellowshipExpansions = true
        self.editButton({index=buttonEntry.index, label=string.char(10004), color={0,1,0}})
    end
end

function shinyButton()
    local find_func = function(o) return o.click_function==("shinyButton") end
    local buttonEntry = findButton(self, find_func)
    if shinyExpansion == true then
        shinyExpansion = false
        self.editButton({index=buttonEntry.index, label="", color={1,0,0}})
    else
        shinyExpansion = true
        self.editButton({index=buttonEntry.index, label=string.char(10004), color={0,1,0}})
    end
end

function shinySetup(index)
    -- put all shiny gens selected into 1 container
    -- add shinyMax random shinies from each color to the game
    -- pink/green have highter chance than blue/red/legend

    local genCount = 0
    for i=1,#generationsUsed do
        if i <= shinyGensAvailable then
            local pokeballShiny = getObjectFromGUID(shinyPokeballs[i][index])
            if generationsUsed[i] == true then
                genCount = genCount + 1
                for j=1,pokeballShiny.getQuantity() do
                    local shiny = pokeballShiny.takeObject({})
                    getObjectFromGUID(shinyFinalPokeballs[index]).putObject(shiny)
                end
            end
            pokeballShiny.destruct()
        end
    end

    local shinyMax = shinyAmount[index] * genCount
    local pokeballShiny = getObjectFromGUID(shinyFinalPokeballs[index])
    if pokeballShiny.getQuantity() != 0 then
        pokeballShiny.shuffle()
        for j=1,shinyMax do
            local shiny = pokeballShiny.takeObject({})
            getObjectFromGUID(usedBalls[index + 1]).putObject(shiny)
        end
    end
end

function shinyCards()
    local bag = getObjectFromGUID(shinyBag)
    local eventTempSpot = {46.41, 1.03, 3.97}
    local itemTempSpot = {50.41, 1.03, 3.97}
    
    local shinyCardsEvents = {}
    shinyCardsEvents.guid = shinyEvents
    shinyCardsEvents.position = eventTempSpot

    local shinyCardsItems = {}
    shinyCardsItems.guid = shinyItems
    shinyCardsItems.position = itemTempSpot

    local deck = getObjectFromGUID(shinyBag).takeObject(shinyCardsEvents)
    for j=1,deck.getQuantity() do
        local card = deck.takeObject({})
        eventDeck.putObject(card)
    end

    deck = getObjectFromGUID(shinyBag).takeObject(shinyCardsItems)
    for j=1,deck.getQuantity() do
        local card = deck.takeObject({})
        itemDeck.putObject(card)
    end

    bag.destruct()
end

function fellowshipSetup()
    local bag = getObjectFromGUID(fellowshipBag)
    eventDeck = getObjectFromGUID(eventCards)
    itemDeck = getObjectFromGUID(itemCards)

    if fellowshipExpansions then
        shinyExpansion = true
        expansionsUsed = {true, false, true, true, false, true, false, false, true} -- our expansions

        for k=1,5 do            
        -- add our extra pokemon (cross gen 1-4)
            local pokeball = getObjectFromGUID(extraContainers[k])
            for i=1,pokeball.getQuantity() do
                local pog = pokeball.takeObject({})
                getObjectFromGUID(usedBalls[k + 1]).putObject(pog)
            end
            pokeball.destruct()

            shinySetup(k)
        end
        
        local eventTempSpot = {46.41, 1.03, 3.97}
        local itemTempSpot = {50.41, 1.03, 3.97}

        local items = {}
        items.guid = fellowShipItems
        items.position = itemTempSpot

        local events = {}
        events.guid = fellowShipEvents
        events.position = eventTempSpot
       
        local deck = getObjectFromGUID(fellowshipBag).takeObject(items)
        for j=1,deck.getQuantity() do
            local card = deck.takeObject({})
            itemDeck.putObject(card)
        end

        deck = getObjectFromGUID(fellowshipBag).takeObject(events)
        for j=1,deck.getQuantity() do
            local card = deck.takeObject({})
            eventDeck.putObject(card)
        end
    else
        for k=1,5 do            
            local pokeball = getObjectFromGUID(extraContainers[k])
            pokeball.destruct()
    
            if shinyExpansion == true then
                shinySetup(k)
            else
                for i=1,shinyGensAvailable do
                    local pokeballShiny = getObjectFromGUID(shinyPokeballs[i][k])
                    pokeballShiny.destruct()
                end

                local pokeballShinyFinal = getObjectFromGUID(shinyFinalPokeballs[k])
                pokeballShinyFinal.destruct()
            end
        end
    end
    bag.destruct()
end

function createCardButtons()
    getObjectFromGUID(rightButtons_GUID).createButton({
            label='Draw Item\nCard!', 
            click_function="takeItem", 
            function_owner=self, 
            position={0.0, 0.0, 0.30},
            rotation = {0,180,0},
            scale={2,1,1},
            height=200,
            width=300,
            font_size=43,
            hover_color={r=1.0, b=0.5, g=0.5},
            press_color={r=1, b=0, g=0}
    })

    getObjectFromGUID(leftButtons_GUID).createButton({
            label='Draw Event\nCard!', 
            click_function="takeEvent", 
            function_owner=self,
            position={0.0, 0.0, 0.30},
            rotation = {0,180,0},
            scale={2,1,1},
            height=200,
            width=300,
            font_size=43,
            hover_color={r=1.0, b=0.5, g=0.5},
            press_color={r=1, b=0, g=0}
    })

    if fellowshipExpansions or expansionsUsed[8] == true then
        getObjectFromGUID(leftButtons_GUID).createButton({
                label='Event\nPremonition!', 
                click_function="premonition", 
                function_owner=self,
                position={0.0, 0.0, -0.20},
                rotation = {0,180,0},
                scale={2,1,1},
                height=200,
                width=300,
                font_size=43,
                hover_color={r=1.0, b=0.5, g=0.5},
                press_color={r=1, b=0, g=0}
        })

        getObjectFromGUID(rightButtons_GUID).createButton({
            label='Item\nSwap!', 
            click_function="itemSwap", 
            function_owner=self,
            position={0.0, 0.0, -0.20},
            rotation = {0,180,0},
            scale={2,1,1},
            height=200,
            width=300,
            font_size=43,
            hover_color={r=1.0, b=0.5, g=0.5},
            press_color={r=1, b=0, g=0}
        })
    end
end

function takeItem(clicked_object, clicker)
    if itemDeck != nil then
        broadcastToAll(clicker .. " drew an Item Card!")
        itemDeck.dealToColor(1, clicker)
    else
        findItemDiscardDeck()
        if itemDiscardDeck != nil then
            broadcastToAll("No cards in the deck! Reshuffling...")
            itemDiscardDeck.flip()
            itemDiscardDeck.shuffle()
            itemDiscardDeck.setPosition(itemCardDeckLocations[boardGeneration])
            Wait.time(function() findItemDeck() end, 0.3)
            Wait.time(function() takeItem(clicked_object, clicker) end, 0.3)
        else
            broadcastToAll("No discarded cards found... where are they?")
        end
    end
end

function premonition(clicked_object, clicker)
    if eventDeck != nil then
        broadcastToAll(clicker .. " is looking into the future!")
        local cards = eventDeck.getQuantity()
        if cards < 3 then
            local rest = 3 - cards
            eventDeck.dealToColor(cards, clicker)
            shuffleEventCards()
            Wait.time(function() dealRemainingPremonition(rest, clicker) end, 0.3)
        else
            eventDeck.dealToColor(3, clicker)
        end 
    else
        shuffleEventCards()
        Wait.time(function() premonition(clicked_object, clicker) end, 0.3)
    end
end

function shuffleEventCards()
    findEventDiscardDeck()
    if eventDiscardDeck != nil then
        broadcastToAll("No cards in the deck! Reshuffling...")
        eventDiscardDeck.flip()
        eventDiscardDeck.shuffle()
        eventDiscardDeck.setPosition(eventCardDeckLocations[boardGeneration])
        Wait.time(function() findEventDeck() end, 0.3)
    else
        broadcastToAll("No discarded cards found... where are they?")
    end
end

function dealRemainingPremonition(remaining, clicker)
    if eventDeck != nil then
        eventDeck.dealToColor(remaining, clicker)
    end
end

function takeEvent(clicked_object, clicker)
    if last_card != nil then
        local cardRotation = last_card.getRotation()
        last_card.setPositionSmooth(eventRevealPosition)
        last_card.setRotationSmooth({cardRotation.x, cardRotation.y, 0})
        eventCallback(last_card)
        last_card = nil
        broadcastToAll(clicker .. " drew an Event Card!")
        return
    end

    if eventDeck != nil then
        local faceup = {}
        faceup.position = eventRevealPosition
        faceup.flip = true
        faceup.callback_function = eventCallback
        broadcastToAll(clicker .. " drew an Event Card!")
        eventDeck.takeObject(faceup)
        last_card = eventDeck.remainder
    else
        shuffleEventCards()
        Wait.time(function() takeEvent(clicked_object, clicker) end, 0.3)
    end
end

function eventCallback(card)
    card.setLock(true)
    Timer.create({
        identifier    = "Discard Card " .. card.guid,
        function_name = "discardEvent",
        parameters    = {card},
        delay         = 4, -- Seconds to hold card out for
        repetitions   = 1
    })
end

function discardEvent(card)
    card[1].setPositionSmooth(discardEventLocations[boardGeneration])
    card[1].setLock(false)
end

function findItemDeck()
    itemDeck = nil
    local objects = itemZone.getObjects()
    for i, decks in ipairs(objects) do
        local tmp = getObjectFromGUID(decks.getGUID())
        local table = getObjectFromGUID(tableMap_GUIDs[boardGeneration])
        if tmp != table then
            itemDeck = tmp
        end
    end
end

function findEventDeck()
    eventDeck = nil
    local objects = eventZone.getObjects()
    for i, decks in ipairs(objects) do
        local tmp = getObjectFromGUID(decks.getGUID())
        local table = getObjectFromGUID(tableMap_GUIDs[boardGeneration])
        if tmp != table then
            eventDeck = tmp
        end
    end
end

function findItemDiscardDeck()
    itemDiscardDeck = nil
    local objects = itemDiscard.getObjects()
    for i, decks in ipairs(objects) do
        local tmp = getObjectFromGUID(decks.getGUID())
        local table = getObjectFromGUID(tableMap_GUIDs[boardGeneration])
        if tmp != table then
            itemDiscardDeck = tmp
        end
    end
end

function findEventDiscardDeck()
    eventDiscardDeck = nil
    local objects = eventDiscard.getObjects()
    for i, decks in ipairs(objects) do
        local tmp = getObjectFromGUID(decks.getGUID())
        local table = getObjectFromGUID(tableMap_GUIDs[boardGeneration])
        if tmp != table then
            eventDiscardDeck = tmp
        end
    end
end

function setupCardZones()
    itemZone = getObjectFromGUID(itemZones[boardGeneration])
    eventZone = getObjectFromGUID(eventZones[boardGeneration])
    itemDiscard = getObjectFromGUID(itemDiscardZones[boardGeneration])
    eventDiscard = getObjectFromGUID(eventDiscardZones[boardGeneration])
    getObjectFromGUID(tableMap_GUIDs[boardGeneration]).interactable = true -- fix for findDeck
    getObjectFromGUID(tableMap_GUIDs[boardGeneration]).setLock(true)
end

function cleanup()
    local buttons = self.getButtons()
    for k = 1,#buttons do
        self.removeButton(buttons[k].index)
    end
    self.interactable = false
    self.setLock(true)
end

function itemSwap()
    local players = getRealSeatedPlayers()
    local playersClockwise = playersClockwise(players)
    
    for i, player in ipairs(playersClockwise) do
        local moveToIndex = 1
        if i == #players
        then
            moveToIndex = 1
        else
            moveToIndex = i + 1
        end
        local moveToPlayer = playersClockwise[moveToIndex]
        rotationOffsetY = moveToPlayer.getHandTransform().rotation.y - player.getHandTransform().rotation.y
        if rotationOffsetY < 0
        then
            rotationOffsetY = rotationOffsetY + 360
        end

        for i,card in ipairs(player.getHandObjects()) do
            card.setPosition(getPlayerHandPosition(moveToPlayer))
            card.rotate({0, rotationOffsetY, 0})
        end
    end
end

-- getSeatedPlayers() doesn't return the actual Player objects.
-- This function will instead return the 'real' Player objects.
function getRealSeatedPlayers()
    local playerColors = getSeatedPlayers()
    local players = {}
    local newI = 1
    for i, playerColor in pairs(playerColors) do
        if Player[playerColor].getPlayerHand() != nil
        then
            players[newI] = Player[playerColor]
            newI = newI + 1
        end
    end
    return players
end

function getPlayerHandPosition(player)
    hand = player.getPlayerHand()
    return {hand.pos_x+1, hand.pos_y+1, hand.pos_z+1} 
    -- somehow +1 on all fixes the item swap bug
end

-- Returns a Table with player angles (in radians) as the keys
function playerAngles(players)
    local angles = {}
    for i, player in pairs(players) do
        angles[getPlayerAngle(player)] = player
    end
    return angles
end

function playersClockwise(players)
    local newPlayers = {}
    local newI = #players
    for i, player in pairsByKeys(playerAngles(players)) do
        newPlayers[newI] = player
        newI = newI - 1
    end
    return newPlayers
end

function getPlayerAngle(player)
    local hand = player.getPlayerHand()
    return math.atan2(hand.pos_z, hand.pos_x)
end

-- Copied from LUA docs... returns iterator in order of keys
function pairsByKeys (t, f)
    local a = {}
    for n in pairs(t) do table.insert(a, n) end
        table.sort(a, f)
        local i = 0      -- iterator variable
        local iter = function ()   -- iterator function
        i = i + 1
        if a[i] == nil then return nil
            else return a[i], t[a[i]]
        end
    end
    return iter
end