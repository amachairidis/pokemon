MODEL_SIZE_CHANGE_STEP = 1.5
SPAWN_DELAY_CHANGE_STEP = 0.1
optionX = -2.2
optionY = 0
optionZ = 0.2
options_shown = false
option_buttons={}

function onLoad(save_state)
    self.tooltip = false

    self.createButton({ --Apply settings button
        label="3D Model Options", click_function="click_model_options",
        function_owner=self, tooltip="Click to show options for the 3D models",
        position={optionX, optionY, optionZ}, rotation={0,90,0}, height=250, width=1000, font_size=100,
    })
end

function click_model_options()
    if options_shown then
        options_shown = false
        for _,button_id in ipairs(option_buttons) do
            local button = findButton(button_id)
            if button then self.removeButton(button.index) end
        end
        option_buttons = {}
    else
        options_shown = true

        -- Enable Models
        self.createButton({
            label="Enable Models", click_function="splash", function_owner=self,
            position={optionX-0.75,optionY,optionZ+0.8}, rotation={0,90,0}, height=0, width=0, font_size=200, font_color={1,1,1}
        })
        register_button("Enable Models")
        self.createButton({
            label="", click_function="toggle_models",
            function_owner=self, tooltip="Check to enable 3D models",
            position={optionX-0.7,optionY,optionZ-0.8}, rotation={0,90,0}, height=224, width=224, font_size=200
        })
        register_button("toggle_models")
        set_enable_button()

        -- Model size
        self.createButton({
            label="Model Size", click_function="splash", function_owner=self,
            position={optionX-1.35,optionY,optionZ+1.1}, rotation={0,90,0}, height=0, width=0, font_size=200, font_color={1,1,1}
        })
        register_button("Model Size")
        self.createButton({
            label="", click_function="decrease_model_size",
            function_owner=self, tooltip="Click to decrease the model size",
            position={optionX-1.3,optionY,optionZ-0.8}, rotation={0,90,0}, height=224, width=224, font_size=200, label="-"
        })
        register_button("decrease_model_size")
        self.createButton({
            label="", click_function="increase_model_size",
            function_owner=self, tooltip="Click to increase the model size",
            position={optionX-1.3,optionY,optionZ-0.2}, rotation={0,90,0}, height=224, width=224, font_size=200, label="+"
        })
        register_button("increase_model_size")


        -- Spawn delay
        self.createButton({
            label="Spawn Delay", click_function="splash", function_owner=self,
            position={optionX-1.95,optionY,optionZ+1.3}, rotation={0,90,0}, height=0, width=0, font_size=200, font_color={1,1,1}
        })
        register_button("Spawn Delay")
        self.createButton({
            label="", click_function="splash_spawn_delay", function_owner=self,
            position={optionX-1.95,optionY,optionZ+3.1}, rotation={0,90,0}, height=0, width=0, font_size=200, font_color={1,1,1}
        })
        register_button("splash_spawn_delay")
        set_spawn_delay_label()
        self.createButton({
            label="", click_function="decrease_spawn_delay",
            function_owner=self, tooltip="Click to decrease the delay before the spawn animation shows (useful if some players cannot see the spawn animations).",
            position={optionX-1.9,optionY,optionZ-0.8}, rotation={0,90,0}, height=224, width=224, font_size=200, label="-"
        })
        register_button("decrease_spawn_delay")
        self.createButton({
            label="", click_function="increase_spawn_delay",
            function_owner=self, tooltip="Click to increase the delay before the spawn animation shows (useful if some players cannot see the spawn animations).",
            position={optionX-1.9,optionY,optionZ-0.2}, rotation={0,90,0}, height=224, width=224, font_size=200, label="+"
        })
        register_button("increase_spawn_delay")
    end
end

function register_button(button_id)
    option_buttons[#option_buttons+1] = button_id
end
--Locates a button with a helper function
function findButton(button_id)
    for _, button in ipairs(self.getButtons()) do
        if button.label == button_id or button.click_function == button_id then
            return button
        end
    end
    return nil
end

function set_enable_button()
    local enable_button = findButton("toggle_models")
    if Global.call("get_models_enabled") then
        self.editButton({index=enable_button.index, label=string.char(10004), color={0,1,0}})
    else
        self.editButton({index=enable_button.index, label="", color={1,1,1}})
    end
end

function toggle_models()
    Global.Call("toggle_models_enabled")
    set_enable_button()
end

function increase_model_size()
    Global.Call("scale_models", {scale=MODEL_SIZE_CHANGE_STEP})
end

function decrease_model_size()
    Global.Call("scale_models", {scale=1/MODEL_SIZE_CHANGE_STEP})
end

function set_spawn_delay_label()
    local spawn_delay_label = findButton("splash_spawn_delay")
    self.editButton({index=spawn_delay_label.index, label=string.format("(%.1fs)", Global.Call("get_spawn_delay"))})
end

function increase_spawn_delay()
    Global.Call("increase_spawn_delay", {delay=SPAWN_DELAY_CHANGE_STEP})
    set_spawn_delay_label()
end

function decrease_spawn_delay()
    Global.Call("increase_spawn_delay", {delay=-SPAWN_DELAY_CHANGE_STEP})
    set_spawn_delay_label()
end

function splash()
    --But nothing happened!
end

function splash_spawn_delay()
    --Nothing happened! But with style.
end