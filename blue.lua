local xLoc = 57
local yLoc = 1.5
local zLoc = -40
local color = "blue"
local input
local chipY = 2.61
local kantoLocations = {{9.85,2.83},{16.43,-7.61},{20.28,4.51},{29.29,4.51},{29.93,-3.25},{28.04,-8.72},{22.67,-11.86},{19.86,-17.22},{9.75,-12.78},{9.45,-20.43}}
local jhotoLocations = {{0.51,-22.98},{-10.82,-18.76},{-17.84,-19.31},{-24.46,-22.85},{-25.25,-16.93},{-17.1,-14.81},{-16.49,-3.92},{-23.19,-3.47},{-28.47,-9.97},{-30.39,-3.23}}
local hoennLocations = {{-38.3,6.73},{-29.83,11.68},{-36.45,15.22},{-33.81,22.39},{-21.8,22.85},{-10.68,20.57},{-20.98,9.96},{-12.09,13.49},{4.58,22.05},{0.9,14.49}}
local sinnohLocations = {{-0.05,1.71},{3.62,7.91},{12.26,7.21},{9.22,13.46},{20.02,17.9},{16.89,10.66},{15.96,3.94},{15.85,-5.11},{5.56,0.56},{11.18,-7.68}}
local unovaLocations = {{-4.65,-7.78},{8.87,-6.67},{-8.84,1.73},{-15.67,-0.36},{-10.65,-6.34},{-18.86,4.06},{-28.16,5.08},{-35.98,0.07},{-30.59,11.59},{-15.2,7.13},{-17.07,12.87}}
local kalosLocations = {{10.92,12.53},{13.03,16.82},{12.12,21,43},{-2.74,18.43},{-11.24,23.33},{-12.65,18.93},{-8.8,14.82},{1.67,12.31},{-11.66,2.3},{-16.44,2.07},{-33.07,-0.83},{-32.35,8.88}}
local alolaLocations = {{3.08,22.94},{2.5,15.31},{0.31,9.39},{2.68,4.56},{8.52,3.36},{12.4,8.17},{8.29,23.23},{15.87,20.97},{14.76,14.16},{13.56,-4.66}}
local locations = {kantoLocations, jhotoLocations, hoennLocations, sinnohLocations, unovaLocations, kalosLocations, alolaLocations}

function onLoad()

end

function move()
  self.setPosition({xLoc, yLoc, zLoc})
end

function activate(input_params)
    input = input_params
  self.createButton({ --Apply settings button
      label="Deal", click_function="deal_chips",
      function_owner=self, tooltip="Click to deal " .. color .. " pokemon chips to the unoccupied " .. color .." spaces.",
      position={0,0,3}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
  self.createButton({ --Apply settings button
      label="Shuffle", click_function="shuffle_chips",
      function_owner=self, tooltip="Click to shuffle contents of the " .. color .. " pokeball.",
      position={0,0,5}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
  self.createButton({ --Apply settings button
      label="Collect", click_function="collectchips",
      function_owner=self, tooltip="Click to return the " .. color .. " chips to the " .. color .. " pokeball.",
      position={0,0,7}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
end

function deal_chips()
  local currentLocations = locations[input.region]
  for i=1, #currentLocations do
   local param = {}
   param.origin = {currentLocations[i][1], chipY, currentLocations[i][2]}
   param.direction = {0,-1,0}
   param.type = 1
   param.max_distance = 1
   param.debug = false
   hits = Physics.cast(param)
   if(#hits == 0) then
     if self.getQuantity() > 0 then
       local param2 = {}
       param2.position = param.origin
       param2.rotation = {180,0,0}
       self.takeObject(param2)
     end
   end
  end
end

function shuffle_chips()
   self.shuffle()
 end

 function collectchips()
   local currentLocations = locations[input.region]
   for i=1, #currentLocations do
     local param = {}
     param.origin = {currentLocations[i][1], chipY, currentLocations[i][2]}
     param.direction = {0,-1,0}
     param.type = 1
     param.max_distance = 1
     param.debug = false
     hits = Physics.cast(param)
     if #hits ~= 0 then
        if hits[2] ~= nil then
            self.putObject(getObjectFromGUID(hits[2].hit_object.guid))
        else
            self.putObject(getObjectFromGUID(hits[1].hit_object.guid))
        end
    end
   end
 end