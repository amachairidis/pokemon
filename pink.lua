local xLoc = 67
local yLoc = 1.5
local zLoc = -30
local color = "pink"
local input
local chipY = 2.61
local kantoLocations = {{-29.33, -9.42}, {-32.83, -4.95}, {-31.49, 4.1}, {-23.45, 9.48}, {-33.98, 9.61}, {-34.5,14.72}, {-28.72,18.29}, {-25.76,23.69}, {-16.7, 23.71}, {-16.59,14.1}}
local jhotoLocations = {{11.73, -13.58}, {7.00, -12.51}, {9.04,-2.75},{8.11,4.98},{-5.33,0.21},{0.85, -2.66},{0.68,-12.12},{-12.9,-12.76},{-12.77,-0.77},{-11.33,5.84}}
local hoennLocations = {{-20.3,-7.96},{-20.29,1.07},{-27.41,0.91},{-38.09,-2.7},{-38.15,-13.8},{-30.19,-14.35}}
local sinnohLocations = {{-32.71,-18.14},{-22.99,-23.57},{-12.59,-23.38},{-10.76,-18.12},{-15.49,-14.46},{-28.07,-15.33},{-24.67,-7.76},{-17.12,-7.74},{-7.37,-3.94},{-7.13,0.78}}
local unovaLocations = {{27.13,-22.31},{34.26,-22.45},{38.51,-12.39},{32.37,-11.3},{26.73,-1.59},{32.03,2.96},{30.92,7.57},{15.66,7.37},{20.8,2.06},{19.23,-4.44},{27.64,-7.73},{18.29,-15.14},{13.57,-7.93},{13.49,-14.71}}
local kalosLocations = {{11.79,-21.6},{6.07,-21.16},{1.74,-18.62},{-0.09,-10.82},{6.82,-14.18},{3.71,-7.74},{1.34,-3.57}}
local alolaLocations = {{-24.99,-16.74},{-29.99,-10.8},{-24.81,-9.22},{-18.3,-6.63},{-28.45,-5.95},{-33.7,-3.65},{-35.36,4.8},{-25.2,5.24},{-33.24,9.29},{-15.62,7.81},{-20.26,-1.69},{1.51,-1.97}}
local locations = {kantoLocations, jhotoLocations, hoennLocations, sinnohLocations, unovaLocations, kalosLocations, alolaLocations}
function onLoad()

end

function move()
  self.setPosition({xLoc, yLoc, zLoc})
end


function activate(input_params)
  input = input_params
  self.createButton({ --Apply settings button
      label="Deal", click_function="deal_chips",
      function_owner=self, tooltip="Click to deal " .. color .. " pokemon chips to the unoccupied " .. color .." spaces.",
      position={0,0,3}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
  self.createButton({ --Apply settings button
      label="Shuffle", click_function="shuffle_chips",
      function_owner=self, tooltip="Click to shuffle contents of the " .. color .. " pokeball.",
      position={0,0,5}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
  self.createButton({ --Apply settings button
      label="Collect", click_function="collectchips",
      function_owner=self, tooltip="Click to return the " .. color .. " chips to the " .. color .. " pokeball.",
      position={0,0,7}, rotation={0,0,0}, height=440, width=1400, font_size=200,
  })
end

function deal_chips()
  local currentLocations = locations[input.region]
  for i=1, #currentLocations do
   local param = {}
   param.origin = {currentLocations[i][1], chipY, currentLocations[i][2]}
   param.direction = {0,-1,0}
   param.type = 1
   param.max_distance = 1
   param.debug = false
   hits = Physics.cast(param)
   if(#hits == 0) then
     if self.getQuantity() > 0 then
       local param2 = {}
       param2.position = param.origin
       param2.rotation = {180,0,0}
       self.takeObject(param2)
     end
   end
  end
end

function shuffle_chips()
   self.shuffle()
 end

 function collectchips()
   local currentLocations = locations[input.region]
   for i=1, #currentLocations do
     local param = {}
     param.origin = {currentLocations[i][1], chipY, currentLocations[i][2]}
     param.direction = {0,-1,0}
     param.type = 1
     param.max_distance = 1
     param.debug = false
     hits = Physics.cast(param)
     if #hits ~= 0 then
        if hits[2] ~= nil then
            self.putObject(getObjectFromGUID(hits[2].hit_object.guid))
        else
            self.putObject(getObjectFromGUID(hits[1].hit_object.guid))
        end
    end
   end
 end